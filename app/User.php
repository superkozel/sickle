<?php

namespace App;

use Adldap\Laravel\Traits\HasLdapUser;
use App\Models\Cluster;
use App\Models\Cryptocurrency\Ethereum;
use App\Models\Payment\EthPay;
use App\Models\Payment\Transaction;
use App\Models\RigRegistration;
use App\Models\User\KnownLocation;
use App\Models\User\NotificationSettings;
use App\Models\User\SecurityLog;
use App\Notifications\BaseNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\RoutesNotifications;

/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cluster[] $clusters
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\KnownLocation[] $locations
 * @property int $id
 * @property string|null $name
 * @property string $email
 * @property string|null $phone
 * @property string|null $telegram
 * @property int $ldap_id
 * @property string $password
 * @property float $balance
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property NotificationSettings $notification_settings
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLdapId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment\Transaction[] $paymentTransactions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigRegistration[] $rigRegistrations
 */
class User extends Authenticatable
{
    use Notifiable {
        notify as protected traitNotify;
        notifyNow as protected traitNotifyNow;
    }
    use HasLdapUser;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getBalance()
    {
        return $this->balance;
    }

    public function clusters()
    {
        return $this->belongsToMany(Cluster::class);
    }

    public function accesses()
    {
        return $this->clusters()->wherePivot('role', '!=', Cluster::USER_ROLE_OWNER);
    }

    public function locations()
    {
        return $this->hasMany(KnownLocation::class);
    }

    public function paymentTransactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function rigRegistrations()
    {
        return $this->hasMany(RigRegistration::class, 'email', 'email');
    }

    public function securityLog()
    {
        return $this->hasMany(SecurityLog::class);
    }

    /**
     * Route notifications for the Telegram channel.
     *
     * @return int
     */
    public function routeNotificationForTelegram()
    {
        return $this->telegram_user_id;
    }

    public function getOwnCluster()
    {
        return $this->clusters()->wherePivot('role', Cluster::USER_ROLE_OWNER)->first();
    }

    public function setCurrentCluster(Cluster $cluster)
    {
        return \Session::put('current_cluster', $cluster->id);
    }

    public function getCurrentCluster()
    {
        if (\Session::has('current_cluster')) {
            return  $this->clusters->where(\Session::get('current_cluster'), '')->first();
        }
        else {
            return $this->getOwnCluster();
        }
    }

    public function getLdapRigs()
    {
        $owner = $this;

        $provider = \Adldap::getDefaultProvider();
        /** @var DomainConfiguration $config */
        $config = $provider->getConfiguration();

        $ldapRigs = \Adldap::search()->where('owner', '=', $config->get('account_prefix') . $owner->name . $config->get('account_suffix'))->get();

        return $ldapRigs;
    }

    public function getLdapUser()
    {
        if (! $this->ldap_id)
            return;

        return \Adldap::search()->users()->where('cn', $this->ldap_id)->first();
    }

    public function getPaymentAddress($currencyId)
    {
        if (! $this->payment_ethereum_address)
            $this->payment_ethereum_address = with(new Ethereum)->generateWallet();

        return $this->payment_ethereum_address;
    }

    public function isEmailConfirmed()
    {
        return $this->email == $this->email_confirmed;
    }

    /**
     * @param BaseNotification $instance
     * @return mixed
     */
    public function notify($instance)
        {
        if ($instance->allows($this)) {
            return $this->traitNotify($instance);
        }
    }

    /**
     * @param BaseNotification $instance
     * @return mixed
     */
    public function notifyNow($instance)
    {
        if ($instance->allows($this)) {
            return $this->traitNotifyNow($instance);
        }
    }

    /**
     * @return NotificationSettings
     */
    public function getNotificationSettingsAttribute()
    {
        return new NotificationSettings(json_decode($this->attributes['notification_settings'], true));
    }

    /**
     * @param NotificationSettings $notificationSettings
     */
    public function setNotificationSettingsAttribute(NotificationSettings $notificationSettings)
    {
        $this->attributes['notification_settings'] = json_encode($notificationSettings->toArray());
    }
}
