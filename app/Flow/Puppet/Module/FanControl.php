<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 26.05.2018
 * Time: 2:57
 */

namespace App\Flow\Puppet\Module;


use App\Models\Configuration;
use App\Models\Rig;

class FanControl extends Module
{
    const MODE_SLOW_SPEED = 0;
    const MODE_FULL_SPEED = 1;
    const MODE_MANUAL = 2;
    const MODE_AUTO = 3;

    public static function getOptions()
    {
        return [
            FanControl::MODE_SLOW_SPEED => 'Low speed - 30% on all GPUs',//Малые обороты - 30% на всех картах
            FanControl::MODE_FULL_SPEED => 'Full speed - 100% on all GPUs',//Полные обороты - 100% на всех картах
            FanControl::MODE_MANUAL => 'Specify manually, it is possible to specify for each GPU separately',//Указать вручную, возможно указать для каждой карты отдельно
            FanControl::MODE_AUTO => 'Automatic regulation'//Автоматическая регуляция
        ];
    }

    public static function disabled()
    {
        return [
            FanControl::MODE_AUTO
        ];
    }

    public static function getEnabledOptions()
    {
        $options = FanControl::getOptions();

        foreach (static::disabled() as $disabled) {
            unset($options[$disabled]);
        }

        return $options;
    }


    public function apply(Rig $rig, Configuration $configuration, $classes, $vars)
    {

    }
}