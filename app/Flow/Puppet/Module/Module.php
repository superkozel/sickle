<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 01.06.2018
 * Time: 1:32
 */

namespace App\Flow\Puppet\Module;


use App\Models\Configuration;
use App\Models\Rig;

abstract class Module
{
    abstract public function apply(Rig $rig, Configuration $configuration, $classes, $vars);
}