<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 10.03.2018
 * Time: 12:25
 */

namespace App\Flow\Puppet;


use function GuzzleHttp\Psr7\build_query;
use function GuzzleHttp\Psr7\parse_query;

class Api
{
    public static function signCertificate($certname)
    {
        $response = static::doRequest(config('puppet.cert_server.url') . '/puppet-ca/v1/certificate_status/' . $certname . '?environment=production', ['desired_state' => 'signed'], 'put');
        return true;
    }

    public static function getRigsFacts($rigIds, $facts = null)
    {
        \Debugbar::startMeasure('fact getting');

        $query = '["or", ' . join(', ', array_map(function($v){return '["=", "certname", "' . $v . '"]';}, $rigIds)) . ']';

        if (! empty($facts)) {
            $query = '["and",
                ' . $query . '
                ,["or", ' . join(', ', array_map(function($v){return '["=", "name", "' . $v . '"]';}, $facts)) . ']
            ]';
        }

        $response = static::doRequest(config('puppet.db.url') . '/pdb/query/v4/facts', [
            'query' => preg_replace('/\s+/', '', $query)
        ]);

        $arr = json_decode($response, true);
        \Debugbar::stopMeasure('fact getting');
        return $arr;
    }

    public static function reports($query = null)
    {
        $data = [];
        if (! empty($query)) {
            $data['query'] = static::buildQuery($query);
        }
        $response = static::doRequest(config('puppet.db.url') . '/pdb/query/v4/reports', $data);
        $arr = json_decode($response, true);
        return $arr;
    }

    public static function buildQuery($query)
    {
        $conditions = [];
        foreach ($query as $condition) {
            if (count($condition) == 3) {
                $conditions[] = '["' . $condition[1] . '", "' . $condition[0] . '", "' . $condition[2] . '"]';
            }
            else {
                if (is_array($condition[1])) {
                    $conditions[] = '["or", ' . join(', ', array_map(function($v) use ($condition){return '["=", "' . $condition[0] . '", "' . $v . '"]';}, $condition[1])) . ']';
                }
                else {
                    $conditions[] = '["=", "' . $condition[0] . '", "' . $condition[1] . '"]';
                }
            }
        }

        if (count($conditions) > 1)
            $query = '["and",' . join(',', $conditions) . ']';
        else
            $query = $conditions[0];


        return preg_replace('/\s+/', '', $query);
    }

    public static function assoc($factsRaw)
    {
        $facts = [];
        foreach ($factsRaw as $fact) {
            $facts[$fact['certname']][$fact['name']] = $fact['value'];
        }
        return $facts;
    }

    public static function doRequest($url, $data = [], $method = 'GET')
    {
        $ch = curl_init();

        if (strcasecmp($method, 'get') === 0) {
            $urlParts = parse_url($url);
            $query = array_get($urlParts, 'query', '');

            $urlParts['query'] = http_build_query(array_merge(parse_query($query), $data));

            $url = $urlParts['scheme'] . '://' . $urlParts['host'] . ($urlParts['port'] ? (':' . $urlParts['port']) : '') . $urlParts['path'] . ($urlParts['query'] ? ('?' . $urlParts['query']) : '');
        }
        else {
            if (strcasecmp($method, 'post') === 0)
                curl_setopt($ch, CURLOPT_POST, 1);
            if (strcasecmp($method, 'put') === 0)
                curl_setopt($ch, CURLOPT_PUT, 1);
            if (strcasecmp($method, 'delete') === 0)
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

//        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));

        $response = curl_exec($ch);
        if ($response === false) {
            throw new \Exception(curl_error($ch), curl_errno($ch));
        }
        curl_close($ch);

        return $response;
    }
}