<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 4:16
 */

namespace App\Flow\Api;


class Cryptocompare
{
    /**
     * Get currency pair rate
     *
     * @param $code
     * @param $currencies
     * @return mixed
     */
    public static function getRate($code, $currencies)
    {
        $data = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/price?fsym=' . $code . '&tsyms=' .
            (is_array($currencies) ? join(',', $currencies) : $currencies)),true);

        if (isset($data['Message']))
            throw new \ErrorException($data['Message']);

        if (is_array($currencies))
            return $data;
        else
            return $data[$currencies];
    }
}