<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 3:17
 */

namespace App\Flow\Api;


class Etherscan
{
    const API_KEY = 'EX49SNMP556SFVC2QUUNSX5NPK2HXCT7GW';//Superkozel:qweasdzxc

    public static function getAddressBalance($address)
    {
        $response = file_get_contents('https://api.etherscan.io/api?module=account&action=balance&address=' . $address . '&tag=latest&apikey=' . Etherscan::API_KEY);

        $data = json_decode($response, true);

        return sprintf('%.18F', strval($data['result'] / pow(10, 18)));
    }
}