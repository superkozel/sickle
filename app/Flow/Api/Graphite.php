<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 6:26
 */

namespace App\Flow\Api;


class Graphite
{
    protected $connection;

    public function open()
    {
        if (! $this->connection)
            $this->connection = fsockopen('172.16.11.55', 2003, $err, $errc, 1);
    }

    public function close()
    {
        fclose($this->connection);
    }

    public function sendBulk($data, $timestamp)
    {
        foreach ($data as $k => $v){
            $this->send($k, $v, $timestamp);
        }
    }

    public function send($key, $value, $timestamp)
    {
        fwrite($this->connection, join(' ', [$key, $value, $timestamp]));
    }
}
