<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 9:06
 */

namespace App\Flow\Api;


class BlockchainInfo
{
    public static function getAddressBalance($address)
    {
        $response = file_get_contents('https://blockchain.info/q/addressbalance/' . $address . '=6');

        return $response / 100000000;
    }
}