<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 3:16
 */

namespace App\Flow\Api;


class Coinbase
{
    /**
     * Get currency pair rate
     *
     * @param $code
     * @param $currency
     * @return mixed
     */
    public static function getRate($code, $currency)
    {
        try {
            $data = json_decode(file_get_contents('https://api.coinbase.com/v2/prices/' . $code . '-'.$currency.'/spot'),true);
            return $data['data']['amount'];
        }
        catch (\ErrorException $e) {
            var_dump($e->getMessage());
            return false;
        }
    }
}