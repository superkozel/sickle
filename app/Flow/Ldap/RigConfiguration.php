<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 19.02.2018
 * Time: 2:05
 */

namespace App\Flow\Ldap;

use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Miner;
use App\Models\Miner\BaseMiner;

class RigConfiguration
{
    const ATTRIBUTE_PUPPETCLASS = 'puppetclass';
    const ATTRIBUTE_PUPPETVARS = 'puppetvar';

    const PUPPETVAR_DUAL = 'my_claymore_dual_mode';
    const PUPPETVAR_NTP = 'my_ntp';
    const PUPPETVAR_HOSTNAME = 'my_hostname';
    const PUPPETVAR_EPOOL = 'my_epool1';
    const PUPPETVAR_EPOOL_PORT = 'my_epool1_p';

    const PUPPETVAR_FAN_MODE = 'my_fan_mode';
    const PUPPETVAR_FAN_SPEED = 'my_fan_speed';

    const PUPPETVAR_NVIDIA_GRAPHIC_CLOCK_OFFSET = 'nv_graphicsclockoffset';
    const PUPPETVAR_NVIDIA_MEMORY_TRANSFER_RATE_OFFSET = 'nv_memorytransferrateoffset';
    const PUPPETVAR_NVIDIA_POWER_LIMIT = 'nv_powerlimit';
    const PUPPETVAR_AMD_CORE_CLOCK = 'amd_core_clock';
    const PUPPETVAR_AMD_CORE_STATE = 'amd_core_state';
    const PUPPETVAR_AMD_CORE_VOLTAGE = 'amd_core_voltage';
    const PUPPETVAR_AMD_VOLTAGE_STATE = 'amd_voltage_state';
    const PUPPETVAR_AMD_MEMORY_CLOCK = 'amd_memory_clock';
    const PUPPETVAR_AMD_MEM_STATE = 'amd_mem_state';

    //базовый класс, используется при создании пустого рига без конфигурации
    const PUPPETCLASS_BASE = 'base';

    //классы по типу рига
    const PUPPETCLASS_GPU_NVIDIA = 'gpu_nvidia';
    const PUPPETCLASS_GPU_AMD = 'gpu_amd';
    const PUPPETCLASS_ASIC = 'asic';

    //класс для остановки майнинга
    const PUPPETCLASS_EMPTY_MINER = 'empty_miner';

	//модуль управления кулером
	const PUPPETCLASS_FANCONTROL = 'fancontrol';

    const WALLET_PREFIX = 'my_';
    const WALLET_SUFFIX = '_wallet';

    public static function getCurrencyPuppetVar(BaseCryptocurrency $currency)
    {
        return static::WALLET_PREFIX . strtolower($currency->getCode()) . static::WALLET_SUFFIX;
    }

    static $minerClasses = [
        Miner::ID_CLAYMORE_DUAL => 'claymore_dual'
    ];

    public static function getMinerPuppetClass(BaseMiner $miner)
    {
        return static::$minerClasses[$miner->getId()];
    }

    public static function allMinerClasses()
    {
        return static::$minerClasses;
    }

    public static function buildVars($vars)
    {
        $result = [];
        foreach ($vars as $k => $v) {
            $result[] = RigConfiguration::buildVar($k, $v);
        }

        return $result;
    }

    public static function buildVar($k, $v)
    {
        return $k . '=' . $v;
    }

    public static function parseVars($vars)
    {
        $result = [];

        if ($vars) {
            foreach ($vars as $var) {
                if (strpos($var, '=') === false)
                    continue;

                list($k, $v) = static::parseVar($var);
                $result[$k] = $v;
            }
        }

        return $result;
    }

    public static function parseVar($var)
    {
        return explode('=', $var);
    }

    public static function addClass(&$classes, $class)
    {
        if (! in_array($class, $classes)) {
            $classes[] = $class;
        }
    }

    public static function removeClass(&$classes, $class)
    {
        unset($classes[$class]);
    }

    public static function overclockOptions()
    {
        return [
            RigConfiguration::PUPPETVAR_NVIDIA_GRAPHIC_CLOCK_OFFSET,
            RigConfiguration::PUPPETVAR_NVIDIA_MEMORY_TRANSFER_RATE_OFFSET,
            RigConfiguration::PUPPETVAR_NVIDIA_POWER_LIMIT,

            RigConfiguration::PUPPETVAR_AMD_CORE_CLOCK,
            RigConfiguration::PUPPETVAR_AMD_CORE_STATE,
            RigConfiguration::PUPPETVAR_AMD_CORE_VOLTAGE,
            RigConfiguration::PUPPETVAR_AMD_VOLTAGE_STATE,
            RigConfiguration::PUPPETVAR_AMD_MEMORY_CLOCK,
            RigConfiguration::PUPPETVAR_AMD_MEM_STATE,
        ];
    }
}