<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 13.03.2018
 * Time: 17:29
 */

namespace App\Flow\Ldap;


use Adldap\Schemas\OpenLDAP;
use App\Flow\Ldap\Entry\Computer;

class Schema extends OpenLDAP
{
    /**
     * {@inheritdoc}
     */
    public function objectClassUser()
    {
        return 'inetorgperson';
    }

    public static function SSHA($password, $salt = true)
    {
        if ($salt)
            $salt = random_hex_string(16);

//        dump($salt);
//        dump(hash('sha1', $password.$salt));

        $hash = hash('sha1', $password.$salt, true);
//        dump($hash);
        return ($salt ? '{SSHA}' : '{SHA}') . base64_encode($hash . $salt);
    }

    /**
     * The class name of the Computer model.
     *
     * @return string
     */
    public function computerModel()
    {
        return Computer::class;
    }

    public function objectClassComputer()
    {
        return 'device';
    }

    public function computer()
    {
        return 'device';
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function user()
//    {
//        return 'inetorgperson';
//    }
}