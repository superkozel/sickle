<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.05.2018
 * Time: 0:53
 */

namespace App\Flow\Ldap\Entry;


use App\Flow\Ldap\RigConfiguration;

class Computer extends \Adldap\Models\Computer
{
    public function addPuppetClass(string $class)
    {
        $classes = $this->getPuppetClasses();

        if (! in_array($class, $this->getPuppetClasses())) {
            $classes[] = $class;
        }

        $this->setPuppetClasses($classes);
    }

    public function getPuppetClasses()
    {
        return $this->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS);
    }

    public function removePuppetClass(string $class)
    {
        $classes = array_diff($this->getPuppetClasses(), [$class]);

        $this->setPuppetClasses($classes);
    }

    public function setPuppetClasses(array $classes)
    {
        $this->setAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS, $classes);
    }

    public function getPuppetVars()
    {
        return RigConfiguration::parseVars($this->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS));
    }

    public function setPuppetVars(array $vars)
    {
        return $this->setAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS, RigConfiguration::buildVars($vars));
    }

    public function addPuppetVar($name, $value)
    {
        $vars = $this->getPuppetVars();
        $vars[$name] = (string) $value;

        $this->setPuppetVars($vars);
    }

    public function getPuppetVar($name)
    {
        $vars = $this->getPuppetVars();

        return $vars[$name];
    }
}