<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 19.02.2018
 * Time: 2:03
 */

namespace App\Flow\Ldap;

use App\Flow\Ldap\Entry\Computer;
use App\Flow\Puppet\Module\FanControl;
use App\Models\Cluster;
use App\Models\Configuration;
use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Cryptocurrency;
use App\Models\Enums\Miner;
use App\Models\Rig\GpuRig;
use App\Models\Rig\FactsProvider;
use App\Models\Rig;

class Import
{
    /** @return Import */
    public static function create()
    {
        return new static;
    }

    public function importRigs(Cluster $cluster, $ldapRigs)
    {
        foreach ($ldapRigs->reverse() as $ldapRig) {
            if (empty($ldapRig->cn[0]))
                continue;

            $this->importRig($cluster, $ldapRig);
        }
    }

    public function importRig(Cluster $cluster, $ldapRig)
    {
        $id = $ldapRig->cn[0];

        $puppetclasses = $ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS);
        $puppetClassToTypeMap = [
            RigConfiguration::PUPPETCLASS_GPU_NVIDIA    => GpuRig\Nvidia::getSingleType(),
            RigConfiguration::PUPPETCLASS_GPU_AMD       => GpuRig\Amd::getSingleType(),
        ];

        $rigTypeClass = 'rig';
        foreach ($puppetClassToTypeMap as $curRigPuppetClass => $class) {
            if (in_array($curRigPuppetClass, $puppetclasses)) {
                $rigTypeClass = $class;
                break;
            }
        }

        $rig = Rig::where('ldap_id', $id)->first();
        if (! $rig) {
            $rig = Rig::classFactory($rigTypeClass);
            $rig->ldap_id = $id;
        }
        else {
            $rig->updateType($rigTypeClass);
            /** @var GpuRig $rig */
            $rig = Rig::where('ldap_id', $id)->first();
        }

        if ($rig->cluster_id)//уже импортирован
            return false;

        $rig->cluster_id = $cluster->id;

        FactsProvider::loadFacts($rig, ['gpus']);
//        if (! $rig->name)
//            $rig->name = $rig->generateName();

        $puppetvars = RigConfiguration::parseVars($ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS));

        if (isset($puppetvars[RigConfiguration::PUPPETVAR_HOSTNAME]))
            $rig->hostname = $puppetvars[RigConfiguration::PUPPETVAR_HOSTNAME];
        else
            $rig->hostname = 'gluk0';

        if (isset($puppetvars[RigConfiguration::PUPPETVAR_FAN_MODE])) {
            $rig->fan_mode = in_array(RigConfiguration::PUPPETVAR_FAN_MODE, array_keys(FanControl::getOptions())) ? $puppetvars[RigConfiguration::PUPPETVAR_FAN_MODE] : null;
        }
        if (isset($puppetvars[RigConfiguration::PUPPETVAR_FAN_SPEED])) {
            $rig->fan_speed = $puppetvars[RigConfiguration::PUPPETVAR_FAN_SPEED];
        }

        $overclockOptions = RigConfiguration::overclockOptions();

        foreach ($overclockOptions as $ocOptName) {
            if (isset($puppetvars[$ocOptName])) {
                $val = preg_replace('/\s+/', '-', $puppetvars[$ocOptName]);
                $rig->{$ocOptName} = mb_strlen($val) > 0 ? $val : null;
            }
        }

        $rigConfiguration = $this->importRigConfiguration($cluster, $ldapRig);

        if ($rigConfiguration) {
            $rig->configuration_id = $rigConfiguration->id;
            $rig->configuration_checksum = $rigConfiguration->checksum;
        }

        $rig->save();

        return $rig;
    }

    protected function importRigConfiguration(Cluster $cluster, $ldapRig)
    {
        $rigConfiguration = new Configuration();
        $puppetclasses = $ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS);
        $puppetvars = $ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS);

        foreach (Miner::all() as $miner) {
            if (in_array(RigConfiguration::getMinerPuppetClass($miner), $puppetclasses)) {
                $rigConfiguration->miner_id = $miner->getId();
                break;
            }
        }

        if (! $rigConfiguration->miner_id)
            return;

        $str_between = function($content,$start,$end){
            $r = explode($start, $content);
            if (isset($r[1])){
                $r = explode($end, $r[1]);
                return $r[0];
            }
            return '';
        };

        $puppetvars = RigConfiguration::parseVars($puppetvars);
        foreach ($puppetvars as $varName => $varValue) {
            if (starts_with($varName, RigConfiguration::WALLET_PREFIX) && ends_with($varName, RigConfiguration::WALLET_SUFFIX)) {
                $currencyCode = $str_between($varName, RigConfiguration::WALLET_PREFIX, RigConfiguration::WALLET_SUFFIX);
                $currency = Cryptocurrency::findByCode($currencyCode);

                if ($currency && ! empty($varValue)) {
                    $wallet = $this->importWallet($cluster, $currency, $varValue);

                    if ($miner->hasCurrency($currency)) {
                        if (! $rigConfiguration->currency_id && $miner->hasPrimaryCurrency($currency)) {
                            $rigConfiguration->currency_id = $currency->getId();
                            $rigConfiguration->wallet_id = $wallet->id;
                        }
                        elseif (! $rigConfiguration->second_currency_id && strcmp(array_get($puppetvars, RigConfiguration::PUPPETVAR_DUAL), '0') === 0) {//не протестировано
                            $rigConfiguration->second_currency_id = $currency->getId();
                            $rigConfiguration->second_wallet_id = $wallet->id;
                        }
                    }
                }
            }
        }

        if (! empty($puppetvars[RigConfiguration::PUPPETVAR_EPOOL])) {
            $rigConfiguration->epools = $puppetvars[RigConfiguration::PUPPETVAR_EPOOL];
            if (! empty($puppetvars[RigConfiguration::PUPPETVAR_EPOOL_PORT])) {
                $rigConfiguration->epools .= ' ' . $puppetvars[RigConfiguration::PUPPETVAR_EPOOL_PORT];
            }
        }

        $rigConfiguration->cluster_id = $cluster->id;

        $checksum = $rigConfiguration->checksum();

        $sameConfig = $cluster->configurations()->where('checksum', $checksum);

        if ($sameConfig->count() == 0) {
            $configCount = $cluster->configurations()->count();
            if ($configCount == 0) {
                $rigConfiguration->name = 'Basic configuration';
            }
            else {
                $rigConfiguration->name = 'Configuration #' . ($configCount + 1);
            }
            $rigConfiguration->save();
        }
        else
            $rigConfiguration = $sameConfig->first();


        return $rigConfiguration;
    }

    public function importWallet(Cluster $cluster, BaseCryptocurrency $cryptocurrency, $address)
    {
        if (empty($address))
            return;

        $wallet = $cluster->wallets()->where('address', $address)->where('currency_id', $cryptocurrency->getId())->first();
        if (! $wallet)
            $wallet = $cluster->wallets()->forceCreate(['address' => $address, 'currency_id' => $cryptocurrency->getId(), 'cluster_id' => $cluster->id]);

        return $wallet;
    }

    public function getRigs(Cluster $cluster, Rig $rig = null)
    {
        $owner = $cluster->getOwner();

        $provider = \Adldap::getDefaultProvider();
        /** @var DomainConfiguration $config */
        $config = $provider->getConfiguration();

        $ldapRigs = \Adldap::search()->where('owner', '=', $config->get('account_prefix') . $owner->ldap_id . $config->get('account_suffix'));

        if ($rig)
            $ldapRigs->where('cn', $rig->ldap_id);

        return $ldapRigs->get();
    }

    /**
     * @param $hash
     * @return Computer
     */
    public function getRigByHash($hash)
    {
        return \Adldap::search()->computers()->where('cn', $hash)->first();
    }
}