<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 19.02.2018
 * Time: 2:04
 */

namespace App\Flow\Ldap;

use Adldap\Models\Entry;
use App\Models\Cluster;
use App\Models\Configuration;
use App\Models\Enums\Miner;
use App\Models\Rig;
use App\User;

class Export
{
    /** @return Export */
    public static function create()
    {
        return new static;
    }

    public static function boot()
    {

    }

    protected function basePuppetClasses()
    {
        return [
            RigConfiguration::PUPPETCLASS_BASE,
            RigConfiguration::PUPPETCLASS_FANCONTROL
        ];
    }

    protected function basePuppetVars()
    {
        return [
            RigConfiguration::PUPPETVAR_NTP             => 'ntp.hostcomm.ru',
            RigConfiguration::PUPPETVAR_FAN_MODE        => '0',
            RigConfiguration::PUPPETVAR_FAN_SPEED       => '100',
        ];
    }

    protected function _configurationToLdapVars(Configuration $configuration)
    {
        $configurationPuppetVars = [];

        if ($currency = $configuration->getCurrency()) {
            $configurationPuppetVars[RigConfiguration::WALLET_PREFIX . strtolower($currency->getCode()) . RigConfiguration::WALLET_SUFFIX] = $configuration->wallet->address;
        }

        if ($secondCurrency = $configuration->getSecondCurrency()) {
            $configurationPuppetVars[RigConfiguration::WALLET_PREFIX . strtolower($secondCurrency->getCode()) . RigConfiguration::WALLET_SUFFIX] = $configuration->secondWallet->address;
            $configurationPuppetVars[RigConfiguration::PUPPETVAR_DUAL] = '0';
        }
        else {
            $configurationPuppetVars[RigConfiguration::PUPPETVAR_DUAL] = '1';
        }

        if ($configuration->epools) {
            list($host, $port) = explode(' ', $configuration->epools);
            $configurationPuppetVars[RigConfiguration::PUPPETVAR_EPOOL] = $host;

            if ($port) {
                $configurationPuppetVars[RigConfiguration::PUPPETVAR_EPOOL_PORT] = $port;
            }
        }

        return $configurationPuppetVars;
    }

    protected function _configurationToLdapClasses(Configuration $configuration)
    {
        if ($configuration->miner_id) {
            $configurationPuppetClass = RigConfiguration::getMinerPuppetClass($configuration->getMiner());
        }
        else {
            $configurationPuppetClass = RigConfiguration::PUPPETCLASS_EMPTY_MINER;
        }

        return $configurationPuppetClass;
    }

    public function exportConfiguration(Configuration $configuration, Rig $rig = null)
    {
        $configurationPuppetClass = $this->_configurationToLdapClasses($configuration);
        $configurationPuppetVars = $this->_configurationToLdapVars($configuration);

        if ($rig) {
            $rigs = [$rig];
        }
        else {
            $rigs = $configuration->rigs;
        }

        foreach ($rigs as $configurationRig) {
            $ldapRig = $configurationRig->getLdapRig();

            $this->applyRigPuppet($ldapRig, $configurationRig);

            if ($configurationRig->configuration_checksum != $configuration->checksum) {
                $this->applyConfigurationPuppet($ldapRig, $configurationRig, $configurationPuppetClass, $configurationPuppetVars);

                $configurationRig->configuration_checksum = $configuration->checksum;

                $configurationRig->save();
            }

            $ldapRig->save();
        }
    }

    public function updateRig(Rig $rig)
    {
        $ldapRig = $rig->getLdapRig();
        $configuration = $rig->configuration;

        if (! $configuration) {
            $configurationPuppetClass = RigConfiguration::PUPPETCLASS_EMPTY_MINER;
            $configurationPuppetVars = [];
            $rig->configuration_checksum = null;
        }
        else if ($rig->configuration_checksum != $configuration->checksum) {
            $configurationPuppetClass = $this->_configurationToLdapClasses($configuration);
            $configurationPuppetVars = $this->_configurationToLdapVars($configuration);
            $rig->configuration_checksum = $configuration->checksum;
        }

        if (! empty($configurationPuppetClass))
        	$this->applyConfigurationPuppet($ldapRig, $rig, $configurationPuppetClass, $configurationPuppetVars);

        $this->applyRigPuppet($ldapRig, $rig);

        $rig->save();
        $ldapRig->save();

        return true;
    }

    protected function applyRigPuppet($ldapRig, Rig $rig)
    {
        $puppetClasses = $ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS);

        if (! $puppetClasses)
            $puppetClasses = [];

        foreach ($this->basePuppetClasses() as $baseClass) {
            if (! in_array($baseClass, $puppetClasses))
                $puppetClasses[] = $baseClass;
        }

        $rigTypeClasses = [RigConfiguration::PUPPETCLASS_ASIC, RigConfiguration::PUPPETCLASS_GPU_AMD, RigConfiguration::PUPPETCLASS_GPU_NVIDIA];
        $puppetClasses = array_diff($puppetClasses, $rigTypeClasses);

        if ($rig->getPuppetClass())
            $puppetClasses[] = $rig->getPuppetClass();

        $ldapRig->setAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS, $puppetClasses);

        $puppetVars = RigConfiguration::parseVars($ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS));

        $puppetVars[RigConfiguration::PUPPETVAR_HOSTNAME] = $rig->hostname;

        $mergedVars = array_merge($this->basePuppetVars(), $puppetVars);

        if (in_array(RigConfiguration::PUPPETCLASS_EMPTY_MINER, $puppetClasses)) {//выключаем управление кулером, если риг не майнит
            $puppetVars[RigConfiguration::PUPPETVAR_FAN_MODE] = 0;
            unset($mergedVars[RigConfiguration::PUPPETVAR_FAN_SPEED]);
        }
        else {
            if ($rig->fan_mode) {
                $mergedVars[RigConfiguration::PUPPETVAR_FAN_MODE] = (string) $rig->fan_mode;
            }
            if ($rig->fan_speed) {
                $mergedVars[RigConfiguration::PUPPETVAR_FAN_SPEED] = $rig->fan_speed;
            }

            $overclockOptions = RigConfiguration::overclockOptions();

            foreach ($overclockOptions as $ocOptName) {
                if (mb_strlen($rig->{$ocOptName}) == 0)
                    unset($mergedVars[$ocOptName]);
                else
                    $mergedVars[$ocOptName] = $rig->{$ocOptName};
            }
        }

//        if (isset($mergedVars[RigConfiguration::PUPPETVAR_FAN_SPEED])) {
//            $speed = $mergedVars[RigConfiguration::PUPPETVAR_FAN_SPEED];
//
//            if (is_numeric($speed) && count(explode(' ', $speed)) == 1) {
//                $gpuCount = $rig->getGpus()->count();
//                if($gpuCount > 0) {
//                    $speed = join(' ', array_fill(0, $gpuCount, $speed));
//                }
//            }
//
//            $mergedVars[RigConfiguration::PUPPETVAR_FAN_SPEED] = $speed;
//        }

        $ldapRig->setAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS, RigConfiguration::buildVars($mergedVars));
    }

    protected function applyConfigurationPuppet($ldapRig, $rig, $configurationPuppetClass, $configurationPuppetVars)
    {
        $allMinerClasses = RigConfiguration::allMinerClasses() + [RigConfiguration::PUPPETCLASS_EMPTY_MINER];

        $puppetClasses = $ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS);

        if (! in_array(RigConfiguration::PUPPETCLASS_BASE, $puppetClasses))
            $puppetClasses[] = RigConfiguration::PUPPETCLASS_BASE;

        $puppetClasses = array_diff($puppetClasses, $allMinerClasses);

        if ($configurationPuppetClass)
            $puppetClasses[] = $configurationPuppetClass;

        $puppetVars = RigConfiguration::parseVars($ldapRig->getAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS));

        $puppetVars = array_filter($puppetVars, function($varName){
            return ! (starts_with($varName, RigConfiguration::WALLET_PREFIX) && ends_with($varName, RigConfiguration::WALLET_SUFFIX))
                OR $varName == RigConfiguration::PUPPETVAR_DUAL;
        }, ARRAY_FILTER_USE_KEY);

        $mergedVars = array_merge($puppetVars, $configurationPuppetVars);

        $ldapRig->setAttribute(RigConfiguration::ATTRIBUTE_PUPPETVARS, RigConfiguration::buildVars($mergedVars));
        $ldapRig->setAttribute(RigConfiguration::ATTRIBUTE_PUPPETCLASS, $puppetClasses);
    }

    public function createRig(Rig $rig)
    {
        $data['dn'] = 'cn=' . $rig->getCertName() . ',ou=production,ou=puppet,o=sickle.pro';
        $data['objectClass'] = [
            'top', 'puppetClient', 'ipHost', 'device'
        ];
        $data['cn'] = $rig->getCertName();
        $data['ipHostNumber'] = '192.168.0.5';
        $data['owner'] = Cluster::find($rig->cluster_id)->getOwner()->getLdapUser()->getDn();

        /** @var Entry $ldapEntry */
        $ldapEntry = \Adldap::make()->computer($data);
        $this->applyRigPuppet($ldapEntry, $rig);
        $ldapEntry->setAttribute($ldapEntry->getSchema()->objectClass(), $data['objectClass']);

        $ldapEntry->save();

        return $ldapEntry;
    }

    public function exportUser(User $user, $password)
    {
        $data = [];
        $config = \Adldap::getDefaultProvider()->getConfiguration();
//        $data['owner'] =
//            ($config->get('admin_account_prefix') ? $config->get('admin_account_prefix') : $config->get('account_prefix'))
//            . $config->get('admin_username')
//            .($config->get('admin_account_suffix') ? $config->get('admin_account_suffix') : $config->get('account_suffix'))
//            ;

        foreach(config('adldap_auth.sync_attributes') as $k => $v) {
            $data[$v] = $user->{$k};
        }

        $id = User::max('ldap_id');

        $i = 0;
        do {
            $id++;
            $cnUsed = (\Adldap::search()->users()->where('cn', $id)->get()->count() > 0);
            $i++;
        }
        while ($cnUsed && $i < 10);

        if ($i >= 10) {
            throw new \Exception('Need to sync users');
        }

        $data = array_filter($data);
        $data['cn'] = $id;
        $data['sn'] = $id;

        $hashedPassword = Schema::SSHA($password);
        $data['userPassword'] = $hashedPassword;

        /** @var \Adldap\Models\User $ldapUser */
        $ldapUser = \Adldap::make()->user($data);

        $objectClass = $ldapUser->getAttribute($ldapUser->getSchema()->objectClass());
        $objectClass[3] = $ldapUser->getSchema()->objectClassUser();
        $ldapUser->setAttribute($ldapUser->getSchema()->objectClass(), $objectClass);
//        $ldapUser->setCommonName($user->name);
//        $ldapUser->setDisplayName($user->name);
        $ldapUser->setDn('cn=' . $data['cn'] . ',ou=users,o=sickle.pro');
        $ldapUser->create();

        return $ldapUser;
    }

    public function updateUserPassword($password)
    {

    }

    public function changeEmail()
    {

    }

//    public function deleteRig(Rig $rig)
//    {
//        $ldapEntry = \Adldap::search()->computers()->where('cn', $rig->getCertName());
//
//        $ldapEntry->delete();
//    }

//    public function removeRigOwner($ldapRig)
//    {
//        $ldapRig->setOwner(null);
//        $ldapRig->save();
//    }
}