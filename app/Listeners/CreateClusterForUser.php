<?php

namespace App\Listeners;

use App\Models\Cluster;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateClusterForUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered|Login $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;

        if ($user->clusters()->count() == 0) {
            \DB::transaction(function() use ($user){
                $cluster = new Cluster();
                $cluster->name = 'My farm';
                $cluster->save();

                $cluster->users()->sync([$user->id => ['role' => Cluster::USER_ROLE_OWNER]]);
            });
        }
    }
}
