<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.03.2018
 * Time: 22:03
 */

namespace App\Listeners\Notification;


use App\Notifications\Rig\Offline;
use App\Notifications\Rig\Online;
use Carbon\CarbonInterval;
use Illuminate\Notifications\Events\NotificationSending;

class SendNotMoreOftenThen
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NotificationSending $event)
    {
        switch (get_class($event->notification))
        {
            case Offline::class:
            case Online::class:
                $period = '1 day';
                break;
        }
        $interval = CarbonInterval::createFromDateString($period);

        if ($event->channel == 'mail') {

        }
    }
}