<?php

namespace App\Listeners\Auth;

use App\Models\User\KnownLocation;
use App\Models\User\SecurityLog;
use App\User;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Input;

class CheckLoginLocation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        /** @var User $user */
        if ($event instanceof Attempting) {
            $user = \Auth::getProvider()->retrieveByCredentials($event->credentials);
        }
        else if ($event instanceof Login) {
            $user = $event->user;
        }

        if (! $user OR ! $user->id)
            return true;

        /** @var KnownLocation $location */
        $location = KnownLocation::checkLocationAndSendNotificationProcedure($user);

        if (! $location->confirmed) {
            \Redirect::action('Auth\LocationController@index', ['auth' => encrypt(json_encode(\Request::all()))])->send();

            return false;
        }
    }
}
