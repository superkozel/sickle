<?php

namespace App\Listeners\Auth;

use App\Models\User\SecurityLog;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogFailedLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = User::where('email', $event->credentials['email'])->first();
        if (!$user)
            return;

        SecurityLog::log($user, SecurityLog::EVENT_FAILED);
    }
}
