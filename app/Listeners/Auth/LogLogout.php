<?php

namespace App\Listeners\Auth;

use App\Models\User\SecurityLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if (! $event->user)
            return;

        SecurityLog::log($event->user, SecurityLog::EVENT_LOGOUT);
    }
}
