<?php

namespace App\Listeners;

use Adldap\Configuration\DomainConfiguration;
use Adldap\Connections\Ldap;
use App\Flow\Ldap\Import;
use App\Models\Configuration;
use App\Models\Enums\Cryptocurrency;
use App\Models\Enums\Miner;
use App\Models\Rig;
use App\Models\Wallet;
use App\Services\RigService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoadRigsFromLdap
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;

        RigService::create()->loadUserRigsFromLdap($user);
    }
}
