<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.03.2018
 * Time: 11:05
 */

namespace App\Services;


use App\Flow\Ldap\Import;
use App\Models\Rig;
use App\Notifications\Rig\Offline;
use App\Notifications\Rig\Online;

class RigService
{
    /**
     * @return static
     */
    public static function create()
    {
        return new static;
    }

    /**
     * @param Rig $rig
     * @param bool $force
     */
    public function reboot(Rig $rig, $force = false)
    {
        $ldap = $rig->getLdapRig();
        $ldap->addPuppetVar($force ? 'my_pwr_freboot' : 'my_pwr_reboot', time());
        return $ldap->save();
    }

    /**
     * @param Rig $rig
     */
    public function shutdown(Rig $rig)
    {
        $ldap = $rig->getLdapRig();
        $ldap->addPuppetVar('my_pwr_shutdown', time());
        return $ldap->save();
    }

    /**
     * @return string
     */
    public static function generateCertname()
    {
        $timestamp = time();
        $tries = 0;
        $maxTries = 10;

        do {
            $hash = random_hex_string(\App\Models\Rig::HASH_LENGTH - mb_strlen($timestamp)) . $timestamp;
            $tries++;
        }
        while(\App\Models\RigRegistration::where('hash', $hash)->count() > 0 && $tries < $maxTries);

        if ($tries < $maxTries) {
            return $hash;
        }
        else {
            throw new Exception('Too many collisions');
        }
    }

    /**
     * @param Rig $rig
     * @param $online
     */
    public function setOnlineStatus(Rig $rig, $online)
    {
        if ($online && ! $rig->online) {
            $rig->online = true;

            $notification = new Online($rig);
            $rig->cluster->notify($notification);

            $rig->save();
        }
        else if (! $online && $rig->online) {
            $rig->online = false;

            $notification = new Offline($rig);
            $rig->cluster->notify($notification);

            $rig->save();
        }
    }

    public function delete(Rig $rig)
    {
        if ($rig->isGpuRig()) {
            $ldapRig = $rig->getLdapRig();
            if ($ldapRig) {
                Export::create()->removeRigOwner($rig->getLdapRig());
                $rig->save();
            }
            else
                $rig->delete();
        }
    }

    /**
     * Importing
     */
    public function loadUserRigsFromLdap($user)
    {
        $import = Import::create();
        foreach ($user->clusters as $cluster) {
            $rigs = $import->getRigs($cluster);

            $import->importRigs($cluster, $rigs);
        }
    }
}