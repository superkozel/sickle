<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 08.07.2018
 * Time: 7:51
 */

namespace App\Events\Auth;


use App\Models\User\KnownLocation;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class LocationConfirmed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var KnownLocation
     */
    public $location;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(KnownLocation $location)
    {
        $this->location = $location;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('location.' . $this->location->id);
    }

    public function broadcastWith()
    {
        return [
            'id' => $this->location->id
        ];
    }

    public function broadcastAs()
    {
        return 'location.confirmed';
    }
}