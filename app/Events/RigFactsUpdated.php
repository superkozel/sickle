<?php

namespace App\Events;

use App\Models\Rig;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RigFactsUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Rig
     */
    public $rig;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Rig $rig)
    {
        $this->rig = $rig;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('cluster.' . $this->rig->cluster_id);
    }

    public function broadcastWith()
    {
        $data = $this->rig->toArray();
        $data['facts'] = array_only($data, ['gpus']);

        return [
            'rig' => $data
        ];
    }
}
