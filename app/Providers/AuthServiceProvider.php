<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-rig', function ($user, $rig) {
            $cluster = $rig->cluster;
            return $cluster->canRead($user);
        });
        Gate::define('control-rig', function ($user, $rig) {
            $cluster = $rig->cluster;
            return $cluster->canWrite($user);
        });

        Gate::define('view-configuration', function ($user, $configuration) {
            $cluster = $configuration->cluster;
            return $cluster->canRead($user);
        });
        Gate::define('control-configuration', function ($user, $configuration) {
            $cluster = $configuration->cluster;
            return $cluster->canWrite($user);
        });

        Gate::define('view-wallet', function ($user, $wallet) {
            $cluster = $wallet->cluster;
            return $cluster->canRead($user);
        });
        Gate::define('control-wallet', function ($user, $wallet) {
            $cluster = $wallet->cluster;
            return $cluster->canWrite($user);
        });

        Gate::define('control-cluster', function ($user, $cluster) {
            return $cluster->isOwner($user);
        });
        Gate::define('view-cluster', function ($user, $cluster) {
            return $user->clusters()->wherePivot('cluster_id', $cluster->id)->count() > 0;
        });
        Gate::define('unlist-cluster', function ($user, $cluster) {
            return ! $cluster->isOwner($user);
        });
    }
}
