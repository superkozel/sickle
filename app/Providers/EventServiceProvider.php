<?php

namespace App\Providers;

use App\Listeners\Auth\CheckLoginLocation;
use App\Listeners\CreateClusterForUser;
use App\Listeners\CreateLdapUser;
use App\Listeners\LoadRigsFromLdap;
use App\Listeners\Auth\LogFailedLogin;
use App\Listeners\Auth\LogLockout;
use App\Listeners\Auth\LogLogout;
use App\Listeners\Auth\LogSuccessfulLogin;
use Illuminate\Notifications\Events\NotificationSending;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        \Illuminate\Auth\Events\Registered::class => [
            CreateClusterForUser::class,
            LoadRigsFromLdap::class
        ],
        \Illuminate\Auth\Events\Attempting::class => [
        ],
        \Illuminate\Auth\Events\Login::class => [
            LogSuccessfulLogin::class,
            CreateClusterForUser::class,//инициализация аккаунта
            LoadRigsFromLdap::class,//импорт из лдап
            CheckLoginLocation::class,//проверка места авторизации
        ],
        \Illuminate\Auth\Events\Failed::class => [
            LogFailedLogin::class,
        ],
        \Illuminate\Auth\Events\Logout::class => [
            LogLogout::class,
        ],
        \Illuminate\Auth\Events\Lockout::class => [
            LogLockout::class,
        ],
        'App\Events\Wallet\Changed' => [
            'App\Listeners\SendWalletChangedNotification',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
