<?php

namespace App\Providers;

use App\Models\Enums\Cryptocurrency;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        \Validator::extend('currency_address', function($attribute, $value, $parameters, $validator) {
            /** @var BaseCryptocurrency $currency */
            $currency = Cryptocurrency::findByCode($parameters[0]);

            return $currency->walletValidationRule($value);
        });

        \Validator::extend('latin', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/[A-Za-z0-9_]/u', $value);
        });

        \Validator::extend('telegram', function ($attribute, $value, $parameters, $validator) {
            if (! $value)
                return true;

            $first = $value[0];
            if ($first == '@')
                $value = mb_substr($value, 1);

            if (mb_strlen($value) < 5 OR mb_strlen($value) > 32)
                return false;

            $ok = preg_match('/[A-Za-z0-9_]/u', $value);

            $validator->{$attribute} = '@' . $value;

            return $ok;
        }, 'Invalid Telegram user name');

        \Validator::extend('check_password', function($attribute, $value){
            return \Hash::check($value, \Auth::user()->getAuthPassword());
        }, 'Invalid password');

//        Horizon::auth(function ($request) {
//            return true;
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
