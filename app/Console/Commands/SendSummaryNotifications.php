<?php

namespace App\Console\Commands;

use App\Models\Cluster;
use App\Notifications\Cluster\Summary;
use Illuminate\Console\Command;

class SendSummaryNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $period = '1';

        Cluster::chunk(100, function($clusters) {
            foreach ($clusters as $cluster) {
                $cluster->getOwner()->notify(new Summary($cluster));
            }
        });
    }
}
