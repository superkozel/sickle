<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 2:07
 */

namespace App\Console\Commands;


use App\Flow\Api\Coinbase;
use App\Flow\Api\Cryptocompare;
use App\Models\ActiveRecord\CurrencyRate;
use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Cryptocurrency;
use App\Models\Enums\Currency;
use Illuminate\Console\Command;

class CryptocurrencyRateUpdate extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:rate_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update currency rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Cryptocurrency::all() as $cryptoCurrency) {
            /** @var BaseCryptocurrency $cryptoCurrency */
            try {
                $currencies = array_keys(Currency::all());
                $rates = Cryptocompare::getRate($cryptoCurrency->getCode(), $currencies);

                if ($rates !== false) {
                    foreach ($currencies as $realCurrencyCode) {
                        if (! empty($rates[$realCurrencyCode])) {
                            $rate = $rates[$realCurrencyCode];
                            CurrencyRate::saveRate($cryptoCurrency->getCode(), $realCurrencyCode, $rate);
                        }
                    }
                }
            }
            catch (\ErrorException $e) {
                var_dump($e);
            }
        }
    }
}