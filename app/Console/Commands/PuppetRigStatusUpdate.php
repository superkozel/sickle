<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 16.03.2018
 * Time: 8:47
 */

namespace App\Console\Commands;

use App\Events\RigFactsUpdated;
use App\Flow\Api\Graphite;
use App\Flow\Puppet\Api;
use App\Models\Rig;
use App\Notifications\Rig\GpuIsMissing;
use App\Notifications\Rig\NotMining;
use App\Notifications\Rig\Offline;
use App\Notifications\Rig\Online;
use Carbon\Carbon;
use Doctrine\DBAL\Schema\Visitor\Graphviz;
use Illuminate\Console\Command;

class PuppetRigStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rig:status_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Rig::select()->chunk(100, function($rigs) {
            $rigs->load('cluster');
            $rigs->keyBy('id');
            $certnames = $rigs->pluck('ldap_id', 'id')->all();

            $reports = collect(Api::reports([
                ['certname', $certnames],
                ['receive_time', '>', date('c', strtotime('-5 minute'))]
            ]))->keyBy('certname');

            $onlineCertnames = $reports->keys()->all();
            $facts = Api::assoc(Api::getRigsFacts($certnames, ['gpus', 'load_averages', 'memory', 'system_uptime', 'processors', 'ipaddress', 'disks', 'operatingsystem']));

            foreach ($rigs as $rig) {
                /** @var Rig $rig */

                $rig->online = (in_array($rig->ldap_id, $onlineCertnames));

                if ($rig->online) {
                    $rig->online_at = Carbon::now();

                    $rigFacts = array_get($facts, $rig->ldap_id);

                    if (! empty($rigFacts['gpus'])) {
                        $gpuCount = count($rigFacts['gpus']);
                        $hashrate = 0;
                        $hashrate2 = 0;
                        $temperature = 0;

                        foreach ($rigFacts['gpus'] as $gpu) {
                            $hashrate += is_numeric(array_get($gpu, 'eth_h')) ? array_get($gpu, 'eth_h') / 1000 : 0;
                            $hashrate2 += is_numeric(array_get($gpu, 'dcr_h')) ? array_get($gpu, 'dcr_h') / 1000 : 0;
                            $temperature = max($temperature, array_get($gpu, 'temp'));
                        }

                        $rig->hashrate = round($hashrate, 3);
                        $rig->second_hashrate = round($hashrate2, 3);
                        $rig->temperature = $temperature;

                        if (! $rig->gpu_count)
                            $rig->gpu_count = $gpuCount;
                        else {
                            if ($rig->gpu_count > $gpuCount) {
                                $rig->missing_gpu_count = $rig->gpu_count - $gpuCount;
                            }
                            else {//если увеличилось кол-во карт
                                $rig->gpu_count = $gpuCount;
                                $rig->missing_gpu_count = 0;
                            }
                        }

                        $rig->facts = $rigFacts;
                        $rig->reported_at = Carbon::createFromFormat('Y-m-d\TH:i:s.uO', $reports[$rig->getCertName()]['receive_time']);

                        if ($rig->wasChanged(['missing_gpu_count', 'gpu_count'])) {
//                            event(new RigGpuCountChanged());
                            if ($rig->getOriginal('gpu_count') > $rig->gpu_count) {
                                $notification = new GpuIsMissing($rig, $rig->gpu_count, $rig->gpu_count - $rig->missing_gpu_count);
                                $rig->cluster->notify($notification);
                            }
                        }
                    }
                }
                else {
                    $rig->hashrate = 0;
                    $rig->second_hashrate = 0;
                    $rig->temperature = null;
                    $rig->facts = [];
                }

                event(new RigFactsUpdated($rig));

//                $notification = new NotMining($rig);
//                $rig->cluster->notify($notification);
                if ($rig->online && $rig->wasChanged('hashrate') && $rig->hashrate == 0) {
                    $notification = new NotMining($rig);
                    $rig->cluster->notify($notification);
                }

                if ($rig->wasChanged(['online'])) {
                    if (! $rig->online) {
                        $notification = new Offline($rig);
                    } else {
                        $notification = new Online($rig);
                    }

                    $rig->cluster->notify($notification);
                }

                $rig->save();
            }

//            $timestamp = time();

//            $graphite = new Graphite();
//            $graphite->open();
//            foreach ($rigs as $rig) {
//                $hostname = $rig->hostname;
//                $rigFacts = array_get($facts, $rig->ldap_id);
//                foreach ($rigFacts['gpus'] as $h) {
//                    $i = 0;
//                    foreach($h['value'] as $g){
//                        $msg[$hostname . ".gpus.gpu" . $i . ".temp"]  = $g["temp"];
//                        $msg[$hostname . ".gpus.gpu" . $i . ".hashrate"] = $g["eth_h"];
//                        $msg[$hostname . ".gpus.gpu" . $i . ".hashrate2"] = $g["dcr_h"];
//                        $msg[$hostname . ".gpus.gpu" . $i . ".power"] = $g["power"];
//                        $i++;
//                    };
//                }
//
//                foreach ($rigFacts['load_averages'] as $h) {
//                    $msg[$hostname . ".la.5m"] = $h['value']['5m'];
////                    $msg[] =  . $hostname . ". " .  . " " . $timestamp. "\n";
//                }
//
//                foreach ($rigFacts['memory'] as $h) {
//                    $hostname = $h['certname'];
//                    foreach($h['value'] as $g){
//                        $msg[$hostname . ".memory.used"] = $g["used_bytes"];
//                        $msg[$hostname . ".memory.avail"] = $g["available_bytes"];
//                    };
//
//                }
//
//                $graphite->sendBulk($msg, $timestamp);
//            }
//            $graphite->close();
        });
    }
}