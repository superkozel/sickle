<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2018
 * Time: 17:52
 */

namespace App\Notifications;

use App\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

abstract class BaseNotification extends Notification
{
    public function getType()
    {
        return get_class($this);
    }

    public function mailToTelegram($notifiable)
    {
        /** @var MailMessage $mail */
        $mail = $this->toMail($notifiable);

        /** @var TelegramMessage $message */
        $message = TelegramMessage::create();

        $message
//            ->to($this->user->telegram_user_id) // Optional.
            ->content(join("\n", $mail->introLines) . "\n" . join("\n", $mail->outroLines)) // Markdown supported.
            ->button($mail->actionText, $mail->actionUrl)// Inline Button
        ;

        return $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = $this->channels($notifiable);

        $via = [];
        if (in_array('mail', $channels)) {
            if ($notifiable->routeNotificationFor('mail'))
                $via[] = 'mail';
        }
        if ((in_array('telegram', $channels) || in_array(TelegramChannel::class, $channels))) {
            if ($notifiable->routeNotificationForTelegram())
                $via[] = TelegramChannel::class;
        }
        if (in_array('database', $channels))
            $via[] = 'database';

        return $via;
    }
}