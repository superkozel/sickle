<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2018
 * Time: 3:48
 */

namespace App\Notifications\Rig;


use App\Models\Rig;
use App\Notifications\RigNotification;
use App\Models\User\NotificationSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class Offline extends RigNotification
{
    use Queueable;

    /** @var Rig */
    protected $rig;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Rig $rig)
    {
        $this->rig = $rig;
    }

    /**
     * @return string
     */
    public function getSettingsId()
    {
        return NotificationSettings::RIG_OFFLINE;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line('Rig "' . $this->rig->getDisplayName() . '" is offline')
            ->action('Go to rig', $this->rig->getUrl())
            ;
    }

    public function toArray($notifiable)
    {
        return [
            'id' => $this->rig->id
        ];
    }

    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }
}