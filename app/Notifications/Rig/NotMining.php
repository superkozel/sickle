<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 24.05.2018
 * Time: 18:53
 */

namespace App\Notifications\Rig;


use App\Models\Rig;
use App\Notifications\RigNotification;
use App\Models\User\NotificationSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class NotMining extends RigNotification
{
    use Queueable;

    /**
     * @var string
     */
    protected $error;

    /**
     * @var Rig
     */
    protected $rig;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($rig)
    {
        $this->rig = $rig;
    }

    public function getSettingsId()
    {
        return NotificationSettings::RIG_NOT_MINING;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line('Rig "' . $this->rig->getDisplayName() . '" is not mining')
            ->action('Go to rig', $this->rig->getUrl())
            ;
    }

    public function toArray($notifiable)
    {
        return [
            'id' => $this->rig->id
        ];
    }

    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }
}