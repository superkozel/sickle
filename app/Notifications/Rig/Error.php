<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 24.05.2018
 * Time: 15:14
 */

namespace App\Notifications\Rig;


use App\Notifications\RigNotification;
use App\Models\User\NotificationSettings;
use Illuminate\Bus\Queueable;

class Error extends RigNotification
{
    use Queueable;

    /**
     * @var string
     */
    protected $error;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($error)
    {
        $this->error = $error;
    }

    public function getSettingsId()
    {
        return NotificationSettings::RIG_ERROR;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Rig "' . $this->rig->getDisplayName() . '" mining error')
            ->line($this->message->getTypeName(), $this->message->content)
            ->action('Go to rig', $this->message->rig->getUrl())
            ;
    }

    public function toArray($notifiable)
    {
        return [
            'id' => $this->message->id
        ];
    }

    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }
}