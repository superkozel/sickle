<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2018
 * Time: 3:51
 */

namespace App\Notifications\Rig;


use App\Models\Rig;
use App\Notifications\BaseNotification;
use App\Notifications\RigNotification;
use App\Models\User\NotificationSettings;
use Illuminate\Bus\Queueable;
use NotificationChannels\Telegram\TelegramMessage;

class GpuIsMissing extends RigNotification
{
    use Queueable;

    /** @var Rig */
    protected $rig;
    /** @var int $before */
    protected $before;
    /** @var int $after */
    protected $after;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Rig $rig, $before, $after)
    {
        $this->rig = $rig;
        $this->before = $before;
        $this->after = $after;
    }

    public function getSettingsId()
    {
        return NotificationSettings::RIG_GPU_MISSING;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Some of your gpus are missing from rig "' . $this->rig->getDisplayName())
            ->line($this->rig->name . ', gpu count before:' . $this->before . ', now:'  . $this->after)
            ->action('Confirm GPU permanent count change: ', action('Auth\RigController@confirmGpuCount'))
            ;
    }


    public function toArray($notifiable)
    {
        return [
            'id' => $this->rig->id,
            'before' => $this->before,
            'after' => $this->after,
        ];
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to($this->user->telegram_user_id) // Optional.
            ->content("*HELLO!* \n Some of your gpus are missing from your rig!") // Markdown supported.
            ->button('Its alright', action('Auth\RigController@confirmGpuCount')); // Inline Button
    }
}