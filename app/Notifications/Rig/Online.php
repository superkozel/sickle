<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.03.2018
 * Time: 17:28
 */

namespace App\Notifications\Rig;

use App\Models\Rig;
use App\Models\User\NotificationSettings;
use App\Notifications\RigNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class Online extends RigNotification
{
    use Queueable;

    /** @var Rig */
    protected $rig;

    protected $settingsId = NotificationSettings::RIG_ONLINE;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Rig $rig)
    {
        $this->rig = $rig;
    }

    /**
     * @return string
     */
    public function getSettingsId()
    {
        return NotificationSettings::RIG_ONLINE;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line('Rig "' . $this->rig->getDisplayName() . '" is online')
            ->action('Go to rig', $this->rig->getUrl())
            ;
    }

    public function toArray($notifiable)
    {
        return [
            'id' => $this->rig->id
        ];
    }

    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }
}