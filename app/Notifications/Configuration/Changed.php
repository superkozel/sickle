<?php

namespace App\Notifications\Configuration;

use App\Models\Configuration;
use App\Notifications\BaseNotification;
use App\Notifications\SecurityNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;

class Changed extends SecurityNotification
{
    use Queueable;

    /** @var Configuration */
    protected $configuration;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Wallet address changed')
            ->line('Configuration of your rigs has been changed.')
            ->line($this->configuration->getChanges())
            ->action('Edit configuration', $this->configuration->getUrl())
            ;
    }

    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            ''
        ];
    }
}
