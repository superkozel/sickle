<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.05.2018
 * Time: 19:13
 */

namespace App\Notifications;


use App\User;

abstract class RigNotification extends BaseNotification
{
    protected $settingsId = 'DEFINE ME';

    function getSettingsId()
    {
        if ($this->settingsId == 'DEFINE ME')
            throw new \Exception('Undefined setting Id for notification');

        return $this->settingsId;
    }

    public function checkInterval(User $user)
    {

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = parent::via($notifiable);

        if ($notifiable instanceof User) {
            $settings = $notifiable->notification_settings;

            foreach ($via as $k => $channel) {
                if (! $settings->channelEnabled($channel)) {
                    unset($via[$k]);
                }
            }
        }

        return $via;
    }

    public function allows(User $user)
    {
        $settings = $user->notification_settings;

        if ($settings->getDisableAll())
            return false;

        return ($settings->notificationEnabled($this->getSettingsId()));
    }
}