<?php

namespace App\Notifications\Auth;

use App\Models\User\KnownLocation;
use App\Notifications\BaseNotification;
use App\Notifications\SecurityNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LoginFromUnknownLocation extends SecurityNotification
{
    use Queueable;

    /** @var KnownLocation */
    protected $location;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($location)
    {
        $this->location = $location;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram', 'database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'ip' => $this->location->ip,
            'user_agent' => $this->location->user_agent
        ];
    }
}
