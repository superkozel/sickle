<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.02.2018
 * Time: 19:50
 */

namespace App\Notifications\Auth;


use App\Models\User\KnownLocation;
use App\Notifications\BaseNotification;
use App\Notifications\SecurityNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class ConfirmNewLocation extends SecurityNotification
{
    use Queueable;

    protected $location;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(KnownLocation $location)
    {
        $this->location = $location;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Someone has logged with your account from new location: ' . $this->location->user_agent . '[' . $this->location->ip . ']')
            ->action('Confirm this action', action('Auth\LocationController@confirm', ['email_token' => $this->location->email_token]))
            ->line('Thank you for using our application!');
    }

    public function toArray($notifiable)
    {
        return [];
    }
}