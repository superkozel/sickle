<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2018
 * Time: 3:49
 */

namespace App\Notifications\Wallet;

use App\Models\Wallet;
use App\Notifications\BaseNotification;
use App\Notifications\SecurityNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;

class Changed extends SecurityNotification
{
    use Queueable;

    /** @var Wallet */
    protected $wallet;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->line('Wallet address has been changed')
            ->line('old adress: [' . $this->wallet->getOriginal('address'). '], new address: [' . $this->wallet->address . ']')
            ->action('Edit wallet', $this->wallet->getEditUrl())
            ;
    }

    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->wallet->id,
            'currency' => $this->wallet->getCurrency()->getCode(),
            'old_address' => $this->wallet->getOriginal('address'),
            'new address' => $this->wallet->getAttribute('address'),
        ];
    }
}