<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2018
 * Time: 23:55
 */

namespace App\Notifications\Cluster;

use App\Models\Cluster;
use App\Models\RigMessage;
use App\Notifications\BaseNotification;
use App\Notifications\RigNotification;
use App\Models\User\NotificationSettings;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class Summary extends RigNotification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Cluster $cluster)
    {
        $this->cluster = $cluster;
    }


    public function getSettingsId()
    {
        return NotificationSettings::RIG_SUMMARY;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function channels($notifiable)
    {
        return ['mail', 'telegram'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $msg = new MailMessage();

        $offlineRigs = $this->cluster->rigs()->where('online', 0)->count();;
        $notMiningRigs = $this->cluster->rigs()->where('online', 1)->where('hashrate', 0)->count();
        $errors = $this->cluster->rigs()->messages()->where(RigMessage::TYPE_ERROR)->where('created_at > 1')->count();
        $warnings = $this->cluster->rigs()->messages()->where(RigMessage::TYPE_WARNING)->count();

        if ($offlineRigs == 0 && $notMiningRigs == 0 && $errors == 0 && $warnings == 0)
        {
            $msg->line('Everything is OK');
        }
        else {
            if ($offlineRigs) {
                $msg->line($offlineRigs . ' rig is offline');
            }
            if ($notMiningRigs) {
                $msg->line($notMiningRigs . ' rig is offline');
            }
            if ($errors) {
                $msg->line($errors . ' errors');
            }
            if ($warnings) {
                $msg->line($warnings . ' warnings');
            }
        }

        return $msg
            ->action('Notification Action', action('Personal\IndexController@index'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toTelegram($notifiable)
    {
        return $this->mailToTelegram($notifiable);
    }
}