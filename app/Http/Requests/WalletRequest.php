<?php

namespace App\Http\Requests;

use App\Models\Cluster;
use App\Models\Enums\Cryptocurrency;
use Illuminate\Foundation\Http\FormRequest;
use PHPUnit\Framework\Constraint\Callback;

class WalletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('control-cluster', Cluster::findOrFail($this->cluster_id)) && (! $this->wallet OR \Gate::allows('control-wallet', $this->wallet));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->wallet)
            $this->currency_id = $this->wallet->currency_id;

        $rules = [
            'address' => 'currency_address:' . Cryptocurrency::find($this->currency_id)->getCode() . '|unique_with:wallets,cluster_id' . ($this->wallet ? ',' . $this->wallet->id : ''),
        ];

        if (! $this->wallet) {
            $rules['currency_id'] = 'in:' . join(',', array_keys(Cryptocurrency::getNames()));
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'address.currency_address' => 'Incorrect ' . Cryptocurrency::find($this->currency_id)->getName() . ' wallet address'
        ];
    }
}
