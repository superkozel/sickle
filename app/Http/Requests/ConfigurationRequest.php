<?php

namespace App\Http\Requests;

use App\Models\Cluster;
use App\Models\Enums\Cryptocurrency;
use App\Models\Enums\Miner;
use App\Models\Wallet;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class ConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        return \Gate::allows('control-cluster', Cluster::findOrFail($this->cluster_id))
            && (! $this->configuration OR \Gate::allows('control-configuration', $this->configuration));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return static::allRules($this);
    }

    public static function allRules($request, $subForm = null)
    {
        static::moreValidators($request, $subForm);

        $rules = [
            'name' => 'required|string',
            'cluster_id' => '',
            'currency_id' => 'required|currency',
            'second_currency_id' => 'currency',
            'wallet_address' => 'required|currency_address:' .  Cryptocurrency::find($request->getSubFormData('currency_id', $subForm))->getCode(),
            'second_wallet_address' => 'required_with:second_currency_id' . ($request->getSubFormData('second_currency_id', $subForm) ? ('|currency_address:' . Cryptocurrency::find($request->getSubFormData('second_currency_id', $subForm))->getCode()) : ''),
            'miner_id' => 'available_miner:currency_id,second_currency_id'
        ];

        if ($minerId = $request->getSubFormData('miner_id', $subForm)) {
            $miner = Miner::find($minerId);

            if ($miner) {
                $minerRules = $miner->getValidationRules($request);
                $minerRules = array_combine(
                    array_map(function($k) use ($subForm){return $subForm ? $subForm . '.' . $k : $k;}, array_keys($minerRules)),
                    $minerRules
                );
                $rules = array_merge($rules, $minerRules);
            }
        }

        return $rules;
    }

    public function getCurrency()
    {
        return Cryptocurrency::find($this->currency_id);
    }

    public function getSecondCurrency()
    {
        return Cryptocurrency::find($this->second_currency_id);
    }

    public function messages()
    {
        return [
            'wallet_address.currency_address' => 'Invalid ' . $this->getCurrency()->getCode() . ' wallet address',
            'second_wallet_id.currency_address' => 'Invalid ' . ($this->getSecondCurrency() ? $this->getSecondCurrency()->getCode() : '') . ' wallet address',
            'miner_id.available_miner' => 'You dont own this wallet',
        ];
    }

    static function moreValidators($request)
    {
        \Validator::extend('currency', function($attribute, $value, $parameters, $validator) {
            if (! $value)
                return true;

            return Cryptocurrency::find($value);
        });
        \Validator::extend('available_miner', function($attribute, $value, $parameters, $validator) {
            if (! $value)
                return true;

            $miner = Miner::find($value);

            $prefix = explode('.', $attribute);
            array_pop($prefix);
            if (count($prefix))
                $path = join('.', $prefix) . '.';
            else
                $path = '';

            return $miner->canMine(array_get($validator->getData(), $path . $parameters[0]), array_get($validator->getData(), $path . $parameters[1]));
        });
        \Validator::extend('user_controls_wallet', function($attribute, $value, $parameters, $validator) use ($request) {
            $wallet = Cluster::find($validator->getData()['cluster_id'])->wallets()->find($value);

            return \Gate::allows('control-wallet', $wallet);
        });
    }

    public function getSubFormData($field, $subForm = null)
    {
        if ($subForm)
            return array_get($this->all(), $subForm . '.' . $field);
        else
            return $this->get($field);
    }
}
