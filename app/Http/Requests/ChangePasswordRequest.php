<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.03.2018
 * Time: 12:33
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return static::allRules($this);
    }

    public static function allRules($request, $subForm = null)
    {
        \Validator::extend('not_same', function($attribute, $value, $options = [], $validator){
            /** @var \Validator $validator */
            return $value != $validator->getData()[$options[0]];
        }, 'New password must differ');

        return
        [
            'old_password' => 'required|check_password',
            'new_password' => 'required|confirmed|not_same:old_password',
            'new_password_confirmation' => 'required:',
        ];
    }
}
