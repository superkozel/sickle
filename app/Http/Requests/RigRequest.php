<?php

namespace App\Http\Requests;

use Adldap\Connections\Ldap;
use App\Flow\Puppet\Module\FanControl;
use App\Models\Cluster;
use App\Models\Configuration;
use App\Models\Rig;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RigRequest extends FormRequest
{
    /** @var Cluster */
    protected $cluster;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->moreValidators();

        $rules = [
            'cluster_id' => '',
            'hostname' => 'required|string|min:3',
            'name' => '',
            'description' => '',
            'type' => [
                'required',
                Rule::in(array_keys(Rig::getOptionsTypes())),
            ],
            'configuration_id' => '',

            'fan_mode' => [
                'required',
                Rule::in(array_keys(FanControl::getEnabledOptions())),
            ],
            'fan_speed' => 'required_if:fan_mode,' . FanControl::MODE_MANUAL . '|fan_speed',

            'nv_graphicsclockoffset' => 'gpu_option',
            'nv_memorytransferrateoffset' => 'gpu_option',
            'nv_powerlimit' => 'gpu_option',

            'amd_core_clock' => 'gpu_option:digits,3-4,positive',
            'amd_core_state' => 'gpu_option:minmax,1-7,positive',
            'amd_core_voltage' => 'gpu_option:digits,1-5,positive',
            'amd_memory_clock' => 'gpu_option:digits,3-4,positive',
            'amd_mem_state' => 'gpu_option:digits,1,positive',
            'amd_voltage_state' => 'gpu_option:minmax,0-15,positive',

            'g-recaptcha-response' => (config('captcha.enabled')) && ! $this->rig->id ? 'required|captcha' : ''//капча при добавлении рига от брутфорса
        ];

        if (! $this->rig) {
            $rules['certname'] = [
                'required',
                Rule::exists('rig_registrations', 'hash')->where(function ($query) {
                    $query->whereNull('rig_id');
                    $query->where('email', \Auth::user()->email);
                }),
            ];
        }
        else if ($this->configuration_id === 0) {
            $configurationRules = ConfigurationRequest::allRules($this, 'Configuration');
            foreach ($configurationRules as $k => $rule) {
                $rules['Configuration.' . $k] = $rule;
            }
        }

        return $rules;
    }

    function messages()
    {
        return [
            'hash.exists' => 'Rig with this hash is not registered',
            'fan_speed.fan_speed' => 'Fan speed must be 0-100 number or array of 0-100 numbers with length equal to GPUs count, delimeted by space(example: 100 67 100 50)',
            'gpu_option' => 'Invalid overclock option value, please check info above',
        ];
    }

    function moreValidators()
    {
        \Validator::extend('fan_speed', function($attribute, $value, $parameters) {
            $values = preg_split('/\s+/', $value);
            $gpuCount = $this->rig->getGpus()->count();

            foreach ($values as &$v) {
                if (! is_numeric($v) OR $v < 0 OR $v > 100)
                    return false;

                $v = intval($v);
            }

            if (count($values) != 1 && count($values) != $gpuCount)
                return false;

            $this->{$attribute} = join(' ', $values);

            return true;
        });

        \Validator::extend('gpu_option', function($attribute, $value, $parameters) {
            if (is_null($value))
                return true;

            $values = preg_split('/\s+/', $value);

            $gpuCount = $this->rig->getGpus()->count();

            if (count($values) == 0)
                return true;

            if (count($values) > 1 && count($values) != $gpuCount)
                return false;

            foreach ($values as &$v) {
                if (! preg_match('/^-?[0-9]*$/', $v))
                    return false;

                foreach ($parameters as $k => $rule) {
                    switch ($rule)
                    {
                        case 'minmax':
                            list($from,$to) = explode('-', $parameters[$k + 1]);
                            if ($v < $from OR $v > $to)
                                return false;

                            break;
                        case 'digits':
                            $size = explode('-', $parameters[$k + 1]);

                            if (count($size) > 1) {
                                if (strlen($v) < $size[0] OR strlen($v) > $size[1])
                                    return false;
                            }
                            else {
                                if (strlen($v) != $size[0])
                                    return false;
                            }

                            break;
                        case 'positive':
                            return $v > 0;

                            break;
                    }
                }
            }

            $this->$attribute = join(' ', $values);

            return true;
        });

        \Validator::extend('user_controls_configuration', function($attribute, $value, $parameters) {
            $config = Cluster::find($this->cluster_id)->configurations()->find($value);

            return \Gate::allows('control-configuration', $config);
        });
    }

    public function getSubFormData($field, $subForm = null)
    {
        if ($subForm)
            return array_get($this->all(), $subForm . '.' . $field);
        else
            return $this->get($field);
    }

    public function getCluster()
    {
        return $this->cluster;
    }
}
