<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 21.02.2018
 * Time: 19:09
 */

namespace App\Http\Requests;


use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Cryptocurrency;
use App\Models\Wallet;
use Illuminate\Foundation\Http\FormRequest;

class DeleteWalletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('control-wallet', $this->wallet);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    }
}