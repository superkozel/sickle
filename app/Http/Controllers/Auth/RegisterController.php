<?php

namespace App\Http\Controllers\Auth;

use App\Flow\Ldap\Export;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/personal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        Validator::extend('ldap_unique', function ($attribute, $value, $parameters, $validator) {
            return \Adldap::search()->users()->where('mail', $value)->get()->count() == 0;
        }, 'User already exist');

        return Validator::make($data, [
//            'name' => 'required|string|max:60|latin',
            'email' => 'required|string|email|max:255|unique:users|ldap_unique',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => (config('captcha.enabled')) ? 'required|captcha' : ''
        ], ['name.latin' => 'Name must be english letters, numbers with underscores']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::make([
//            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $ldapUser = Export::create()->exportUser($user, $data['password']);

        if ($ldapUser) {
            try {
                $user->setLdapUser($ldapUser);
                $user->ldap_id = $ldapUser->cn[0];

                $user->save();

                return $user;
            }
            catch (\Exception $e) {
                $ldapUser->delete();

                throw $e;
            }
        }
        else {
            throw new \Exception('Failed to create ldap user');
        }
    }
}
