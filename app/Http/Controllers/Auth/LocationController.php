<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.02.2018
 * Time: 19:11
 */

namespace App\Http\Controllers\Auth;

use App\Events\Auth\LocationConfirmed;
use App\Http\Controllers\Controller;
use App\Models\User\KnownLocation;
use App\User;

class LocationController extends Controller
{
    public function index()
    {
        $user = $this->user;

        $authData = array();
        if (! $user) {
            if (\Request::has('auth')) {
                $authData = json_decode(decrypt(\Request::get('auth')), true);

                $loginController = app(LoginController::class);
                $user = \Auth::getProvider()->retrieveByCredentials(array_only($authData, $loginController->username()));
            }
        }

        if (! $user)
            return \Redirect::action('Auth\LoginController@login');

        /** @var KnownLocation $location */
        $location = KnownLocation::checkLocationAndSendNotificationProcedure($user);

        $intended = ! empty($authData) ?
            action('Auth\LoginController@login') :
            ((\Request::has('intended')) ? decrypt(\Request::get('intended')) : action('Personal\IndexController@index'));

        return view('auth.location.confirm', [
            'user' => $user,
            'location' => $location,
            'intended' => $intended,
            'auth' => encrypt(json_encode(array_except($authData, ['_token']))),
            'isPost' => ! empty($authData)
        ]);
    }

    public function resend()
    {
        /** @var KnownLocation $location */
        $location = \Auth::user()->locations()->where('ip', \Request::ip())->first();

        $ok = false;
        if ($location && $location->canSendAnotherNotification())
        {
            $location->sendNotification();
            $ok = true;
        }

        $location->save();

        return \Redirect::action('Auth\LocationController@index')->with('sent', $ok);
    }

    public function confirm($token)
    {
        $location = KnownLocation::where('email_token', $token)->firstOrFail();

        if (! $location->confirmed) {
            event(new LocationConfirmed($location));

            $location->confirm();

            $location->save();
        }

        return view('auth.location.confirmed', [
            'location' => $location
        ]);
    }

    public static function routes()
    {
        \Route::get('auth/location/{token}/confirm', 'Auth\LocationController@confirm');
        \Route::get('auth/location/resend', 'Auth\LocationController@resend');
        \Route::get('auth/location', 'Auth\LocationController@index');
    }
}