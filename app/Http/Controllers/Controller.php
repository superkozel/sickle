<?php

namespace App\Http\Controllers;

use App\Models\Rig\FactsProvider;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var User $user */
    protected $user;
    /** @var boolean */
    protected $signed_in;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();
            $this->signed_in = \Auth::check();

            view()->share('signed_in', $this->signed_in);
            view()->share('user', $this->user);

            return $next($request);
        });
    }
}
