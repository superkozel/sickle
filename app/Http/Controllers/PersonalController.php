<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 09.07.2018
 * Time: 0:03
 */

namespace App\Http\Controllers;


abstract class PersonalController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $this->user->load(['clusters' => function($query) {
                $query->withPivot('role');
                $query->with('rigs');
            }]);

            $rigs = collect();
            foreach ($this->user->clusters as $cluster) {
                $rigs = $rigs->merge($cluster->rigs);
            }

            return $next($request);
        });
    }
}