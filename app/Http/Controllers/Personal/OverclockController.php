<?php

namespace App\Http\Controllers\Personal;

use App\Flow\Ldap\Export;
use App\Flow\Ldap\Import;
use App\Http\Controllers\PersonalController;
use App\Models\Overclock\Profile;
use App\Models\Rig;

class OverclockController extends PersonalController
{
    public function index()
    {
        $rigs = \Auth::user()->getOwnCluster()->rigs;

        return view('personal.overclock.index', ['rigs' => $rigs]);
    }

    public function rig(Rig $rig)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        return view('personal.overclock.rig', ['rig' => $rig]);
    }

    public function loadGpuProfile(Rig $rig, $gpuId)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        $data = Import::create()->loadGpuProfile();
    }

    public function applyGpuProfile(Rig $rig, $gpuId, Profile $profile)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        $gpu = $rig->getGpus()->get($gpuId);

        if ($profile->validForGpu($gpu)) {
            Export::create()->applyGpuProfile();
        }

        return \Redirect::back();
    }
}
