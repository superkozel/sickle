<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 15.02.2018
 * Time: 17:50
 */

namespace App\Http\Controllers\Personal;

use App\Http\Controllers\PersonalController;
use App\Models\Rig;

class GpuController extends PersonalController
{
    public function index(Rig $rig, $id)
    {
        $gpu = $rig->getGpus()->get($id);
        return view('personal.gpu.stats', ['']);
    }

    public static function routes()
    {
        \Route::get('rig/{rig}/{id}', 'GpuController@index');
    }
}