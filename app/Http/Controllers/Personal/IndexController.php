<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.02.2018
 * Time: 4:18
 */

namespace App\Http\Controllers\Personal;

use App\Http\Controllers\PersonalController;

class IndexController extends PersonalController
{
    public function index()
    {
//        RigService::create()->loadUserRigsFromLdap($this->user);

//        $facts = Api::assoc(Api::getRigsFacts(['4e6e18d936dfbf2fe331d619a600ff02e91527421429']));

        $clusters = $this->user->clusters()->with('rigs')->get();

        $cluster = \Auth::user()->getOwnCluster();

        $rigsCount = 0;
        $maxGpus = 0;
        foreach ($clusters as $cluster)
        {
            foreach ($cluster->rigs as $rig) {
                $rigsCount++;
                $maxGpus = max($rig->getGpus()->count(), $maxGpus);
            }
        }

        if ($clusters->count() == 0)
            return view('personal.home', compact('clusters', 'cluster'));
        else
            return view('personal.monitor', compact('clusters', 'cluster', 'rigsCount', 'maxGpus'));

    }
}