<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 08.02.2018
 * Time: 19:03
 */

namespace App\Http\Controllers\Personal;


use App\Flow\Ldap\Schema;
use App\Http\Controllers\PersonalController;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\Cluster;
use App\Models\Enums\Cryptocurrency;
use App\Models\Payment\EthPay;
use App\Models\User\NotificationSettings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;

class AccountController extends PersonalController
{

    public function __construct()
    {
        $tabs = [
            action('Personal\AccountController@finance', [], false) => ['Finances', 'ico-money'],
            action('Personal\AccountController@edit', [], false) => ['Account', 'ico-user2'],
            action('Personal\AccountController@security', [], false) => ['Security', 'ico-shield3'],
            action('Personal\AccountController@notifications', [], false) => ['Notifications', 'ico-bell'],
            action('Personal\AccountController@access', [], false) => ['Access', 'ico-users3'],
            action('Personal\AccountController@password', [], false) => ['Change password', 'ico-key2'],
        ];
        view()->share('tabs', $tabs);
        view()->share('breadcrumbs', 'account');

        parent::__construct();
    }

    public function edit()
    {
        return view('personal.account.edit', ['user' => \Auth::user()]);
    }

    public function security()
    {
        return view('personal.account.security', ['user' => \Auth::user()]);
    }

    public function notifications()
    {
        return view('personal.account.notifications', ['user' => \Auth::user(), 'settings' => $this->user->notification_settings]);
    }

    public function updateNotificationSettings()
    {
        $user = $this->user;
        $settings = $user->notification_settings;

        $settings->setDisableAll(Input::has('disable_all'));

        $settings->setDisabledChannels(array_diff(array_keys(NotificationSettings::getChannelOptions()), Input::get('channels', [])));

        $settings->setDisabledNotifications(array_values(array_diff(array_keys(NotificationSettings::getNotificationOptions()), Input::get('notifications', []))));

        $user->notification_settings = $settings;
        $user->save();

        return redirect()->back();
    }

    public function password()
    {
        return view('personal.account.password', ['user' => \Auth::user()]);
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $password = $request->new_password;

        $user = $this->user;

        $ldapUser = $user->ldap;
        $ldapUser->setAttribute('userPassword', Schema::SSHA($password));
        $ok = $ldapUser->save();

        if ($ok) {
            $this->user->password = \Hash::make($request->new_password);
            $this->user->save();
        }

        return view('personal.account.password', ['user' => \Auth::user()]);
    }

    public function access()
    {
        $received = \Auth::user()->accesses()->withPivot('role')->get();
        $granted = \Auth::user()->getOwnCluster()->accesses()->withPivot('role')->get();

        return view('personal.account.access', ['user' => \Auth::user(), 'received' => $received, 'granted' => $granted]);
    }

    public function grantAccess(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'user_id' => 123,
            'role' => ''
        ]);
    }

    public function declineAccess($clusterId)
    {
        \Auth::user()->accesses()->where('cluster_id', $clusterId)->delete();

        return \Redirect::back();
    }

    public function removeAccess($userId)
    {
        \Auth::user()->getOwnCluster()->accesses()->where('user_id', $userId)->delete();

        return \Redirect::back();
    }

    public function update(Request $request)
    {
        $user = \Auth::user();

        \Validator::extend('unique_user_email', function($attribute, $value, $parameters){
            $query = User::where(function($query){

            });
            if ($parameters[0])
                $query->whereKeyNot($parameters[0]);

            return $query->count() == 0;

        }, 'Email already in use');

        $validator = \Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'telegram' => 'telegram',
            'password' => 'required|check_password'
        ]);

        if ($validator->passes()) {
            $user->email = $request->email;
            $user->telegram = $request->telegram;

            $user->save();

            return \Redirect::back();
        }
        else {
            return \Redirect::back()->withErrors($validator)->withInput($request->all());
        }
    }

    public function confirmEmail()
    {
        $user = \Auth::user();

        $user->email_confirmed = $user->email;
        $user->save();
    }

    public function finance()
    {
        $user = \Auth::user();
//        $ethPay = new EthPay();
//        try {
//            $rate = $ethPay->getCurrentPrice();
//        }
//        catch (\Exception $e) {
//            $rate = 0;
//        }
        $rate = 0;

//        $address = \Auth::user()->getPaymentAddess(Cryptocurrency::ID_ETHEREUM);
        $address = null;


        return view('personal.account.finance', ['user' => \Auth::user(), 'ethRate' => $rate, 'transactions' => $user->paymentTransactions, 'address' => $address]);
    }

    public function updatePayments()
    {
        $ethPay = new EthPay();
        $ethPay->getTransactions();

        $transactions = $ethPay->getTransactions($address);

        if ($transactions->notObrabotana) {
            $balance += processTransaction($trans);
            $balance->save();
        }
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'account'], function(){
            \Route::get('/', 'AccountController@index');
            \Route::get('/finance', 'AccountController@finance');
            \Route::get('/security', 'AccountController@security');
            \Route::get('/notifications', 'AccountController@notifications');
            \Route::post('/notifications/settings/update', 'AccountController@updateNotificationSettings');
            \Route::get('/access', 'AccountController@access');
            \Route::get('/password', 'AccountController@password');
            \Route::post('/password', 'AccountController@updatePassword');

            \Route::get('/edit', 'AccountController@edit');
            \Route::post('/edit', 'AccountController@update');
        });
    }
}