<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 12.03.2018
 * Time: 20:09
 */

namespace App\Http\Controllers\Personal;

use App\Http\Controllers\PersonalController;
use App\Models\Cluster;

class ClusterController extends PersonalController
{
    public function select(Cluster $cluster)
    {
        if (! \Gate::allows('view-cluster', $cluster))
            abort(403);

        $this->user->setCurrentCluster($cluster);

        return \Redirect::back();
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'cluster'], function(){
            \Route::get('select/{cluster}', 'ClusterController@select');
        });
    }
}