<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 08.02.2018
 * Time: 19:02
 */

namespace App\Http\Controllers\Personal;

use App\Flow\Ldap\Export;
use App\Http\Controllers\PersonalController;
use App\Http\Requests\WalletRequest;
use App\Models\Cluster;
use App\Models\Wallet;
use Illuminate\Http\Request;

class WalletController extends PersonalController
{
    public function __construct()
    {
        view()->share('breadcrumbs', 'wallet');

        parent::__construct();
    }

    public function index(Cluster $cluster = null)
    {
        if (! $cluster)
            $cluster = \Auth::user()->getOwnCluster();

        return view('personal.wallet.index', ['cluster' => $cluster, 'wallets' => $cluster->wallets]);
    }

    public function add(Cluster $cluster = null)
    {
        if (! $cluster)
            $cluster = \Auth::user()->getOwnCluster();

        if (! \Gate::allows('control-cluster', $cluster))
            abort(403);

        return view('personal.wallet.add', ['wallet' => new Wallet(), 'cluster' => $cluster]);
    }

    public function store(WalletRequest $request)
    {
        $wallet = new Wallet();

        $wallet->currency_id = $request->currency_id;
        $wallet->address = $request->address;
        $wallet->cluster_id = $request->cluster_id;

        $wallet->save();

        if (! $request->isXmlHttpRequest())
            return \Redirect::action('Personal\WalletController@index');
        else
            return \Response::json(['success' => true, 'wallet' => $wallet->jsonSerialize()]);
    }

    public function edit(Wallet $wallet)
    {
        if (! \Gate::allows('control-wallet', $wallet))
            abort(403);

        return view('personal.wallet.edit', ['wallet' => $wallet, 'cluster' => $wallet->cluster]);
    }

    public function update(Wallet $wallet, WalletRequest $request)
    {
        $wallet->forceFill($request->validated());

        $wallet->save();

        if ($configuration = $wallet->configuration) {
            $this->_updateWalletConfiguration($configuration);
        }

        if (! $request->isXmlHttpRequest())
            return \Redirect::back();
        else
            return \Response::json(['success' => true, 'wallet' => $wallet->jsonSerialize()]);
    }

    public function delete(Wallet $wallet, Request $request)
    {
        if (! \Gate::allows('control-wallet', $wallet))
            abort(403);

        $associatedConfigs = $wallet->getConfigurations()->count();

        if ($associatedConfigs > 0 && ! $request->new_wallet_address) {
            $otherWallets = $wallet->cluster->wallets()->where('currency_id', $wallet->currency_id)->whereKeyNot($wallet->id)->get();

            return view('personal.wallet.remove', ['wallet' => $wallet, 'otherWallets' => $otherWallets, 'requiredNewWallet' => $associatedConfigs > 0]);
        }

        return \DB::transaction(function() use ($request, $wallet, $associatedConfigs){
            $cluster = $wallet->cluster;

            $validator = \Validator::make(\Request::all(), [
                'new_wallet_address' => ($associatedConfigs > 0 ? 'required|' : '') . '|currency_address:' . $wallet->getCurrency()->getCode()
            ]);

            if ($validator->passes()) {
                if ($associatedConfigs > 0 && $request->new_wallet_address) {
                    /** @var Wallet $newWallet */
                    $newWallet = $cluster->wallets()
                        ->where('currency_id', $wallet->currency_id)
                        ->where('address', $request->new_wallet_address)
                        ->first()
                    ;

                    if ($newWallet) {
                        if ($newWallet->id != $wallet->id) {
                            $wallet->cluster->configurations()->where('wallet_id', $wallet->id)->update(['wallet_id' => $newWallet->id]);
                            $wallet->cluster->configurations()->where('second_wallet_id', $wallet->id)->update(['second_wallet_id' => $newWallet->id]);

                            $wallet->delete();
                        }
                    }
                    else {
                        $wallet->address = $request->new_wallet_address;
                        $wallet->save();
                    }

                    foreach ($newWallet->getConfigurations()->get() as $configuration) {
                        $this->_updateWalletConfiguration($configuration);
                    }
                }
                else {
                    $wallet->delete();
                }

                if (! \Request::isXmlHttpRequest())
                    return \Redirect::to(action('Personal\WalletController@index'));
                else
                    return \Response::json(['success' => true]);
            }
            else {
                if (! \Request::isXmlHttpRequest())
                    return \Redirect::back()->withErrors($validator)->withInput($request->all());
                else
                    return \Response::json(['success' => false, 'errors' => $validator->errors()]);
            }
        });
    }

    protected function _updateWalletConfiguration($configuration)
    {
        $newChecksum = $configuration->checksum();
        if ($configuration->checksum != $newChecksum) {
            $configuration->checksum = $newChecksum;
            $configuration->save();

            Export::create()->exportConfiguration($configuration);
        }
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'wallet'], function(){
            \Route::get('add/{cluster?}', 'WalletController@add');
            \Route::post('add', 'WalletController@store');
            \Route::get('edit/{wallet}', 'WalletController@edit');
            \Route::post('edit/{wallet}', 'WalletController@update');
            \Route::post('delete/{wallet}', 'WalletController@delete');
            \Route::get('/{cluster?}', 'WalletController@index');
        });
    }
}