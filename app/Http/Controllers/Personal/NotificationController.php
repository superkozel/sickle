<?php

namespace App\Http\Controllers\Personal;

use App\Http\Controllers\PersonalController;
use Illuminate\Http\Request;

class NotificationController extends PersonalController
{
    public function __construct()
    {
        view()->share('breadcrumbs', 'notification');

        parent::__construct();
    }

    public function index()
    {
        $user = \Auth::user();

        $user->unreadNotifications()->update(['read_at' => now()]);

        return view('personal.notification.list', ['notifications' => $user->notifications()]);
    }

    public function clear(Request $request)
    {
        $user = \Auth::user();

        $user->unreadNotifications()->update(['read_at' => now()]);

        if (! $request->isXmlHttpRequest())
            return \Redirect::back();
        else
            return \Response::json(['success' => true, 'html' =>
                view('personal.notification.listing', ['notifications' => $user->unreadNotifications()])->render()
            ]);
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'notification'], function(){
            \Route::get('/', 'NotificationController@index');
            \Route::post('clear', 'NotificationController@clear');
        });
    }
}
