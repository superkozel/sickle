<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 4:35
 */

namespace App\Http\Controllers\Personal;


use App\Http\Controllers\PersonalController;
use App\Models\ActiveRecord\CurrencyRate;

class RatesController extends PersonalController
{
    public function index()
    {
        $rateData = CurrencyRate::all()->toArray();
        foreach ($rateData as $rate) {
            $rates[$rate['cryptocurrency']][$rate['currency']] = $rate['rate'];
        }

        return view('personal.rates.list', ['rates' => $rates]);
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'rates'], function(){
            \Route::get('/', 'RatesController@index');
        });
    }
}