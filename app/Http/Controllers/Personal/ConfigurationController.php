<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 18:55
 */

namespace App\Http\Controllers\Personal;


use App\Flow\Ldap\Export;
use App\Http\Controllers\PersonalController;
use App\Http\Requests\ConfigurationRequest;
use App\Models\Configuration;

class ConfigurationController extends PersonalController
{
    public function __construct()
    {
        view()->share('breadcrumbs', 'configuration');

        parent::__construct();
    }

    public function index()
    {
        if ($id = \Request::get('cluster_id'))
            $cluster = \Auth::user()->clusters()->findOrFail($id);
        else
            $cluster = \Auth::user()->getOwnCluster();

        $configs = $cluster->configurations()->with('wallet', 'secondWallet', 'rigs')->get();
        return view('personal.configuration.list', ['configurations' => $configs, 'cluster' => $cluster]);
    }

    public function create()
    {
        if ($id = \Request::get('cluster_id'))
            $cluster = \Auth::user()->clusters()->findOrFail($id);
        else
            $cluster = \Auth::user()->getOwnCluster();

        if (\Gate::denies('control-cluster', $cluster))
            abort(403);

        $configuration = new Configuration();

        if (\Request::isXmlHttpRequest()) {

            if (\Request::has('Configuration')) {
                $subform = 'Configuration';
                $configuration->forceFill(\Request::get('Configuration'));
            }
            else {
                $subform = null;
                $configuration->forceFill(\Request::all());
            }

            return view('personal.configuration.form', ['configuration' => $configuration, 'cluster' => $cluster, 'subform' => $subform]);
        }
        else
            return view('personal.configuration.create', ['configuration' => $configuration, 'cluster' => $cluster]);
    }

    public function store(ConfigurationRequest $request)
    {
        $configuration = \DB::transaction(function() use ($request){
            $configuration = new Configuration();
            $configuration->forceFill($request->validated());

            $configuration->save();

            return $configuration;
        });

        if (! $request->isXmlHttpRequest())
            return \Redirect::to($configuration->getEditUrl());
    }

    public function edit(Configuration $configuration)
    {
        if (\Gate::denies('control-configuration', $configuration))
            abort(403);

        $cluster = $configuration->cluster;

        if (\Request::isXmlHttpRequest()) {
            $configuration->forceFill(\Request::all());
            return view('personal.configuration.form', ['configuration' => $configuration, 'cluster' => $cluster]);
        }
        else
            return view('personal.configuration.edit', ['configuration' => $configuration, 'cluster' => $cluster]);
    }

    public function update(Configuration $configuration, ConfigurationRequest $request)
    {
        $configuration = \DB::transaction(function() use ($request, $configuration){
            $configuration->forceFill(array_except($request->validated(), 'cluster_id'));

            $configuration->save();

            Export::create()->exportConfiguration($configuration);

            return $configuration;
        });

        if (! $request->isXmlHttpRequest())
            return \Redirect::to($configuration->getEditUrl());
    }

    public function apply(Configuration $configuration)
    {
        if (\Gate::denies('control-configuration', $configuration) OR \Gate::denies('control-cluster', $configuration->cluster))
            abort(403);

        Export::create()->exportConfiguration($configuration);

        if (! \Request::isXmlHttpRequest())
            return \Redirect::back();
    }

    public function delete(Configuration $configuration)
    {
        if (\Gate::denies('control-configuration', $configuration))
            abort(403);

        $configuration->delete();
        //everything else get deleted cascade with foreign key constraint

        if (! \Request::isXmlHttpRequest())
            return \Redirect::back();
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'configuration'], function(){
            \Route::get('/', 'ConfigurationController@index');
            \Route::get('create', 'ConfigurationController@create');
            \Route::post('create', 'ConfigurationController@store');
            \Route::get('edit/{configuration}', 'ConfigurationController@edit');
            \Route::post('edit/{configuration}', 'ConfigurationController@update');
            \Route::post('apply/{configuration}', 'ConfigurationController@apply');
            \Route::post('delete/{configuration}', 'ConfigurationController@delete');
            \Route::get('{id}', 'ConfigurationController@show');
        });
    }
}