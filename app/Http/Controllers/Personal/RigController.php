<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 08.02.2018
 * Time: 19:02
 */

namespace App\Http\Controllers\Personal;

use App\Flow\Ldap\Export;
use App\Flow\Ldap\Import;
use App\Flow\Puppet\Api;
use App\Http\Controllers\PersonalController;
use App\Http\Requests\RigRequest;
use App\Models\Cluster;
use App\Models\Configuration;
use App\Models\Rig;
use App\Models\Rig\GpuRig;
use App\Services\RigService;

class RigController extends PersonalController
{
    public function __construct()
    {
        view()->share('breadcrumbs', 'rig');

        parent::__construct();
    }

    public function index()
    {
        $rigs = \Auth::user()->rigs;

        return view('personal.rig.list', ['rigs' => $rigs]);
    }

    public function reboot(Rig $rig)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        RigService::create()->reboot($rig);

        return \Redirect::back();
    }

    public function shutdown(Rig $rig)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        RigService::create()->shutdown($rig);

        return \Redirect::back();
    }

    public function show(Rig $rig)
    {
        if (\Gate::denies('view-rig', $rig))
            abort(403);

        Rig\FactsProvider::loadFacts($rig, ['operatingsystem', 'operatingsystemrelease', 'disks']);

        return view('personal.rig.show', ['rig' => $rig]);
    }

    public function add()
    {
        $rig = new GpuRig();

        if (\Request::get('cluster_id'))
            $cluster = Cluster::find(\Request::get('cluster_id'));
        else
            $cluster = \Auth::user()->getOwnCluster();

        if (\Request::get('certname'))
            $registration = $this->user->rigRegistrations()->where('hash', \Request::get('certname'))->whereNull('rig_id')->first();
        else
            $registration = null;

        return view('personal.rig.add', ['rig' => $rig, 'cluster' => $cluster, 'registration' => $registration]);
    }

    public function store(RigRequest $request)
    {
        $err = null;
        /** @var Rig $rig */
        $rig = \DB::transaction(function() use ($request, &$err){
            $registration = $this->user->rigRegistrations()->where('hash', $request->get('certname'))->first();
            if (! $registration) {
                $err = 'Registration not found';
            }

            if ($registration->rig_id) {
                $err = 'Rig already registered';
            }

            $cluster = $this->user->getOwnCluster();

            if (\Gate::denies('control-cluster', $cluster))
                abort(403);

            $ok = Api::signCertificate($request->get('hash'));
            if (! $ok) {
                $err = 'Rig cannot be signed';
            }

            if ($err) {
                \Session::set('errors', ['hash' => [
                    $err
                ]]);
            }

//            $ldapRig->setOwner(\Auth::user()->ldap);
//            $ldapRig->save();

            $ldapRig = Import::create()->getRigByHash($registration->hash);
            if ($ldapRig)  {
                $rig = Import::create()->importRig($cluster, $ldapRig);
            }
            else {
                $rig = Rig::classFactory($request->get('type'));
                $rig->cluster_id = $cluster->id;
                $rig->ldap_id = $registration->hash;

                $rig->hostname = $request->get('hostname');
                $rig->name = $request->get('name');
                $rig->description = $request->get('description');

                $ldapRig = Export::create()->createRig($rig);

                if (! $ldapRig) {
                    die('Unable to export rig to ldap');
                }

                $rig->save();
            }

            $registration->rig_id = $rig->id;
            $registration->save();

            return $rig;
        });

        if ($err)
            return \Redirect::back()->with('errors', ['ldap_id' => $err]);

        if (! $request->isXmlHttpRequest())
            return \Redirect::to($rig->getEditUrl());
        else
            return json_encode(['success' => true, 'rig' => $rig->toJson()]);
    }

    public function edit(Rig $rig)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        $cluster = $rig->cluster;

        return view('personal.rig.edit', ['rig' => $rig, 'cluster' => $cluster]);
    }

    public function update(Rig $rig, RigRequest $request)
    {
        return \DB::transaction(function() use ($rig, $request){
            $rig->forceFill(array_except($request->validated(), ['Configuration', 'type']));

            if ($request->configuration_id === 0) {
                $configuration = new Configuration();
                $configuration->forceFill($request->validated()['Configuration']);
                $configuration->save();

                $rig->configuration_id = $configuration->id;
                $rig->setRelation('configuration', $configuration);
            }

            $rig->setSingleTableType($request->type);
            $rig->type = $request->type;
            $rig->save();

            Rig::where('id', $rig->id)->update(['type' => $request->type]);

            Export::create()->updateRig($rig);

            if (! $request->isXmlHttpRequest())
                return \Redirect::back();
        });
    }

    public function remove(Rig $rig)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        $ldapRig = $rig->getLdapRig();
        if ($ldapRig) {
            $ok = $ldapRig->delete();
        }
        $rig->delete();


        return \Redirect::back();
    }

    /**
     * Burn configuration
     * @param Rig $rig
     * @return \Illuminate\Http\RedirectResponse
     */
    public function burn(Rig $rig)
    {
        if (\Gate::denies('control-rig', $rig))
            abort(403);

        Export::create()->exportConfiguration($rig->configuration, $rig);

        return \Redirect::back();
    }

    public function suggestName()
    {
        $hash = \Request::get('hash');

        $rig = new GpuRig();
        $rig->id = $hash;
        $rig->cluster_id = \Auth::user()->getOwnCluster()->id;

        if ($rig) {
            return json_response(['success' => true, 'name' => $rig->generateName()]);
        }
        else {
            return json_response(['success' => false]);
        }
    }

    public function stats(Rig $rig)
    {
        if (\Gate::denies('view-rig', $rig))
            abort(403);

        return view('personal.rig.stats', ['rig' => $rig, 'cluster' => $rig->cluster]);
    }

    public function gpu(Rig $rig, $id)
    {
        if (\Gate::denies('view-rig', $rig))
            abort(403);

        $gpu = $rig->getGpu($id);

        return view('personal.rig.stats', ['rig' => $rig, 'gpu' => $gpu]);
    }

    public static function routes()
    {
        \Route::group(['prefix' => 'rig'], function(){
            \Route::get('/', 'RigController@index')->name('rig');
            \Route::get('stats/{rig}', 'RigController@stats')->name('rig.stats');
            \Route::get('add', 'RigController@add')->name('rig.add');
            \Route::post('add', 'RigController@store');
            \Route::get('edit/{rig}', 'RigController@edit')->name('rig.edit');
            \Route::post('edit/{rig}', 'RigController@update');
            \Route::post('remove/{rig}', 'RigController@remove')->name('rig.remove');
            \Route::post('burn/{rig}', 'RigController@burn');
            \Route::post('reboot/{rig}', 'RigController@reboot');
            \Route::post('shutdown/{rig}', 'RigController@shutdown');
            \Route::get('suggest_name', 'RigController@suggestName');
            \Route::post('{rig}/{id}', 'RigController@gpu');
            \Route::get('{rig}', 'RigController@show');
        });
    }
}