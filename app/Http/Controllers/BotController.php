<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 01.03.2018
 * Time: 21:04
 */

namespace App\Http\Controllers;

use Telegram\Bot\Api;

class BotController extends Controller
{
    public function index()
    {
        $telegram = new Api('BOT TOKEN');

        $response = $telegram->getMe();

        $botId = $response->getId();
        $firstName = $response->getFirstName();
        $username = $response->getUsername();
    }
}