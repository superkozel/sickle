<?php

namespace App\Http\Middleware;

use App\Models\User\KnownLocation;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Console\MiddlewareMakeCommand;

class ConfirmLoginFromUnknownLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! KnownLocation::isKnown(\Request::ip(), \Auth::user())) {
            return \Redirect::action('Auth\LocationController@index', ['intended' => encrypt($request->fullUrl())]);
        }

        return $next($request);
    }
}
