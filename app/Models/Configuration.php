<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 17:57
 */

namespace App\Models;

use App\Flow\Ldap\Export;
use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Cryptocurrency;
use App\Models\Enums\Miner;
use App\Models\Miner\BaseMiner;
use App\Notifications\Configuration\Changed;

/**
 * App\Models\Configuration
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rig[] $rigs
 * @property-read \App\Models\Wallet $secondWallet
 * @property-read \App\Models\Wallet $wallet
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $cluster_id
 * @property int $currency_id
 * @property int|null $second_currency_id
 * @property int $wallet_id
 * @property int|null $second_wallet_id
 * @property int|null $miner_id
 * @property string|null $epools
 * @property string|null $dpools
 * @property int|null $second_coin_intensity
 * @property int|null $config_override
 * @property string|null $checksum
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereChecksum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereConfigOverride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereDpools($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereEpools($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereMinerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereSecondCoinIntensity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereSecondCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereSecondWalletId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereWalletId($value)
 */
class Configuration extends \Eloquent
{

    public static function boot()
    {
        parent::boot();

        static::saving(function(Configuration $configuration) {
            $configuration->checksum = $configuration->checksum();
            unset($configuration->wallet);
            unset($configuration->second_wallet);

            //saving wallet addresses from ConfigurationRequest
            Wallet::unguard();
            if ($configuration->wallet_address) {
                $wallet = $configuration->cluster->wallets()->firstOrCreate([
                    'address' => $configuration->wallet_address,
                    'currency_id' => $configuration->currency_id
                ]);

                $configuration->wallet_id = $wallet->id;
                unset($configuration->wallet_address);
            }

            if ($configuration->second_wallet_address) {
                $wallet = $configuration->cluster->wallets()->firstOrCreate([
                    'address' => $configuration->second_wallet_address,
                    'currency_id' => $configuration->second_currency_id
                ]);

                $configuration->second_wallet_id = $wallet->id;
                unset($configuration->second_wallet_address);
            }
            Wallet::reguard();
        });
//        static::updated(function(Wallet $wallet)
//        {
//            $notification = new Changed();
//
//            if ($wallet->wasChanged(['wallet_id', 'second_wallet_id']))
//                $wallet->cluster->notify($notification);
//        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster()
    {
        return $this->belongsTo(Cluster::class);
    }

    /**
     * @return BaseCryptocurrency
     */
    public function getCurrency()
    {
        return Cryptocurrency::find($this->currency_id);
    }

    /**
     * @return BaseCryptocurrency
     */
    public function getSecondCurrency()
    {
        return Cryptocurrency::find($this->second_currency_id);
    }

    /**
     * @return BaseMiner
     */
    public function getMiner()
    {
        return Miner::find($this->miner_id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondWallet()
    {
        return $this->belongsTo(Wallet::class, 'second_wallet_id');
    }

    public function rigs()
    {
        return $this->hasMany(Rig::class);
    }

    /**
     * @return bool
     */
    public function isDual()
    {
        return !! $this->second_currency_id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->getEditUrl();
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return action('Personal\ConfigurationController@edit', ['configuration' => $this]);
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return action('Personal\ConfigurationController@delete', ['configuration' => $this]);
    }

    /**
     * checksum for
     * identity check
     * @return array
     */
    public function checksum()
    {
        $arr = array_except($this->getAttributes(), ['id', 'name', 'cluster_id', 'created_at', 'updated_at', 'checksum']);

        $macrofields = ['epools', 'dpools', 'config_override'];
        foreach ($macrofields as $field) {
            if (empty($arr[$field]))
                continue;

            $arr[$field] = $this->applyMacroses($arr[$field]);
        }

        array_filter($arr);//empty null fields doesn't count

        return md5(join(';', $arr));
    }

    /**
     * macroses for text replacement in
     * config fields
     * @return array
     */
    public function getMacroses()
    {
        $macroses = [];

        if ($wallet = $this->wallet)
            $macroses['#' . mb_strtoupper($wallet->getCurrency()->getCode() . '_wallet')] = $wallet->address;
        if ($secondWallet = $this->secondWallet)
            $macroses['#' . mb_strtoupper($secondWallet->getCurrency()->getCode() . '_wallet')] = $secondWallet->address;

        return $macroses;
    }

    /**
     * @param $str
     * @return mixed
     */
    public function applyMacroses($str)
    {
        if (! $str)
            return $str;

        foreach ($this->getMacroses() as $id => $val) {
            $str = str_replace($id, $val, $str);
        }

        return $str;
    }

    /**
     * Burn/Apply configuration
     * to associated rigs
     * @return $this
     */
    public function applyToRigs()
    {
        Export::create()->exportConfiguration($this);

        return $this;
    }
}