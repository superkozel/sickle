<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 25.02.2018
 * Time: 18:37
 */

namespace App\Models\Payment;


/**
 * App\Models\Payment\Transaction
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $payment_type_id
 * @property string $address
 * @property string $transaction_id
 * @property int $amount
 * @property int $usd
 * @property int $processed
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction wherePaymentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Payment\Transaction whereUserId($value)
 */
class Transaction extends \Eloquent
{
    public $table = 'payment_transactions';

    const UPDATED_AT = null;

    const PAYMENT_TYPE_ETHEREUM = 1;
}