<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 17.03.2018
 * Time: 21:39
 */

namespace App\Models\Enums;


class Algorithm extends BaseEnum
{
    const ID_SHA256 = 'sha256';
    const ID_SHA256D = 'sha256d';
    const ID_SHA3 = 'sha3';
    const ID_SHA512 = 'sha512';
    const ID_DAGGER_HASHIMOTO = 'dagger_hashimoto';
    const ID_BLAKE2S = 'blake_2s';
    const ID_BLAKE2B = 'blake_2b';
    const ID_CRYPTONIGHT = 'cryptonight';
    const ID_SCRYPT = 'scrypt';
    const ID_X11 = 'x11';
    const ID_X13 = 'x13';
    const ID_NIST5 = 'nist5';
    const ID_EQUIHASH = 'equihash';
    const ID_ETHASH = 'ethash';
    const ID_GROESTL = 'groestl';
    const ID_LYRA2RE = 'lyra2re';


    protected static $names = [
        Algorithm::ID_SHA256 => 'SHA256',
        Algorithm::ID_SHA256D => 'SHA256D',
        Algorithm::ID_CRYPTONIGHT => 'CryptoNight',
        Algorithm::ID_SCRYPT => 'Scrypt',
        Algorithm::ID_X11 => 'X11',
        Algorithm::ID_X13 => 'X13',
        Algorithm::ID_NIST5 => 'NIST5',
        Algorithm::ID_EQUIHASH => 'Equihash',
        Algorithm::ID_ETHASH => 'Ethash',
        Algorithm::ID_GROESTL => 'Groestl',
        Algorithm::ID_LYRA2RE =>  'Lyra2RE',
        Algorithm::ID_BLAKE2S =>  'Blake 2s',
        Algorithm::ID_BLAKE2B =>  'Blake 2b',
    ];

    public static function sha256()
    {
        return static::create(static::ID_SHA256);
    }
}