<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 12.02.2018
 * Time: 17:51
 */

namespace App\Models\Enums;

use App\Models\Miner\ClaymoreDual;

class Miner extends BaseFactoryEnum
{
    const ID_CLAYMORE_DUAL = 1;

    protected static $list = [
        Miner::ID_CLAYMORE_DUAL => ClaymoreDual::class,
    ];
}