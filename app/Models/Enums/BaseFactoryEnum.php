<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:06
 */

namespace App\Models\Enums;

use App\Models\Cryptocurrency\Bitcoin;
use App\Models\Cryptocurrency\Ethereum;

class BaseFactoryEnum extends BaseEnum
{
    protected static $list = [
    ];

    /**
     * @param $id
     * @return EnumFactoryValueInterface
     */
    public static function find($id)
    {
        if (isset(static::$list[$id])){
            $class = static::$list[$id];
            return new $class;
        }
    }

    /**
     * @return array
     */
    public static function getNames($objects = null)
    {
        $result = [];

        if (! $objects) {
            foreach (static::$list as $id => $class) {
                $result[$id] = (new $class)->getName();
            }
        }
        else {
            foreach ($objects as $obj) {
                $result[$obj->getId()] = $obj->getName();
            }
        }
        return $result;
    }
}