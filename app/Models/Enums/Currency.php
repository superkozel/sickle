<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 2:17
 */

namespace App\Models\Enums;


class Currency extends BaseEnum
{
    const ID_USD = 'USD';
    const ID_EUR = 'EUR';
    const ID_RUR = 'RUR';

    protected static $names = [
        Currency::ID_USD => 'Доллар США',
        Currency::ID_EUR => 'Евро',
        Currency::ID_RUR => 'Российский рубль',
    ];
}