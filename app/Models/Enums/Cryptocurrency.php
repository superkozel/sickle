<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 14:50
 */

namespace App\Models\Enums;

use App\Models\Cryptocurrency\Ethereum;
use App\Models\Cryptocurrency\Bitcoin;
use App\Models\Cryptocurrency\Monero;
use App\Models\Cryptocurrency\Siacoin;
use App\Models\Cryptocurrency\Zcash;

class Cryptocurrency extends BaseFactoryEnum
{
    const ID_ETHEREUM = 1;
    const ID_BITCOIN = 2;
    const ID_SIACOIN = 3;
    const ID_ZCASH = 4;
    const ID_MONERO = 5;

    protected static $list = [
        Cryptocurrency::ID_ETHEREUM => Ethereum::class,
        Cryptocurrency::ID_BITCOIN => Bitcoin::class,
        Cryptocurrency::ID_SIACOIN => Siacoin::class,
        Cryptocurrency::ID_ZCASH => Zcash::class,
        Cryptocurrency::ID_MONERO => Monero::class,
    ];

    public static function findByCode($code)
    {
        foreach (static::all() as $currency) {
            if (strtolower($currency->getCode()) == strtolower($code))
                return $currency;
        }
    }
}