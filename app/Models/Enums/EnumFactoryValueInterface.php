<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:20
 */

namespace App\Models\Enums;

interface EnumFactoryValueInterface
{
    public function getName();

    public function getId();
}