<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:19
 */

namespace App\Models\Miner;

use App\Models\Enums\Cryptocurrency;

class ClaymoreDual extends BaseMiner
{
    public function getName()
    {
        return 'Claymore dual';
    }

    public function getFields()
    {
        return [
            'currency,second_currency,pool,wallet_id,second_wallet_id'
        ];
    }

    public function isDual()
    {
        return true;
    }

    public function getCurrencies()
    {
        return Cryptocurrency::getByIds([
            Cryptocurrency::ID_ETHEREUM,
            Cryptocurrency::ID_SIACOIN
        ]);
    }

    public function getPrimaryCurrencies()
    {
        return Cryptocurrency::getByIds([
            Cryptocurrency::ID_ETHEREUM
        ]);
    }

    public function getValidationRules($data = [])
    {
        return [
            'epools' => 'required_with:currency_id',
            'dpools' => 'required_with:second_currency_id',
            'second_coin_intensivity' => 'required_with:second_currency_id|integer|min:0|max:100',
            'config_override' => ''
        ];
    }
}