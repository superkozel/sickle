<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 20:55
 */

namespace App\Models\Miner;


use App\Models\Enums\Cryptocurrency;

class SgMiner extends BaseMiner
{
    public function getName()
    {
        return 'Claymore dual';
    }

    public function getFields()
    {
        return [
            'currency,second_currency,pool,wallet_id,second_wallet_id'
        ];
    }

    public function isDual()
    {
        return true;
    }

    public function getCurrencies()
    {
        return Cryptocurrency::getByIds([
            Cryptocurrency::ID_ETHEREUM,
            Cryptocurrency::ID_ZCASH,
        ]);
    }

    public function getPrimaryCurrencies()
    {
        return Cryptocurrency::getByIds([
            Cryptocurrency::ID_MONERO
        ]);
    }

    public function getValidationRules($data = [])
    {
        return [
            'epools' => 'required_with:currency_id',
            'dpools' => 'required_with:second_currency_id',
            'second_coin_intensivity' => 'required_with:second_currency_id|integer|min:0|max:100',
            'config_override' => ''
        ];
    }
}