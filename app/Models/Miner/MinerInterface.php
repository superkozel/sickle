<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:18
 */

namespace App\Models\Miner;

use App\Models\Cryptocurrency\CryptocurrencyInterface;
use App\Models\Enums\EnumFactoryValueInterface;

interface MinerInterface extends EnumFactoryValueInterface
{
    /**
     * @return bool
     */
    public function isDual();

    /**
     * @return CryptocurrencyInterface[]
     */
    public function getCurrencies();

    /**
     * @var array $data
     * @return mixed
     */
    public function getValidationRules($data);

    /**
     * @return CryptocurrencyInterface[]
     */
    public function getPrimaryCurrencies();
}