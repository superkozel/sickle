<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:16
 */

namespace App\Models\Miner;

use App\Models\Enums\Miner;

abstract class BaseMiner implements MinerInterface
{
    public function getId()
    {
        foreach (Miner::getNames() as $id => $name) {
            if ($name == $this->getName())
                return $id;
        }
    }

    public function hasCurrency($currency)
    {
        foreach ($this->getCurrencies() as $minerCurrency) {
            if ($minerCurrency->is($currency))
                return true;
        }

        return false;
    }

    public function hasPrimaryCurrency($currency)
    {
        foreach ($this->getPrimaryCurrencies() as $minerCurrency) {
            if ($minerCurrency->is($currency))
                return true;
        }

        return false;
    }

    public function getSecondaryCurrencies()
    {
        return array_diff_key($this->getCurrencies(), $this->getPrimaryCurrencies());
    }

    public function is($id)
    {
        if (is_object($id))
            $id = $id->getId();

        return $this->getId() == $id;
    }

    public function canMine($currency1, $currency2 = null)
    {
        $currencies = func_get_args();

        $can = true;
        foreach ($currencies as $currency) {
            if (empty($currency))
                continue;

            if (! $this->hasCurrency($currency))
                $can = false;
        }
        return $can;
    }
}