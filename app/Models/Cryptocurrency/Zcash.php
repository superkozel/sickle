<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 20.02.2018
 * Time: 18:51
 */

namespace App\Models\Cryptocurrency;

class Zcash extends BaseCryptocurrency
{
    public function getName()
    {
        return 'Zcash';
    }

    public function getCode()
    {
        return 'ZEC';
    }

    public function walletValidationRule($address)
    {
        return $this->isTAddress() OR $this->isZAddress();
        return $this->isBase58() && strlen();
    }
}