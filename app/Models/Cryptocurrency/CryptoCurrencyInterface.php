<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:03
 */

namespace App\Models\Cryptocurrency;

use App\Models\Enums\EnumFactoryValueInterface;

interface CryptocurrencyInterface extends EnumFactoryValueInterface
{
    public function walletValidationRule($address);

    public function getCode();
    }