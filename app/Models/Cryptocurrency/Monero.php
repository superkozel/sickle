<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 23.02.2018
 * Time: 2:15
 */

namespace App\Models\Cryptocurrency;


class Monero extends BaseCryptocurrency
{
    public function walletValidationRule($address)
    {
//        They consist of 95 characters
//        They start with a 4
//        The second character can only be a number (0-9), or letters A or B
//        0-9 aZ
        return mb_strlen($address) == 95
            && mb_substr($address, 0, 1) == '4'
            && (in_array(mb_substr($address, 1, 1), range(0, 9) + ['A', 'B']))
            && ctype_alnum($address)
            ;
    }

    public function getName()
    {
        return 'Monero';
    }

    public function getCode()
    {
        return 'XMR';
    }

    public function getWalletBalance()
    {
        return false;
    }
}