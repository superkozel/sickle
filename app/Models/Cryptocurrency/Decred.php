<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 05.05.2018
 * Time: 16:02
 */

namespace App\Models\Cryptocurrency;


class Decred extends BaseCryptocurrency
{
    public function getName()
    {
        return 'Decred';
    }

    public function getCode()
    {
        return 'DCR';
    }

    public function walletValidationRule($address)
    {
        return $this->isAddress($address);
    }
}