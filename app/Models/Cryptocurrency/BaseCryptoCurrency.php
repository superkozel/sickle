<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 15:01
 */
namespace App\Models\Cryptocurrency;

use App\Models\ActiveRecord\CurrencyRate;
use App\Models\Enums\Cryptocurrency;

abstract class BaseCryptocurrency implements CryptocurrencyInterface
{
    /** @var string */
    protected $addressLength;
    /** @var string */
    protected $hashFunction;

    public function getId()
    {
        foreach (Cryptocurrency::getNames() as $id => $name) {
            if ($name == $this->getName())
                return $id;
        }
    }

    public function is($id)
    {
        if (is_object($id))
            $id = $id->getId();

        return $this->getId() == $id;
    }

//    function getChecksum(payload) {
//        // Each currency may implement different hashing algorithm
//        switch ($this->getHashFunction()) {
//            case 'blake256':
//                return cryptoUtils.blake256Checksum(payload);
//                break;
//            case 'sha256':
//            default:
//                return cryptoUtils.sha256Checksum(payload);
//        }
//    }
//
//    public function getAddressType($address) {
//        // should be 25 bytes per btc address spec and 26 decred
//        $expectedLength = $this->getAddressLength();
//        $hashFunction = $this->getHashFunction();
//        $decoded = $this->decodeAddress($address);
//
//        if ($decoded) {
//            $length = mb_strlen($decoded);
//
//            if ($length != $expectedLength) {
//                return null;
//            }
//
//
//            $checksum = cryptoUtils.toHex(decoded.slice(length - 4, length));
//            $body = cryptoUtils.toHex(decoded.slice(0, length - 4));
//            $goodChecksum = $this->getChecksum($body);
//
//            return checksum === goodChecksum ? cryptoUtils.toHex(decoded.slice(0, expectedLength - 24)) : null;
//        }
//
//        return null;
//    }

    public function walletValidationRule($address)
    {
        $addressType = $this->getAddressType($address);

        if (is_null($addressType))
            return false;

        $networkType = 'prod';
        if ($networkType === 'prod' || $networkType === 'testnet'){
            $correctAddressTypes = $this->getAddressTypes($networkType);
        } else {
            $correctAddressTypes = array_merge($this->getAddressTypes('prod'), $this->getAddressTypes('testnet'));
        }

        return in_array($addressType, $correctAddressTypes);
    }

    /**
     * @return string
     */
    public function getAddressLength(): string
    {
        return $this->addressLength;
    }

    /**
     * @return string
     */
    public function getHashFunction(): string
    {
        return $this->hashFunction;
    }

    public function getBalance($address)
    {
        return false;
    }

    public function getRate($currency = 'USD')
    {
        return CurrencyRate::getRate($this->getCode(), $currency);
    }
}