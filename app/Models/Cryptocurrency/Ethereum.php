<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 14:51
 */
namespace App\Models\Cryptocurrency;

use App\Flow\Api\Etherscan;
use App\Hash\Keccak256;
use App\Models\Enums\Algorithm;

class Ethereum extends BaseCryptocurrency
{
    protected $hashFunction = Algorithm::ID_ETHASH;

    public function getName()
    {
        return 'Ethereum';
    }

    public function getCode()
    {
        return 'ETH';
    }

    public function addressLength()
    {
        return 40;
    }

    public function walletValidationRule($address)
    {
        return $this->isAddress($address);
    }

    public function generateWallet()
    {
        return 'asdasdasdasdasdad';
    }

    /**
     * Checks if the given string is an address
     *
     * @method isAddress
     * @param {String} $address the given HEX adress
     * @return {Boolean}
     */
    function isAddress($address) {
        if (!preg_match('/^(0x)?[0-9a-f]{40}$/i',$address)) {
            // check if it has the basic requirements of an address
            return false;
        } elseif (!preg_match('/^(0x)?[0-9a-f]{40}$/',$address) || preg_match('/^(0x)?[0-9A-F]{40}$/',$address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
            // Otherwise check each case
            return $this->isChecksumAddress($address);
        }
    }

    /**
     * Checks if the given string is a checksummed address
     *
     * @method isChecksumAddress
     * @param {String} $address the given HEX adress
     * @return {Boolean}
     */
    function isChecksumAddress($address) {
        // Check each case
        $address = str_replace('0x','',$address);

        if (strlen($address) != 40)
            return false;

        return true;

        dump(strtolower($address));

        $addressHash = Keccak256::hash(strtolower($address), 256);

        dd($addressHash);


        $addressArray=str_split($address);
        $addressHashArray=str_split($addressHash);

        for($i = 0; $i < 40; $i++ ) {
            // the nth letter should be uppercase if the nth digit of casemap is 1
            if ((intval($addressHashArray[$i], 16) > 7 && strtoupper($addressArray[$i]) !== $addressArray[$i]) || (intval($addressHashArray[$i], 16) <= 7 && strtolower($addressArray[$i]) !== $addressArray[$i])) {
                return false;
            }
        }
        return true;
    }

    public function getBalance($address)
    {
        return floor_precise(Etherscan::getAddressBalance($address), 6);
    }
}