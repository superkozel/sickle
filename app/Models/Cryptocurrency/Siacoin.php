<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 18.02.2018
 * Time: 23:45
 */

namespace App\Models\Cryptocurrency;


class Siacoin extends BaseCryptocurrency
{
    public function getName()
    {
        return 'Siacoin';
    }

    public function getCode()
    {
        return 'SC';
    }

    public function walletValidationRule($address)
    {
        return strlen($address) == 76 && ctype_xdigit($address);
    }
}