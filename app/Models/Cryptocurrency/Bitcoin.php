<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 13.02.2018
 * Time: 16:40
 */

namespace App\Models\Cryptocurrency;

use App\Flow\Api\BlockchainInfo;

class Bitcoin extends BaseCryptocurrency
{
    function getName()
    {
        return 'Bitcoin';
    }

    public function getCode()
    {
        return 'BTC';
    }

    function walletValidationRule($address): string
    {
        return false;
    }

    public function getHashFunction(): string
    {
        return 'sha256';
    }

    /**
     * @param $address
     * @return bool|float
     */
    public function getBalance($address)
    {
        return floor_precise(BlockchainInfo::getAddressBalance($address), 8);
    }
}