<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 25.01.2018
 * Time: 16:43
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ActiveRecord\Page
 *
 * @mixin \Eloquent
 * @property mixed $content
 * @property int $id
 * @property string $url
 * @property string $path
 * @property string $h1
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUrl($value)
 */
class Page extends Model
{
    public static function boot()
    {
        parent::boot();

        static::saving(function($page){
            if (! $page->path) {
                $page->path = Rus2Lat($page->h1);
            }
        });

        static::saved(function($page)
        {
            $page->saveContent();
        });
    }

    public function getUrl($absolute = false) {
        $url = $this->url;
        if ($url != '/')
            $url = '/' . $url;
        return $url;
    }

    public function getView()
    {
        return view('pages.' . $this->path);
    }

    public function getContentAttribute()
    {
        return $this->path && file_exists($this->getFullPath()) ? file_get_contents($this->getFullPath()) : '';
    }

    public function getFullPath()
    {
        return resource_path('/views/pages/' . $this->path . '.blade.php');
    }

    protected $_content;

    public function setContentAttribute($content)
    {
        $this->_content = $content;
    }

    public function saveContent()
    {
        $fullPath = $this->getFullPath();

        if (! file_exists($fullPath)) {
            $file = fopen($fullPath, 'w') or die('Error opening file: ' + $fullPath);
            fclose($file);
        }

        if ($this->_content) {
            file_put_contents($fullPath, $this->_content);
        }

        return true;
    }

    public function render()
    {
        return $this->getView()->render();
    }
}