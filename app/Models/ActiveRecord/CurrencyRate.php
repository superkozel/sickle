<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 04.07.2018
 * Time: 2:40
 */

namespace App\Models\ActiveRecord;

class CurrencyRate extends \Eloquent
{
    protected $guarded = [];

    public static function getRate($currency1, $currency2)
    {
        $rate = CurrencyRate::where([
            'cryptocurrency' => $currency1,
            'currency' => $currency2
        ])->pluck('rate')->first();

        return $rate;
    }

    public static function saveRate($currency1, $currency2, $rate)
    {
        CurrencyRate::updateOrCreate([
            'cryptocurrency' => $currency1,
            'currency' => $currency2
        ], [
            'rate' => $rate
        ]);
    }

//    protected function alignPair()
//    {
//        if (strcmp($currency1, $currency2) < 0)
//            list($currency1, $currency2) = [$currency2, $currency1];
//    }
}