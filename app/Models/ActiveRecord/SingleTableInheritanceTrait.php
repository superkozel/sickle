<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 20.05.2018
 * Time: 23:27
 */

namespace App\Models\ActiveRecord;

trait SingleTableInheritanceTrait
{
    use \Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

    public static function getSingleType()
    {
        return static::$singleTableType;
    }

    public static function classFactory($classType, $attributes = [])
    {
        $childTypes = static::getSingleTableTypeMap();
        if (array_key_exists($classType, $childTypes)) {
            $class = $childTypes[$classType];
            $instance = (new $class);
            $instance->setFilteredAttributes($attributes);
            return $instance;
        } else {
            // Throwing either of the exceptions suggests something has gone very wrong with the Global Scope
            // There is not graceful recovery so complain loudly.
            throw new SingleTableInheritanceException("Cannot construct newFromBuilder for unrecognized type=$classType");
        }
    }

    public static function isSubclass($object)
    {
        $map = static::getSubclasses();

        dd($map);
    }

    public static function getSubclasses()
    {
        return static::getSingleTableTypeMap();
    }
}