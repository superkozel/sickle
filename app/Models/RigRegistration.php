<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 06.03.2018
 * Time: 6:08
 */

namespace App\Models;


/**
 * App\Models\RigRegistration
 *
 * @property string $certname
 * @property string $email
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RigRegistration new()
 */
class RigRegistration extends \Eloquent
{
    const RIG_TYPE_AMD = 1;
    const RIG_TYPE_NVIDIA = 2;
    const RIG_TYPE_ASIC = 3;

    public static function getNewByHash($hash)
    {
        RigRegistration::where('hash', $hash)->where('rig_id', null)->first();
    }

    public static function getByHash($hash)
    {
        RigRegistration::where('hash', $hash)->first();
    }

    public function getRigTypes()
    {
        return [
            RigRegistration::RIG_TYPE_AMD       => 'AMD',
            RigRegistration::RIG_TYPE_NVIDIA    => 'Nvidia',
            RigRegistration::RIG_TYPE_ASIC      => 'ASIC',
        ];
    }

    public function scopeNew($query)
    {
        $query->whereNull('rig_id');
    }
}