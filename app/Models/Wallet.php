<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 12.02.2018
 * Time: 16:59
 */

namespace App\Models;
use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Cryptocurrency;
use App\Notifications\Wallet\Changed;

/**
 * App\Models\Wallet
 *
 * @mixin \Eloquent
 * @property-read \App\Models\Cluster $cluster
 * @property int $id
 * @property string $address
 * @property int $currency_id
 * @property int $cluster_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereUpdatedAt($value)
 */
class Wallet extends \Eloquent
{
    public static function boot()
    {
        parent::boot();

        static::updated(function(Wallet $wallet)
        {
            $notification = new Changed($wallet);
            $wallet->syncChanges();

            if ($wallet->wasChanged('address')) {
                $wallet->cluster->notify($notification);
            }
        });
    }

    /**
     * @return BaseCryptocurrency
     */
    public function getCurrency()
    {
        return Cryptocurrency::find($this->currency_id);
    }

    public function getCode()
    {
        return $this->generateCode();
    }

    public function generateCode()
    {
        return strtoupper($this->getCurrency()->getCode() . '_wallet_' . $this->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster()
    {
        return $this->belongsTo(Cluster::class);
    }

    public function getEditUrl()
    {
        return action('Personal\WalletController@edit', ['wallet' => $this]);
    }

    public function getDeleteUrl()
    {
        return action('Personal\WalletController@delete', ['wallet' => $this]);
    }

    public function getBalanceLink()
    {
        return 'https://etherscan.io/address/' . $this->address;
//        $this->getCurrency()->getBalanceLink();
    }

    public function getBalance()
    {
        return $this->getCurrency()->getBalance($this->address);
    }

    public function getConfigurations()
    {
        $wallet = $this;
        return $this->cluster->configurations()->where(function($query) use ($wallet){
            $query->where('wallet_id', $wallet->id);
            $query->orWhere('second_wallet_id', $wallet->id);
        });
    }
}