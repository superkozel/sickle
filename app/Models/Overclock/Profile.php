<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 24.02.2018
 * Time: 23:14
 */

namespace App\Models\Overclock;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Overclock\Profile
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $core_clock
 * @property int|null $core_state
 * @property int|null $core_voltage
 * @property int $memory_clock
 * @property int|null $memory_state
 * @property int|null $fan_speed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereCoreClock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereCoreState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereCoreVoltage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereFanSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereMemoryClock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereMemoryState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Overclock\Profile whereUpdatedAt($value)
 */
class Profile extends Model
{
    public $table = 'overclock_profiles';

    public function isValidForGpu()
    {
        return true;
    }
}