<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 28.02.2018
 * Time: 21:24
 */

namespace App\Models;

use App\User;

class Pricing
{
    const MONTHLY_RIG_FEE = 3;

    public static function calculateMonthlyPaymentForUser($user)
    {
        if ($user->getOwnCluster()->rigs->count() <= 3)
            return 0;

        return $user->getOwnCluster()->rigs->count() * static::getMonthlyRigFee($user);
    }

    public static function getMonthlyRigFee(User $user)
    {
        return static::MONTHLY_RIG_FEE;
    }

    public static function getDaysLeft(User $user)
    {
        $rigCount = $user->getOwnCluster()->rigs->count();
        if (! $rigCount)
            return 0;
        return floor($user->balance / ($rigCount * static::getMonthlyRigFee($user)));
    }
}