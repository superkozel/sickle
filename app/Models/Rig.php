<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 17.03.2018
 * Time: 9:52
 */

namespace App\Models;

use Adldap\Models\Entry;
use App\Flow\Ldap\Entry\Computer;
use App\Flow\Ldap\Export;
use App\Flow\Ldap\Import;
use App\Flow\Puppet\Module\FanControl;
use App\Models\Rig\Asic;
use App\Models\Rig\FactsProvider;
use App\Models\Rig\GpuRig;
use App\Models\Rig\StatusInterface;
use Nanigans\SingleTableInheritance\Exceptions\SingleTableInheritanceException;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

/**
 * App\Models\Rig
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 * @mixin \Eloquent
 */
class Rig extends \Eloquent implements StatusInterface
{
    use \App\Models\ActiveRecord\SingleTableInheritanceTrait;

    /**
     * @const длина уникального хэша рига
     */
    const HASH_LENGTH = 44;

    const DEFAULT_RED_TEMPERATURE = 65;

    const TYPE_AMD = 'gpu_amd_rig';
    const TYPE_NVIDIA = 'gpu_nvidia_rig';
    const TYPE_ASIC = 'asic';

    public $table = 'rigs';

    public static $puppetClass = null;//inherit me

    protected static $singleTableType = 'rig';

    protected static $singleTableTypeField = 'type';

    protected static $singleTableSubclasses = [GpuRig::class, Asic::class];

    public $incrementing = true;

    public $fillable = [
        'hostname', 'type', 'name', 'description'
    ];

    protected $casts = [
        'facts' => 'array',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'online_at',
        'reported_at',
    ];

    protected $attributes = [
        'fan_mode' => FanControl::MODE_FULL_SPEED,
    ];

    public function getFact($fact, $default = null)
    {
        return array_get($this->facts, $fact, $default);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function configuration()
    {
        return $this->belongsTo(Configuration::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster()
    {
        return $this->belongsTo(Cluster::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(RigMessage::class);
    }

    public function isGpuRig()
    {
        return is_subclass_of($this, GpuRig::class);
    }

    public function isAmd()
    {
        return $this instanceof GpuRig\Amd;
    }

    public function isNvidia()
    {
        return $this instanceof GpuRig\Nvidia;
    }

    public function isAsic()
    {
        return is_subclass_of($this, Asic::class);
    }

    /**
     * @return Computer
     */
    public function getLdapRig()
    {
        return Import::create()->getRigByHash($this->ldap_id);
    }

    public function getDisplayName()
    {
        return $this->name ?: $this->hostname;
    }

    public function isOnline()
    {
        return $this->online;
    }

    public function isOverheated()
    {
        return $this->getTemperature() > $this->getRedTemperature();
    }

    public function isMining()
    {
        return $this->hashrate > 0;
    }

    public function scopeOnline($query)
    {
        return $query;
    }

    public function scopeOffline($query)
    {
        return $query->where('id', '<', 0);
    }

    public function scopeNotMining($query)
    {
        return $query->where('hashrate', '=', 0);
    }

    public function getRedTemperature()
    {
        return $this->red_temperature ?: Rig::DEFAULT_RED_TEMPERATURE;
    }

    public function getPuppetClass()
    {
        return static::$puppetClass;
    }

    public static function getOptionsTypes()
    {
        return [
            GpuRig\Amd::$singleTableType => 'AMD',
            GpuRig\Nvidia::$singleTableType => 'Nvidia',
        ];
    }

    public function updateType($type)
    {
        return Rig::where('id', $this->id)->update(['type' => $type]);
    }

    /**
     * Where configuration is not
     * up to date due to checksum check
     * @param $query
     * @return mixed
     */
    public function scopeNotUpToDate($query)
    {
        return $query->whereHas('configuration', function($query){
            $query->where('checksum', '!=', \DB::raw('rigs.configuration_checksum'));
        });
    }

    public function isUpToDate()
    {
        return $this->configuration_checksum == $this->configuration->checksum;
    }

    public function getUrl()
    {
        return action('Personal\RigController@show', ['rig' => $this]);
    }

    public function getEditUrl()
    {
        return action('Personal\RigController@edit', ['rig' => $this]);
    }

    public function getDeleteUrl()
    {
        return action('Personal\RigController@remove', ['rig' => $this]);
    }

    public function getTemperature(){}

    public function getHashrate(){}

    public function getSecondHashrate(){}

    public function getPower(){}

    public function getLastBootTime(){}

    public function getCertName()
    {
        return $this->ldap_id;
    }

    public function getIpAddress()
    {
        return $this->getFact('ipaddress');
    }
}