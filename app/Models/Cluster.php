<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 12.02.2018
 * Time: 16:51
 */

namespace App\Models;
use App\Models\Enums\Cryptocurrency;
use App\User;

/**
 * App\Models\Cluster
 *
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Configuration[] $configurations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rig[] $rigs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wallet[] $wallets
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cluster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cluster whereName($value)
 */
class Cluster extends \Eloquent
{
    const USER_ROLE_OWNER = 1;
    const USER_ROLE_ADMIN = 2;
    const USER_ROLE_GUEST = 3;

    public $timestamps = false;

    public static function getUserRoleOptions()
    {
        return [
            Cluster::USER_ROLE_OWNER => 'owner',
            Cluster::USER_ROLE_ADMIN => 'admin',
            Cluster::USER_ROLE_GUEST => 'guest',
        ];
    }

    public static function getGrantableUserRoleOptions()
    {
        return array_except(static::getUserRoleOptions(),[Cluster::USER_ROLE_OWNER]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function accesses()
    {
        return $this->users()->wherePivot('role', '!=', Cluster::USER_ROLE_OWNER);
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->users()->where('role', Cluster::USER_ROLE_OWNER)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function configurations()
    {
        return $this->hasMany(Configuration::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rigs()
    {
        return $this->hasMany(Rig::class);
    }

    /**
     * @return bool
     */
    public function isOwner(User $user)
    {
        return $this->getOwner()->id == $user->id;
    }

    public function canWrite(User $user)
    {
        $roleUser = $this->users()->where('id', $user->id)->withPivot('role')->first();

        return $roleUser->pivot->role == Cluster::USER_ROLE_OWNER || $roleUser->pivot->role == Cluster::USER_ROLE_ADMIN;
    }

    public function canRead(User $user)
    {
        return $this->users()->where('id', $user->id)->count() > 0;
    }

    public function getTotalPower()
    {
        $total = 0;
        foreach ($this->rigs as $rig) {
            $total += $rig->getPower();
        }
        return $total;
    }

    public function getTotalGpuCount()
    {
        $total = 0;
        foreach ($this->rigs as $rig) {
            $total += count($rig->getGpus());
        }
        return $total;
    }

    public function getMissingGpuCount()
    {
        $total = 0;
        foreach ($this->rigs as $rig) {
            $total += $rig->getMissingGpuCount();

        }
        return $total;
    }

    public function getHashrate($currencyId)
    {
        return $this->rigs->map(function(Rig $rig) use ($currencyId){
            $rig->calculateHashrate($currencyId);
        })->sum();
    }

    public function getHashrates()
    {
        $hashrates = [];
        foreach ($this->rigs as $rig) {
            if (! $rig->configuration)
                continue;

            if ($currency = $rig->configuration->getCurrency() AND $rig->getHashrate() > 0) {
                if (! isset($hashrates[$currency->getCode()]))
                    $hashrates[$currency->getCode()] = 0;

                $hashrates[$currency->getCode()] += $rig->getHashrate();
            }

            if ($rig->configuration->getSecondCurrency() && $rig->getSecondHashrate() > 0) {
                if (! isset($hashrates[$currency->getSecondCurrency()]))
                    $hashrates[$currency->getSecondCurrency()] = 0;

                $hashrates[$rig->configuration->getSecondCurrency()->getCode()] += $rig->getSecondHashrate();
            }
        }

        return $hashrates;
    }

    public function getMonthlyFee()
    {
        return $this->rigs()->count() * 3;
    }

    public function notify($notification)
    {
        $this->getOwner()->notify($notification);
    }
}