<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 23.04.2018
 * Time: 18:18
 */

namespace App\Models\Rig\GpuRig;


use App\Flow\Ldap\RigConfiguration;
use App\Models\Rig;
use App\Models\Rig\GpuRig;

/**
 * App\Models\Rig\GpuRig\Nvidia
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 * @mixin \Eloquent
 */
class Nvidia extends GpuRig
{
    protected static $singleTableType = Rig::TYPE_NVIDIA;

    public static $puppetClass = RigConfiguration::PUPPETCLASS_GPU_NVIDIA;
}