<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 17.03.2018
 * Time: 22:07
 */

namespace App\Models\Rig\Asic\Bitmain;


use App\Models\Enums\Algorithm;
use App\Models\Rig\Asic;

/**
 * App\Models\Rig\Asic\Bitmain\S8
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 * @mixin \Eloquent
 */
class S8 extends Asic
{
    protected static $singleTableType = 's8';

    public function algorithm()
    {
        return Algorithm::sha256();
    }
}