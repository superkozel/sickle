<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 17.03.2018
 * Time: 22:01
 */

namespace App\Models\Rig\Asic\Bitmain;

use App\Models\Enums\Algorithm;
use App\Models\Rig\Asic;

/**
 * App\Models\Rig\Asic\Bitmain\L3
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 * @mixin \Eloquent
 */
class L3 extends Asic
{
    protected static $singleTableType = 'l3';

    public function algorithm()
    {
        return Algorithm::create(Algorithm::ID_SCRYPT);
    }
}