<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 08.02.2018
 * Time: 19:13
 */

namespace App\Models\Rig;
use App\Flow\Ldap\Import;
use App\Models\Cryptocurrency\BaseCryptocurrency;
use App\Models\Enums\Cryptocurrency;
use App\Models\Gpu;
use App\Models\Rig;
use App\Models\Rig\FactsProvider;
use Illuminate\Support\Collection;

/**
 * App\Models\Rig
 *
 * @mixin \Eloquent
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 * @property string $id
 * @property string $name
 * @property string|null $description
 * @property string|null $facts
 * @property int|null $configuration_id
 * @property int $cluster_id
 * @property float|null $hashrate
 * @property float|null $second_hashrate
 * @property float|null $temperature
 * @property int|null $gpu_count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereConfigurationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereFacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereGpuCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereHashrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereSecondHashrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereTemperature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 */
class GpuRig extends Rig
{
    protected static $singleTableType = 'gpu_rig';

    protected static $singleTableSubclasses = [Rig\GpuRig\Amd::class, Rig\GpuRig\Nvidia::class];

    public function getTemperature()
    {
        return $this->getGpus()->map(function(Gpu $gpu){return $gpu->getTemperature();})->max();
    }

    public function getHashrate()
    {
        $this->loadMissing(['configuration']);

        if (! $this->configuration OR ! $this->configuration->getCurrency())
            return 0;

        return is_null($this->hashrate) ? $this->calculateHashrate($this->configuration->getCurrency()) : $this->hashrate;
    }

    public function getSecondHashrate()
    {
        $this->loadMissing(['configuration']);

        if (! $this->configuration OR ! $this->configuration->getSecondCurrency())
            return 0;

        return is_null($this->second_hashrate) ? $this->calculateHashrate($this->configuration->getSecondCurrency()) : $this->second_hashrate;
    }

    public function calculateHashrate(BaseCryptocurrency $cryptocurrency)
    {
        $config = $this->configuration;

        return $this->getGpus()->map(function(Gpu $gpu) use ($config, $cryptocurrency){
            if ($cryptocurrency->is($config->getCurrency()))
                return $gpu->getHashrate();
            else if ($cryptocurrency->is($config->getSecondCurrency()))
                return $gpu->getSecondHashrate();
            else
                return 0;
        })->sum();
    }

    public function getPower()
    {
        return $this->getGpus()->map(function(Gpu $gpu){return $gpu->getPower();})->sum();
    }

    public function getIp()
    {
        return '192.133.1.313';
    }

    public function getWallet(Cryptocurrency $cryptocurrency)
    {
        return $this->configuration->getWalletId($cryptocurrency);
    }

    /** @var Gpu[]|Collection */
    protected $gpus;

    /**
     * @return Gpu[]|Collection
     */
    public function getGpus()
    {
        if (! isset($this->gpus)) {
            $rig = $this;
            $this->gpus = collect(array_map(function($data) use ($rig){return (new Gpu($data, $this->configuration))->setRig($rig);}, $this->getFact('gpus', [])))
//            ->keyBy(function($gpu){return $gpu->getBusId();})
            ;
        }

        return $this->gpus;
    }

    public function getMissingGpuCount()
    {
        return $this->missing_gpu_count;
    }

    /**
     * @return Gpu
     */
    public function getGpu($id)
    {
        return $this->getGpus()->get($id);
    }

    /**
     * @return mixed
     */
    public function getMiner()
    {
        return $this->configuration ? $this->configuration->getMiner() : null;
    }

    public function getProcessorFrequency()
    {
        return $this->getFact('processors.count') . 'x' . $this->getFact('processors.speed') ;
    }

    public function getLastBootTime()
    {
        return $this->getFact('system_uptime.uptime');
    }

    public function getMaxTemperature()
    {
        return 65;
    }

    public function generateName()
    {
//        $name = $this->cluster->getOwner()->name . "'s Rig #" . ($this->cluster->rigs()->count() + 1);
        $name = '';

        if (($gpus = $this->getGpus())->count() > 0) {
            $values = array_count_values($gpus->map(function($gpu){return $gpu->getName();})->all());
            array_walk($values, function(&$v, $k){$v = $v .'x' . $k;});
            $name .= ' ' . join(', ', $values);
        }
        else {
            $name = 'Rig ' . $this->cluster->id . '-' . $this->cluster->rigs()->count();
        }

        return $name;
    }


    public function generateHostname()
    {
    }
}