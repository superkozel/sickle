<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 17.03.2018
 * Time: 9:46
 */

namespace App\Models\Rig;

use App\Flow\Ldap\RigConfiguration;
use App\Models\Rig;

abstract class Asic extends Rig
{
    protected static $singleTableType = Rig::TYPE_ASIC;

    public static $puppetClass = RigConfiguration::PUPPETCLASS_ASIC;

    protected static $singleTableSubclasses = [
        Rig\Asic\Bitmain\S9::class,
        Rig\Asic\Bitfury\B8::class,
    ];
}