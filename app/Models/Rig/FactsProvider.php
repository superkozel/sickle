<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 10.03.2018
 * Time: 12:42
 */

namespace App\Models\Rig;


use App\Flow\Puppet\Api;
use App\Models\Rig;

class FactsProvider
{
    protected static $rigs = [];
    protected static $requiredFacts = [];
    protected static $facts = [];

    public static function loadFacts($rigs = [], $facts = [])
    {
        if (! is_array($rigs))
            $rigs = [$rigs];
        
        static::addRigs($rigs);
        static::requireFacts($facts);

        $newFacts = Api::getRigsFacts(array_pluck(static::$rigs, 'ldap_id'), static::$requiredFacts);
        if (! is_array($newFacts))
            $newFacts = [];

        foreach (static::$rigs as $rig) {
            foreach (static::$requiredFacts as $fact) {
                static::$facts[$rig->ldap_id][$fact] = null;
            }
        }

        foreach ($newFacts as $fact) {
            static::$facts[$fact['certname']][$fact['name']] = $fact['value'];
        }

        static::$rigs = [];
        static::$requiredFacts = [];
    }

    public static function addRig(Rig $rig)
    {
        static::$rigs[] = $rig;
    }

    public static function addRigs($rigs)
    {
        static::$rigs = array_merge(static::$rigs, $rigs);
    }

    public static function requireFacts($facts)
    {
        static::$requiredFacts = array_merge(static::$requiredFacts, $facts);
    }

    public static function getFact(Rig $rig, $fact, $default = null)
    {
        if (! array_has(static::$facts, $rig->ldap_id . '.' . $fact))
            throw new \Exception('Fact not preloaded: ' . $fact);

        $result = array_get(static::$facts, $rig->ldap_id . '.' . $fact, $default);

        if (is_null($result))
            return $default;

        return $result;
    }
}