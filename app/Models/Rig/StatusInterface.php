<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 17.03.2018
 * Time: 10:02
 */

namespace App\Models\Rig;


interface StatusInterface
{
    public function getTemperature();

    public function getHashrate();

    public function getSecondHashrate();

    public function getPower();

    public function getLastBootTime();
}