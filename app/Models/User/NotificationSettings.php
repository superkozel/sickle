<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.05.2018
 * Time: 19:15
 */

namespace App\Models\User;


use NotificationChannels\Telegram\TelegramChannel;

class NotificationSettings
{
    const CHANNEL_TELEGRAM = 'telegram';
    const CHANNEL_MAIL = 'mail';

    const RIG_NOT_MINING = 'rig:not_mining';
    const RIG_ONLINE = 'rig:online';
    const RIG_OFFLINE = 'rig:offline';
    const RIG_GPU_MISSING = 'rig:gpu_missing';
    const RIG_HIGH_TEMPERATURE = 'rig:high_temperature';
    const RIG_SUMMARY = 'rig:summary';
    const RIG_ERROR = 'rig:error';

    /** @var true */
    protected $disableAll;

    /** @var array */
    protected $disabledChannels;

    /** @var array */
    protected $disabledNotifications;

    function __construct($data)
    {
        $this->disableAll = ! empty($data['disable_all']);
        $this->disabledChannels = array_get($data, 'disabled_channels', []);
        $this->disabledNotifications = array_get($data, 'disabled_notifications', []);
    }

    public function toArray()
    {
        if ($this->disableAll)
            $data['disable_all'] = true;

        $data['disabled_channels'] = $this->disabledChannels;

        $data['disabled_notifications'] = $this->disabledNotifications;

        return $data;
    }

    protected static $channelOptions = [
        NotificationSettings::CHANNEL_MAIL => 'Mail',
        NotificationSettings::CHANNEL_TELEGRAM => 'Telegram'
    ];

    public static function getChannelOptions()
    {
        return static::$channelOptions;
    }

    protected static $notificationOptions = [
        NotificationSettings::RIG_ONLINE => 'Rig online',
        NotificationSettings::RIG_OFFLINE => 'Rig offline',
        NotificationSettings::RIG_NOT_MINING => 'Rin miner not working',
        NotificationSettings::RIG_HIGH_TEMPERATURE => 'GPU Temp >= Red Temp + 3°',
        NotificationSettings::RIG_GPU_MISSING => 'One or more GPUs missing',
        NotificationSettings::RIG_SUMMARY => 'Summary report with problems',
        NotificationSettings::RIG_ERROR => 'Miner worker errors',
    ];

    public static function getNotificationOptions()
    {
        return static::$notificationOptions;
    }

    public function channelEnabled($channel)
    {
        if ($channel == TelegramChannel::class OR $channel == 'telegram') {
            return ! in_array('telegram', $this->disabledChannels);
        }
        else if ($channel == 'mail') {
            return ! in_array('mail', $this->disabledChannels);
        }

        return true;
    }

    public function notificationEnabled($notificationId)
    {
        if (! $notificationId)
            throw new \Exception('Notification id is not provided');

        return (! in_array($notificationId, $this->disabledNotifications));
    }

    /**
     * @return true
     */
    public function getDisableAll()
    {
        return $this->disableAll;
    }

    /**
     * @param true $disableAll
     */
    public function setDisableAll($disableAll): void
    {
        $this->disableAll = $disableAll;
    }

    /**
     * @return array
     */
    public function getDisabledChannels(): array
    {
        return $this->disabledChannels;
    }

    /**
     * @param array $disabledChannels
     */
    public function setDisabledChannels(array $disabledChannels): void
    {
        $this->disabledChannels = $disabledChannels;
    }

    public function disableChannel($channel)
    {
        $this->disabledChannels[] = $channel;
    }

    /**
     * @return array
     */
    public function getDisabledNotifications(): array
    {
        return $this->disabledNotifications;
    }

    /**
     * @param array $disabledNotifications
     */
    public function setDisabledNotifications(array $disabledNotifications): void
    {
        $this->disabledNotifications = $disabledNotifications;
    }
}