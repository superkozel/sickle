<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.02.2018
 * Time: 2:08
 */

namespace App\Models\User;

use App\User;

/**
 * App\Models\User\SecurityLog
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $event_type_id
 * @property string $ip
 * @property string $session_id
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereEventTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereUserId($value)
 */
class SecurityLog extends \Eloquent
{
    protected $table = 'user_security_logs';

    const UPDATED_AT = null;

    const EVENT_FAILED = 1;
    const EVENT_AUTHENTICATED = 2;
    const EVENT_LOGOUT = 3;
    const EVENT_LOCKOUT = 4;
    const EVENT_NEW_LOCATION = 5;
    const EVENT_LOCATION_CONFIRMED = 6;

    public function getOptionsEventType()
    {
        return [
            SecurityLog::EVENT_FAILED => 'Failed authorization',
            SecurityLog::EVENT_AUTHENTICATED => 'Authorization',
            SecurityLog::EVENT_LOGOUT => 'Logout',
            SecurityLog::EVENT_LOCKOUT => 'Lockout',
            SecurityLog::EVENT_NEW_LOCATION => 'New location login',
            SecurityLog::EVENT_LOCATION_CONFIRMED => 'Login location confirmed',
        ];
    }
    public static function log(User $user, $eventTypeId)
    {
        return SecurityLog::forceCreate([
            'event_type_id' => $eventTypeId,
            'ip' => \Request::ip(),
            'session_id' => \Session::getId(),
            'user_id' => $user->id
        ]);
    }

}