<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 22.02.2018
 * Time: 1:31
 */

namespace App\Models\User;

use App\Notifications\Auth\ConfirmNewLocation;
use App\User;
use Carbon\Carbon;

/**
 * App\Models\User\KnownLocation
 *
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $ip
 * @property string $secret
 * @property \Carbon\Carbon|null $created_at
 * @property int $confirmed
 * @property string|null $email_token
 * @property \Carbon\Carbon|null $email_sent_at
 * @property int $email_sent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereEmailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereEmailSentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereEmailToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereUserId($value)
 */
class KnownLocation extends \Eloquent
{
    protected $table = 'user_known_locations';
    protected $dates = ['email_sent_at'];

    const TOKEN_LENGTH = 50;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function new(User $user)
    {
        $newLocation = static::forceCreate([
            'user_id' => $user->id,
            'ip' => \Request::ip(),
            'user_agent' => \Request::userAgent(),
            'secret' => static::generateToken(),
        ]);

        return $newLocation;
    }

    public function remember()
    {
        $this->setRememberCookie($this->user, $this->secret);

        return $this;
    }

    public static function setRememberCookie($user, $value)
    {
        $cookie = \Cookie::forever(static::getUserCookieName($user), $value);
        \Cookie::queue($cookie);
    }

    public static function getRememberCookie($user)
    {
        return \Cookie::get(KnownLocation::getUserCookieName($user));
    }

    public static function getUserCookieName(User $user)
    {
        return md5('location' . $user->id);
    }

    public static function generateToken()
    {
        return strval(bin2hex(openssl_random_pseudo_bytes(static::TOKEN_LENGTH / 2)));
    }

    public static function isKnown($ip, User $user)
    {
        return $user->locations()->where('ip', $ip)->where('confirmed', 1)->count() > 0;
    }

    public function canSendAnotherNotification()
    {
        return true;
//        return ! $this->email_sent OR ($this->email_sent_at->diff('now') > 60)
    }

    public function sendNotification()
    {
        $this->email_token = $this::generateToken();
        $this->email_sent = true;
        $this->email_sent_at = Carbon::now();

        $notification = new ConfirmNewLocation($this);
        \Notification::send($this->user, $notification);
    }

    public function confirm()
    {
        SecurityLog::log($this->user, SecurityLog::EVENT_LOCATION_CONFIRMED);

        $this->confirmed = true;
        $this->email_sent = 0;
        $this->email_token = null;
        $this->email_sent_at = null;

        return $this;
    }

    public static function findUserLocationByIpOrSecret(User $user)
    {
        $location = $user->locations()
            ->where('ip', \Request::ip())
            ->first();

        if (! $location && $secret = KnownLocation::getRememberCookie($user)) {
            $location = $user->locations()
                ->where('secret', $secret)
                ->first()
            ;
        }

        return $location;
    }

    public static function checkLocationAndSendNotificationProcedure(User $user)
    {
        /** @var KnownLocation $location */
        $location = KnownLocation::findUserLocationByIpOrSecret($user);

        if ($location) {
            $location->remember();
            $location->ip = \Request::ip();
            $location->user_agent = \Request::userAgent();
            $location->save();
        }
        else {
            $location = KnownLocation::new($user);
        }

        if (! $location->confirmed) {
            if ($user->locations()->where('confirmed', 1)->count() == 0) {
                $location->confirm();
            }
            else if (! $location->email_sent) {
                $location->sendNotification();
            }
        }

        $location->save();

        return $location;
    }


}