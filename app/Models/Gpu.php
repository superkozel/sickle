<?php
/**
 * Created by PhpStorm.
 * User: d.zykov
 * Date: 08.02.2018
 * Time: 19:31
 */

namespace App\Models;

use App\Models\Enums\Cryptocurrency;

class Gpu
{
    /** @var string */
    protected $name;
    /** @var float */
    protected $hashRate;
    /** @var float */
    protected $secondHashrate;
    /** @var float */
    protected $temperature;
    /** @var string */
    protected $bios;
    /** @var float */
    protected $memorySize;
    /** @var string */
    protected $memoryType;
    /** @var string */
    protected $busid;
    /** @var float */
    protected $power;
    /** @var GpuModel */
    protected $model;

    /**
     * @var Rig
     */
    protected $rig;

    public function __construct($data, Configuration $configuration = null)
    {
        $this->name = array_get($data, 'name', 'SYSTEMERRORNONAME');
        $this->busid = array_get($data, 'busid');
        $this->hashRate = $configuration ? array_get($data, 'eth_h') / 1000 : 0;
        $this->secondHashrate = ((! $configuration OR ! $configuration->isDual() OR array_get($data, 'dcr_h') == 'off') ? 0 : array_get($data, 'dcr_h')) / 1000;
        $this->temperature = array_get($data, 'temp');
        $this->bios = array_get($data, 'vbios');
        $this->memorySize = array_get($data, 'memory');
        $this->memoryType = array_get($data, 'mem_type');
        $this->fanSpeed = array_get($data, 'fan');
        $this->power = array_get($data, 'power');

        $this->model = GpuModel::identify($this);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Gpu
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHashRate()
    {
        return $this->hashRate;
    }

    /**
     * @param mixed $hashRate
     * @return Gpu
     */
    public function setHashRate($hashRate)
    {
        $this->hashRate = $hashRate;
        return $this;
    }

    /**
     * @return float
     */
    public function getSecondHashrate()
    {
        return $this->secondHashrate;
    }

    /**
     * @param float $secondHashrate
     * @return Gpu
     */
    public function setSecondHashrate($secondHashrate): Gpu
    {
        $this->secondHashrate = $secondHashrate;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @param mixed $temperature
     * @return Gpu
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBios()
    {
        return $this->bios;
    }

    /**
     * @param mixed $bios
     * @return Gpu
     */
    public function setBios($bios)
    {
        $this->bios = $bios;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemorySize()
    {
        return $this->memorySize;
    }

    /**
     * @param mixed $memorySize
     * @return Gpu
     */
    public function setMemorySize($memorySize)
    {
        $this->memorySize = $memorySize;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemoryType()
    {
        return $this->memoryType;
    }

    /**
     * @param mixed $memoryType
     * @return Gpu
     */
    public function setMemoryType($memoryType)
    {
        $this->memoryType = $memoryType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBusId()
    {
        return $this->busid;
    }

    /**
     * @param mixed $busid
     */
    public function setBusId($busid)
    {
        $this->busid = $busid;
    }

    /**
     * @return mixed
     */
    public function getFanSpeed()
    {
        return round($this->fanSpeed);
    }

    /**
     * @param mixed $fanSpeed
     * @return Gpu
     */
    public function setFanSpeed($fanSpeed)
    {
        $this->fanSpeed = $fanSpeed;
        return $this;
    }

    /**
     * @return Rig
     */
    public function getRig(): Rig
    {
        return $this->rig;
    }

    /**
     * @param Rig $rig
     * @return $this
     */
    public function setRig(Rig $rig)
    {
        $this->rig = $rig;

        return $this;
    }

    /**
     * @return float
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param float $power
     * @return Gpu
     */
    public function setPower($power): Gpu
    {
        $this->power = $power;
        return $this;
    }

    /**
     * @return GpuModel
     */
    public function getModel(): GpuModel
    {
        return $this->model;
    }

    /**
     * @param GpuModel $model
     * @return Gpu
     */
    public function setModel(GpuModel $model): Gpu
    {
        $this->model = $model;
        return $this;
    }

    public function isFine()
    {
        return true;
    }

    public function getUrl()
    {
        return action('Personal\GpuController@index', ['rig' => $this->getRig(), 'id' => $this->getBusId()]);
    }

}