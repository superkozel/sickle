<?php
/**
 * Created by PhpStorm.
 * User: Superkozel
 * Date: 24.02.2018
 * Time: 23:27
 */

namespace App\Models;

/**
 * App\Models\GpuModel
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $name
 * @property int $memory_size
 * @property int $memory_type_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GpuModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GpuModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GpuModel whereMemorySize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GpuModel whereMemoryTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GpuModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GpuModel whereUpdatedAt($value)
 */
class GpuModel extends \Eloquent
{
    const MEMORY_HYNIX = 1;
    const MEMORY_SAMSUNG = 2;
    const MEMORY_MICRONE = 3;

    public static function identify(Gpu $gpu)
    {
        return null;

        dd($gpu);
        return static::firstOrCreate([
            'name' => $gpu->getName(),
            'memory_type_id' => static::identifyMemoryType($gpu->getMemoryType()),
            'memory_size' => $gpu->getMemorySize()
        ])
        ;
    }

    public static function identifyMemoryType($type)
    {
        dd($type);
    }
}