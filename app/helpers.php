<?php

function short_hash($str, $len = 5)
{
    return substr($str, 0, $len) . '......' . substr($str, -$len, $len);
}

function short_text($str, $len, $after = '...')
{
    if (mb_strlen($str) > $len) {
        return mb_substr($str, 0, $len) . $after;
    }
    else {
        return $str;
    }
}

function cc(\App\Models\Cryptocurrency\BaseCryptocurrency $cryptocurrency, $htmlOptions = [])
{
    $map = [
        \App\Models\Enums\Cryptocurrency::ID_SIACOIN => 'SIA'
    ];

    if (isset($map[$cryptocurrency->getId()]))
        $code = $map[$cryptocurrency->getId()];
    else
        $code = $cryptocurrency->getCode();

    $htmlOptions['class'] = 'cc ' .mb_strtoupper($code);
    $htmlOptions['title'] = $cryptocurrency->getName();

    return Html::tag('i', '', $htmlOptions);
}

/**
 * Round to significant digits
 * @param $value
 * @param $digits
 * @return float
 */
function significant_round($value, $digits)
{
    if ($value == 0) {
        $decimalPlaces = $digits - 1;
    } elseif ($value < 0) {
        $decimalPlaces = $digits - floor(log10($value * -1)) - 1;
    } else {
        $decimalPlaces = $digits - floor(log10($value)) - 1;
    }

    $answer = round($value, $decimalPlaces);
    return $answer;
}

function floor_precise($number, $precision)
{
    return (int) floor($number * pow(10, $precision)) / pow(10, $precision);
}

function fa($icon, $options = [])
{
    $options['class'] = 'fa fa-' . $icon;

    return Html::tag('i', '', $options);
}

function page_url($id)
{
    if (is_object($id))
        $page = $id;
    else
        $page = \App\Models\Page::find($id);

    return $page->getUrl();
}

function random_hex_string($length) {
    return bin2hex(openssl_random_pseudo_bytes($length / 2));
}