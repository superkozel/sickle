<?php

namespace Tests\Feature;

use App\Notifications\Rig\NotMining;
use App\Notifications\Rig\GpuIsMissing;
use App\Notifications\Rig\Offline;
use App\Notifications\Rig\Online;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

class NotificationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGpuIsMissing()
    {
        $user = User::find(1);
        $rig = $user->getOwnCluster()->rigs->first();


        $cluster = $user->find(1)->clusters()->first();
        $rig = $cluster->rigs()->first();

        $notifications[] = new GpuIsMissing($rig, 6, 5);
        $notifications[] = new Offline($rig);
        $notifications[] = new Online($rig);
        $notifications[] = new GpuIsMissing($rig, 6, 5);
        $notifications[] = new NotMining($rig);

        foreach($notifications as $n) {
            $cluster->notify($n);
        }
//        Mail::fake();
//
//        // Perform order shipping...
//
//        Mail::assertSent(OrderShipped::class, function ($mail) use ($order) {
//            return $mail->order->id === $order->id;
//        });
//
//        // Assert a message was sent to the given users...
//        Mail::assertSent(OrderShipped::class, function ($mail) use ($user) {
//            return $mail->hasTo($user->email) &&
//                $mail->hasCc('...') &&
//                $mail->hasBcc('...');
//        });
    }
}
