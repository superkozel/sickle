<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('cluster.{cluster}', function ($user, \App\Models\Cluster $cluster) {
    return Gate::allows('view-cluster', $user, $cluster);
});

Broadcast::channel('user.{user}', function ($user, \App\User $channelUser) {
    return $channelUser->id == $user->id;
});

Broadcast::channel('location.{location}', function ($user, \App\Models\User\KnownLocation $location) {
    return true;
});