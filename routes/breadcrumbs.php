<?

// Home
Breadcrumbs::register('personal', function ($breadcrumbs) {
    $breadcrumbs->push('Home', action('Personal\IndexController@index'));
});

// Home > Rigs
Breadcrumbs::register('rig', function ($breadcrumbs) {
    $breadcrumbs->parent('personal');
    $breadcrumbs->push('Rigs', action('Personal\RigController@index'));
});

// Home > Configurations
Breadcrumbs::register('configuration', function ($breadcrumbs) {
    $breadcrumbs->parent('personal');
    $breadcrumbs->push('Configurations', action('Personal\ConfigurationController@index'));
});

// Home > Wallets
Breadcrumbs::register('wallet', function ($breadcrumbs) {
    $breadcrumbs->parent('personal');
    $breadcrumbs->push('Wallets', action('Personal\WalletController@index'));
});

// Home > Notifications
Breadcrumbs::register('notification', function ($breadcrumbs) {
    $breadcrumbs->parent('personal');
    $breadcrumbs->push('Notifications', action('Personal\NotificationController@index'));
});

// Home > Account
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('personal');
    $breadcrumbs->push('Account', action('Personal\AccountController@edit'));
});

// Home > Account > Edit
Breadcrumbs::register('account.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Edit profile', action('Personal\AccountController@edit'));
});

// Home > Account > Change password
Breadcrumbs::register('account.password', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Change password', action('Personal\AccountController@password'));
});

// Home > Account > Change password
Breadcrumbs::register('account.access', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Access', action('Personal\AccountController@access'));
});

// Home > Currency rates
Breadcrumbs::register('rates', function ($breadcrumbs) {
    $breadcrumbs->parent('personal');
    $breadcrumbs->push('Currency rates', action('Personal\RatesController@index'));
});