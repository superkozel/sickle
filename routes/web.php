<?php

Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Adldap\Laravel\Facades\Adldap;

if (Schema::hasTable(with(new \App\Models\Page)->getTable())) {
    foreach (\App\Models\Page::all() as $page) {
        Route::get($page->url, function() use ($page){
            View::share([
                'title' => $page->title,
                'h1' => $page->h1,
                'page' => $page
            ]);

            return $page->render();
        });
    }
}

Route::group([
    'prefix'     => 'personal',
    'namespace'  => 'Personal',
    'middleware' => ['auth', 'location']
], function () {
    Route::get('/', 'IndexController@index');
    \App\Http\Controllers\Personal\ClusterController::routes();
    \App\Http\Controllers\Personal\RigController::routes();
    \App\Http\Controllers\Personal\ConfigurationController::routes();
    \App\Http\Controllers\Personal\WalletController::routes();
    \App\Http\Controllers\Personal\AccountController::routes();
    \App\Http\Controllers\Personal\GpuController::routes();
    \App\Http\Controllers\Personal\NotificationController::routes();
    \App\Http\Controllers\Personal\RatesController::routes();
});

Auth::routes();
\App\Http\Controllers\Auth\LocationController::routes();