<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//request rig registration, must be accepted after in personal
Route::get('register_rig', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'email' => 'required|email|max:90',
        'certname' => 'required|string|min:' . \App\Models\Rig::HASH_LENGTH . '|max:' . \App\Models\Rig::HASH_LENGTH. '|unique:rig_registrations,hash|unique:rigs,ldap_id'
    ]);

    if ($validator->passes()) {
        \App\Models\RigRegistration::forceCreate([
            'email' => $request->email,
            'hash' => $request->certname
        ]);

        return Response::json(['success' => 1]);
    }
    else {
        return Response::json(['success' => 0, 'errors' => $validator->errors()]);
    }
});

//generate unique certname for rig
Route::get('get_certname', function(Request $request) {
    return \App\Services\RigService::generateCertname();
});