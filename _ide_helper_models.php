<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Cluster
 *
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Configuration[] $configurations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rig[] $rigs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wallet[] $wallets
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cluster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cluster whereName($value)
 */
	class Cluster extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Configuration
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rig[] $rigs
 * @property-read \App\Models\Wallet $secondWallet
 * @property-read \App\Models\Wallet $wallet
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $cluster_id
 * @property int $currency_id
 * @property int|null $second_currency_id
 * @property int $wallet_id
 * @property int|null $second_wallet_id
 * @property int|null $miner_id
 * @property string|null $epools
 * @property string|null $dpools
 * @property int|null $second_coin_intensity
 * @property int|null $config_override
 * @property string|null $checksum
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereChecksum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereConfigOverride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereDpools($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereEpools($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereMinerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereSecondCoinIntensity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereSecondCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereSecondWalletId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuration whereWalletId($value)
 */
	class Configuration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GpuModel
 *
 * @mixin \Eloquent
 */
	class GpuModel extends \Eloquent {}
}

namespace App\Models\Overclock{
/**
 * App\Models\Overclock\Profile
 *
 * @mixin \Eloquent
 */
	class Profile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ActiveRecord\Page
 *
 * @mixin \Eloquent
 * @property mixed $content
 */
	class Page extends \Eloquent {}
}

namespace App\Models\Payment{
/**
 * App\Models\Payment\Transaction
 *
 * @mixin \Eloquent
 */
	class Transaction extends \Eloquent {}
}

namespace App\Models\Rig\Asic\Bitfury{
/**
 * App\Models\Rig\Asic\Bitfury\B8
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class B8 extends \Eloquent {}
}

namespace App\Models\Rig\Asic\Bitmain{
/**
 * App\Models\Rig\Asic\Bitmain\D3
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class D3 extends \Eloquent {}
}

namespace App\Models\Rig\Asic\Bitmain{
/**
 * App\Models\Rig\Asic\Bitmain\L3
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class L3 extends \Eloquent {}
}

namespace App\Models\Rig\Asic\Bitmain{
/**
 * App\Models\Rig\Asic\Bitmain\S8
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class S8 extends \Eloquent {}
}

namespace App\Models\Rig\Asic\Bitmain{
/**
 * App\Models\Rig\Asic\Bitmain\S9
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class S9 extends \Eloquent {}
}

namespace App\Models\Rig\GpuRig{
/**
 * App\Models\Rig\GpuRig\Amd
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class Amd extends \Eloquent {}
}

namespace App\Models\Rig\GpuRig{
/**
 * App\Models\Rig\GpuRig\Nvidia
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class Nvidia extends \Eloquent {}
}

namespace App\Models\Rig{
/**
 * App\Models\Rig
 *
 * @mixin \Eloquent
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 * @property string $id
 * @property string $name
 * @property string|null $description
 * @property string|null $facts
 * @property int|null $configuration_id
 * @property int $cluster_id
 * @property float|null $hashrate
 * @property float|null $second_hashrate
 * @property float|null $temperature
 * @property int|null $gpu_count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereConfigurationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereFacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereGpuCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereHashrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereSecondHashrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereTemperature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 */
	class GpuRig extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Rig
 *
 * @property-read \App\Models\Cluster $cluster
 * @property-read \App\Models\Configuration $configuration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigMessage[] $messages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notMining()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig notUpToDate()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig offline()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rig online()
 */
	class Rig extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RigMessage
 *
 * @mixin \Eloquent
 */
	class RigMessage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RigRegistration
 *
 * @property string $certname
 * @property string $email
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RigRegistration new()
 */
	class RigRegistration extends \Eloquent {}
}

namespace App\Models\User{
/**
 * App\Models\User\KnownLocation
 *
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $ip
 * @property string $secret
 * @property \Carbon\Carbon|null $created_at
 * @property int $confirmed
 * @property string|null $email_token
 * @property \Carbon\Carbon|null $email_sent_at
 * @property int $email_sent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereEmailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereEmailSentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereEmailToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\KnownLocation whereUserId($value)
 */
	class KnownLocation extends \Eloquent {}
}

namespace App\Models\User{
/**
 * App\Models\User\SecurityLog
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $event_type_id
 * @property string $ip
 * @property string $session_id
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereEventTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SecurityLog whereUserId($value)
 */
	class SecurityLog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Wallet
 *
 * @mixin \Eloquent
 * @property-read \App\Models\Cluster $cluster
 * @property int $id
 * @property string $address
 * @property int $currency_id
 * @property int $cluster_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereClusterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wallet whereUpdatedAt($value)
 */
	class Wallet extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cluster[] $clusters
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\KnownLocation[] $locations
 * @property int $id
 * @property string|null $name
 * @property string $email
 * @property string|null $phone
 * @property string|null $telegram
 * @property int $ldap_id
 * @property string $password
 * @property float $balance
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLdapId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment\Transaction[] $paymentTransactions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RigRegistration[] $rigRegistrations
 */
	class User extends \Eloquent {}
}

