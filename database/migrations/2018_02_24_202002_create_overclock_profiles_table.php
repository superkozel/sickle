<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverclockProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overclock_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->unsignedSmallInteger('core_clock');
            $table->unsignedTinyInteger('core_state')->nullable();
            $table->unsignedTinyInteger('core_voltage')->nullable();
            $table->unsignedSmallInteger('memory_clock');
            $table->unsignedTinyInteger('memory_state')->nullable();
            $table->unsignedTinyInteger('fan_speed')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overclock_profiles');
    }
}
