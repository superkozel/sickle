<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');

            $table->tinyInteger('payment_type_id');
            $table->string('address', 255);
            $table->string('transaction_id', 255);

            $table->tinyInteger('amount');
            $table->integer('usd');
            $table->boolean('processed')->default(0);

            $table->timestamp('created_at');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
