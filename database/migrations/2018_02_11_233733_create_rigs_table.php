<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rigs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 50);
            $table->string('ldap_id', \App\Models\Rig::HASH_LENGTH);
            $table->string('hostname', 50);
            $table->string('ip', 50)->nullable();
            $table->string('name', 55)->nullable()->comment('Красивое название в интерфейсе');
            $table->string('description', 200)->nullable();
            $table->text('facts')->nullable();
            $table->unsignedInteger('configuration_id')->nullable();
            $table->unsignedInteger('cluster_id');
            $table->double('hashrate',null, 3)->nullable();
            $table->double('second_hashrate',null, 3)->nullable();
            $table->double('temperature',null, 2)->nullable();
            $table->tinyInteger('gpu_count')->nullable();
            $table->tinyInteger('missing_gpu_count')->nullable();
            $table->tinyInteger('red_temperature')->nullable();
            $table->string('additional_fields', 500)->nullable()->comment('Пользовательские поля');
            $table->string('configuration_checksum', 32)->nullable()->comment('Контрольная сумма для проверки актуальности конфига');

            $table->boolean('online')->default(0);

            $table->timestamps();
            $table->timestamp('reported_at')->nullable();
            $table->timestamp('online_at')->nullable();

            $table->index('cluster_id');
            $table->index('configuration_id');

            $table->foreign('configuration_id')
                ->references('id')->on('configurations')
                ->onDelete('set null');

            $table->foreign('cluster_id')
                ->references('id')->on('clusters')
                ->onDelete('cascade');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rigs');
    }
}
