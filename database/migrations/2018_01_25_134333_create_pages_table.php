<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('path');
            $table->string('h1');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('breadcrumb')->nullable();
            $table->string('title');
            $table->timestamps();
        });

        $data = [
            [1, 'Main page', 'index', '/', 'Innovative mining rig OS with web interface'],
            [2, 'Getting started', 'getting-started'],
        ];

        foreach ($data as $pageData) {
            \App\Models\Page::forceCreate([
                'id' => $pageData[0],
                'h1' => $pageData[1],
                'title' => array_get($pageData, 4, $pageData[1]),
                'url' => isset($pageData[3]) ? $pageData[3] : $pageData[2],
                'path' => $pageData[2],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
