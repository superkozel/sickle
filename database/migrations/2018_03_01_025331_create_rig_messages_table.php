<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRigMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rig_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type_id');
            $table->string('content', 250);
            $table->unsignedInteger('rig_id');

            $table->timestamp('created_at')->nullable();

            $table->foreign('rig_id')
                ->references('id')->on('rigs')
                ->onDelete('cascade');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rig_messages');
    }
}
