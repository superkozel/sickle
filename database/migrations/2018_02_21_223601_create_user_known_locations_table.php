<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserKnownLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_known_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('ip', 15);
            $table->string('user_agent', 150);
            $table->string('secret', 50);

            $table->timestamps();

            $table->boolean('confirmed')->default(0);
            $table->string('email_token', \App\Models\User\KnownLocation::TOKEN_LENGTH)->nullable();
            $table->timestamp('email_sent_at')->nullable();
            $table->boolean('email_sent')->default(0);

            $table->unique(['user_id', 'ip']);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_known_locations');
    }
}
