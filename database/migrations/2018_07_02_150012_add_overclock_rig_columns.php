<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOverclockRigColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rigs', function (Blueprint $table) {
            foreach (\App\Flow\Ldap\RigConfiguration::overclockOptions() as $ocOpt)
            {
                if (! Schema::hasColumn('rigs', $ocOpt))
                    $table->string($ocOpt)->nullable();
            }
        });

        \App\Models\Rig::select()->update([
            'fan_speed' => DB::raw("REPLACE(fan_speed, '-', ' ')")
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rigs', function (Blueprint $table) {
            foreach (\App\Flow\Ldap\RigConfiguration::overclockOptions() as $ocOpt)
            {
                if (Schema::hasColumn('rigs', $ocOpt))
                    $table->dropColumn($ocOpt);
            }
        });

        \App\Models\Rig::select()->update([
            'fan_speed' => DB::raw("REPLACE(fan_speed, ' ', '-')")
        ]);
    }
}
