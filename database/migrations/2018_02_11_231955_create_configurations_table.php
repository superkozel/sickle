<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 55);
            $table->unsignedInteger('cluster_id');
            $table->tinyInteger('currency_id');
            $table->tinyInteger('second_currency_id')->nullable();
            $table->unsignedInteger('wallet_id');
            $table->unsignedInteger('second_wallet_id')->nullable();
            $table->tinyInteger('miner_id')->nullable();
            $table->string('epools', 150)->nullable();
            $table->string('dpools', 150)->nullable();
            $table->tinyInteger('second_coin_intensity')->nullable();
            $table->tinyInteger('config_override')->nullable();
            $table->string('checksum', 32)->nullable();

            $table->timestamps();

            $table->index('cluster_id');
            $table->index('wallet_id');
            $table->index('second_wallet_id');

            $table->foreign('wallet_id')
                ->references('id')->on('wallets')
                ->onDelete('restrict');

            $table->foreign('second_wallet_id')
                ->references('id')->on('wallets')
                ->onDelete('restrict');

            $table->foreign('cluster_id')
                ->references('id')->on('clusters')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
