<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email', 90)->unique();
            $table->string('email_confirmed')->unique()->nullable();
            $table->string('email_confirmation_token', 10)->nullable();

            $table->string('phone_number')->unique()->nullable();
            $table->string('phone_number_confirmed')->unique()->nullable();
            $table->string('phone_confirmation_token', 5)->nullable();

            $table->string('telegram')->unique()->nullable();
            $table->string('telegram_user_id')->unique()->nullable();
            $table->integer('ldap_id')->unique();
            $table->string('password');
            $table->double('balance', null, 2)->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
