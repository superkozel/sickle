<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRigRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rig_registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rig_id')->nullable();
            $table->string('email', 90);
            $table->string('hash', \App\Models\Rig::HASH_LENGTH);
            $table->timestamps();

            $table->index('rig_id');

            $table->foreign('rig_id')
                ->references('id')->on('rigs')
                ->onDelete('cascade');
            ;

            $table->unique('hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rig_registrations');
    }
}
