<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address', 100);
            $table->unsignedTinyInteger('currency_id');
            $table->unsignedInteger('cluster_id');

            $table->timestamps();

            $table->unique(['cluster_id', 'address']);

            $table->foreign('cluster_id')
                ->references('id')->on('clusters')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
