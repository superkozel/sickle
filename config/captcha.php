<?php

return [
    'enabled' => env('APP_ENV') != 'local',
    'secret' => env('NOCAPTCHA_SECRET'),
    'sitekey' => env('NOCAPTCHA_SITEKEY'),
    'options' => [
        'timeout' => 2.0,
    ],
];
