<?php
return [
    'db' => [
        'url' => env('PUPPETDB_SERVER')
    ],
    'cert_server' => [
        'url' => env('PUPPET_CERT_SERVER')
    ],
];