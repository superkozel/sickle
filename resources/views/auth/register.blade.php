@extends('layouts.auth')

@section('h1')
    Register
@endsection

@section('content')
    <form class="form-horizontal panel" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="panel-body">

            {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                {{--<div class="col-md-6">--}}
                    {{--<div class="has-icon pull-left">--}}
                        {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                        {{--<i class="ico-user2 form-control-icon"></i>--}}
                    {{--</div>--}}

                    {{--@if ($errors->has('name'))--}}
                        {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                    {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                <div class="col-md-6">
                    <div class="has-icon pull-left">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="off">
                        <i class="ico-envelop form-control-icon"></i>
                    </div>

                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>

                <div class="col-md-6">
                    <div class="has-icon pull-left">
                        <input id="password" type="password" class="form-control" name="password" required autocomplete="off">
                        <i class="ico-key2 form-control-icon"></i>
                    </div>

                    @if ($errors->has('password'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                <div class="col-md-6">
                    <div class="has-icon pull-left">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        <i class="ico-asterisk form-control-icon"></i>
                    </div>
                </div>
            </div>

            @if (config('captcha.enabled'))
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {!! NoCaptcha::display() !!}

                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
            @endif
        </div>

        <div class="panel-footer">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                    Register
                </button>
            </div>
        </div>
    </form>

    <hr><!-- horizontal line -->

    <p class="text-center">
        <span class="text-muted">Already have an account? <a class="semibold" href="{{ action('Auth\LoginController@login') }}">Sign in here</a></span>
    </p>
@endsection
