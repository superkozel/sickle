@extends('layouts.auth')

@section('h1')
    Confirm new location
@endsection

@section('content')
    @push('scripts')
    <script>
        function redirectAfterConfirm() {
            var url = '{{ $intended }}';
            var auth = '{{ $auth }}';
            var isPost = {{ (int) $isPost }};

            if (isPost) {
                var form = $('<form action="' + url + '" method="post" style="display:none">' +
                    '<input type="text" name="auth" value="' + auth + '" />' +
                    '{{ csrf_field() }}' +
                    '</form>');
                $('body').append(form);
                form.submit();
            }
            else {
                window.location = url;
            }
        }

        @if ($location->confirmed)
            redirectAfterConfirm();
        @else
            Echo.private('location.{{ $location->id }}')
                .listen('.location.confirmed', function (e) {
                    redirectAfterConfirm();
                });
        @endif

    </script>
    @endpush
    <p>We have sent you email to confirm this authorization request</p>

    <p>Didn't receive email? <a href="{{ action('Auth\LocationController@resend', ['id' => $location->id]) }}">Resend message</a></p>
@endsection