@extends('layouts.auth')

@section('h1')
    Location confirmed
@endsection

@section('content')
    <p>New location confirmed {{ $location->ip }}</p>

    <p>You may now open browser windows in which login attempt was made</p>
@endsection