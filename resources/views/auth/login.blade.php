@extends('layouts.auth')

@section('h1')
    Login
@endsection

@section('content')
    <form class="form-horizontal panel" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="panel-body">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                <div class="col-md-6">

                    <div class="has-icon pull-left">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        <i class="ico-envelop form-control-icon"></i>
                    </div>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>

                <div class="col-md-6">

                    <div class="has-icon pull-left">
                        <input id="password" type="password" class="form-control" name="password" required>
                        <i class="ico-key2 form-control-icon"></i>
                    </div>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox custom-checkbox">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">&nbsp;&nbsp;Remember me</label>
                    </div>
                </div>
            </div>

            @if (config('captcha.enabled'))
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {!! NoCaptcha::display() !!}

                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
            @endif
        </div>

        <div class="panel-footer">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-success">
                    Login
                </button>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </div>
        </div>
    </form>

    <hr><!-- horizontal line -->

    <p class="text-center">
        <span class="text-muted">Dont have an account? <a class="semibold" href="{{ action('Auth\RegisterController@register') }}">Register here</a></span>
    </p>
@endsection
