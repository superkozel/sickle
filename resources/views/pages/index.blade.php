<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title', 'sickle') - mining ecosystem</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #000000;
                color: #636b6f;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: left;
                display: flex;
                justify-content: left;
                padding-left: 50px;
                padding-top: 30px;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .content-right {
                text-align: right;
            }

            .title {
                font-size: 65px;
                color: white;
                text-shadow: 0px 0px 3px black;
            }

            .links > a {
                padding: 0 25px;
                font-size: 18px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                color: #00ff00;
                font-family: 'Julius Sans One', sans-serif;
            }
/**/
            /*.content .links > a {*/
                /*color: white;*/
                /*text-shadow: 0px 0px 3px black;*/
            /*}*/

            /*.m-b-md {*/
                /*margin-bottom: 30px;*/
            /*}*/

            .bootstrap-sickle {
                padding-top: 25px;
                font-size: 25px;
                color: white;
                text-shadow: 0px 0px 3px black;
            }

            .bootstrap-sickle  a {
                text-decoration: none;
                color: #00ff00;
            }

            .ascii {
                color: #00ff00;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height" style="background-color: #000000">
            @if (Route::has('login'))
                <div style="height:55px;width:100%;top:0px;left:0px;position:absolute;">
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/personal') }}">Dashboard</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>
                            <a href="{{ route('register') }}">Register</a>
                        @endauth
                    </div>
                </div>
            @endif

            <div class="content">

<div>

		<pre class="ascii"> _______  ___   _______  ___   _  ___      _______
|       ||   | |       ||   | | ||   |    |       |
|  _____||   | |       ||   |_| ||   |    |    ___|
| |_____ |   | |       ||      _||   |    |   |___
|_____  ||   | |      _||     |_ |   |___ |    ___|
 _____| ||   | |     |_ |    _  ||       ||   |___
|_______||___| |_______||___| |_||_______||_______|
</pre>
    <div class="title">майнинговая экосистема для GPU / ASIC ригов, построенная на свободном программном обеспечении</div>

                </div>

                <div><img src="https://pp.userapi.com/c834300/v834300906/60da3/qHCCVyPeY64.jpg" width="300"></div>

                <div class="links content-right">
                    {{--<a href="#use">Мы используем</a>--}}
                    <a href="#bootstrap-sickle">Начало работы</a>
                    {{--<a href="{{ page_url(2) }}"></a>--}}
                </div>


                <div class="bootstrap-sickle">
                    <a name="bootstrap-sickle"></a>
                    <ul>1. <a href="{{ route('register') }}">Зарегистрируйтесь</a> в системе</ul>
                    <ul>2. Установите на ваш риг <a href="https://mirror.yandex.ru/debian-cd/9.4.0/amd64/iso-cd/debian-9.4.0-amd64-netinst.iso">Debian 9.4.0</a>
                        в минимальной комплектации или использовать <a href="">готовый образ предустановленной системы</a></ul>
                    <ul>3. От рута выполните команду <pre class="ascii">wget -qO - --no-check-certificate https://sickle.pro/bootstrap.pl | perl - your@email</pre>
                    введя ваш почтовый адрес
                    </ul>
                    <ul>4. Через веб-интерфейс добавьте ваш риг в систему</ul>
                    <ul>5. Разделяйте и властвуйте</ul>
                </div>
                </div>
            </div>
        </div>
    </body>
</html>
