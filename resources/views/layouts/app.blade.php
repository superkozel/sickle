@extends('layouts.base')

@php($cluster = $user->getCurrentCluster())
@php($notificationsCount = $user->unreadNotifications()->count())

@section('header')
    <script>
        function clearNotifications()
        {
            $.post('{{ action('Personal\NotificationController@clear') }}', {
                _token : '{{ csrf_token() }}'
            }, function(data){
                if(data.success) {
                    $('.notification-list').html(data.html);
                }
            })
        }
    </script>
    <!-- START navbar header -->
    <div class="navbar-header">
        <!-- Brand -->
        <a class="navbar-brand" href="{{ url('/') }}">
            <span class="logo-figure"></span>
            <span class="logo-text"></span>
        </a>
        <!--/ Brand -->
    </div>
    <!--/ END navbar header -->

    <!-- START Toolbar -->
    <div class="navbar-toolbar clearfix">
        <!-- START Left nav -->
        <ul class="nav navbar-nav navbar-left">
            <!-- Sidebar shrink -->
            <li class="hidden-xs hidden-sm">
                <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                <span class="meta">
                    <span class="icon"></span>
                </span>
                </a>
            </li>
            <!--/ Sidebar shrink -->

            <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
            <li class="navbar-main hidden-lg hidden-md hidden-sm">
                <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                <span class="meta">
                    <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                </span>
                </a>
            </li>
            <!--/ Offcanvas left -->

            @if ($user->clusters->count() > 0)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cluster <b>[{{ $cluster->getOwner()->email }}] <span class="caret"></span></b></a>
                    <ul class="dropdown-menu">
                        @foreach ($user->clusters as $curCluster)
                            <li {{ $curCluster->id == $cluster->id ? 'class="active"' : ''}}>
                                <a href="{{ action('Personal\ClusterController@select', $curCluster->id) }}">{{ $curCluster->getOwner()->email }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endif
        </ul>
        <!--/ END Left nav -->

        <!-- START navbar form -->
        <div class="navbar-form navbar-left dropdown" id="dropdown-form">
            <form action="" role="search">
                <div class="has-icon">
                    <input type="text" class="form-control" placeholder="Search application...">
                    <i class="ico-search form-control-icon"></i>
                </div>
            </form>
        </div>
        <!-- START navbar form -->

        <!-- START Right nav -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Notification dropdown -->
            <li class="dropdown custom" id="header-dd-notification" title="Notifications">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <span class="meta">
                        <span class="icon"><i class="ico-bell"></i></span>
                        @if ($notificationsCount > 0)
                            <span class="badge badge-danger">{{ $notificationsCount }}</span>
                        @endif
                    </span>
                </a>

                <!-- Dropdown menu -->
                <div class="dropdown-menu" role="menu">
                    <div class="dropdown-header">
                        <span class="title">Notification <span class="count"></span></span>
                        @if (($count = $user->notifications()->count()) > 0)
                            <span class="option text-right"><a href="{{ action('Personal\NotificationController@index') }}">See all ({{ $count }})</a></span>
                        @endif
                        <span class="option text-right"><a href="{{ action('Personal\AccountController@notifications') }}">Settings</a></span>
                    </div>
                    <div class="dropdown-body slimscroll notification-list">
                        @include('personal.notification.listing', ['notifications' => $user->notifications(), 'limit' => 10])
                    </div>
                </div>
                <!--/ Dropdown menu -->
            </li>
            <!--/ Notification dropdown -->

            <!-- Profile dropdown -->
            <li class="dropdown profile">
                <a class="dropdown-toggle" data-toggle="dropdown">
                <span class="meta">
                    <span class="avatar"><img src="/theme/lander/image/avatar/avatar.png" class="img-circle" alt="" /></span>
                    <span class="text hidden-xs hidden-sm pl5">{{ $user->email }}</span>
                </span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ action('Personal\AccountController@finance') }}"><span class="icon"><i class="ico-dollar"></i></span><?=number_format($user->balance, 2)?></a></li>
                    <li><a href="{{ action('Personal\AccountController@edit') }}"><i class="ico-cog4"></i></span> Account Settings</a></li>
                    <li><a href="{{ action('Personal\AccountController@security') }}"><i class="ico-shield3"></i></span> Security</a></li>
                    <li><a href="javascript:void(0);"><span class="icon"><i class="ico-question"></i></span> Help</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span class="icon"><i class="ico-exit"></i></span> Sign Out</a></li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
            <!-- Profile dropdown -->
        </ul>
        <!--/ END Right nav -->
    </div>
    <!--/ END Toolbar -->
@endsection


@section('sidebar')
    <!-- START Sidebar Content -->
    <section class="content slimscroll">
        <!-- START Template Navigation/Menu -->
        <ul class="topmenu topmenu-responsive" data-toggle="menu">
            <li {!! Request::url() == action('Personal\IndexController@index') ? 'class="active"' : '' !!}>
                <a href="{{ action('Personal\IndexController@index') }}" data-target="#dashboard" data-parent=".topmenu">
                    <span class="figure"><i class="ico-screen4"></i></span>
                    <span class="text">Monitor</span>
                </a>
            </li>
            <li {!! Request::url() == action('Personal\RigController@index') ? 'class="active"' : '' !!}>
                <a href="{{ action('Personal\RigController@index') }}" data-target="#dashboard" data-parent=".topmenu">
                    <span class="figure"><i class="ico-cabinet"></i></span>
                    <span class="text">Rigs</span>
                    <span class="number">
                        <span class="label label-success">{{ $cluster->rigs->count() }}</span>
                        @if ($cluster->rigs()->offline()->count() > 0)
                            <span class="label label-teal">{{ $cluster->rigs()->offline()->count() }}</span>
                        @endif
                        @if ($cluster->rigs()->notMining()->count() > 0)
                            <span class="label label-danger">{{ $cluster->rigs()->notMining()->count() }}</span>
                        @endif
                    </span>
                </a>
            </li>
            <li {!! Request::url() == action('Personal\ConfigurationController@index') ? 'class="active"' : '' !!}>
                <a href="{{ action('Personal\ConfigurationController@index') }}" data-target="#dashboard" data-parent=".topmenu">
                    <span class="figure"><i class="ico-settings"></i></span>
                    <span class="text">Configurations</span>
                </a>
            </li>
            <li {!! Request::url() == action('Personal\WalletController@index') ? 'class="active"' : '' !!}>
                <a href="{{ action('Personal\WalletController@index') }}" data-target="#dashboard" data-parent=".topmenu">
                    <span class="figure"><i class="ico-credit"></i></span>
                    <span class="text">Wallets</span>
                </a>
            </li>
            <li {!! Request::url() == action('Personal\NotificationController@index') ? 'class="active"' : '' !!}>
                <a href="{{ action('Personal\NotificationController@index') }}" data-target="#dashboard" data-parent=".topmenu">
                    <span class="figure"><i class="ico-bell3"></i></span>
                    <span class="text">Notifications</span>
                    @if ($notificationsCount > 0)
                        <span class="number"><span class="label label-danger">{{ $notificationsCount }}</span></span>
                    @endif
                </a>
            </li>
            <li {!! Request::url() == action('Personal\RatesController@index') ? 'class="active"' : '' !!}>
                <a href="{{ action('Personal\RatesController@index') }}" data-target="#dashboard" data-parent=".topmenu">
                    <span class="figure"><i class="ico-chart"></i></span>
                    <span class="text">Currency rates</span>
                </a>
            </li>

        </ul>
        <!--/ END Template Navigation/Menu -->
        <!-- START Sidebar summary -->
        <!-- Summary -->
        <h5 class="heading">Summary</h5>
        <div class="wrapper">
            <div class="table-layout">
                <div class="col-xs-5 valign-middle">
                    <span class="sidebar-sparklines" sparkType="bar" sparkBarColor="#00B1E1">{{ join(',', array_fill(0, 12, $cluster->rigs()->online()->count())) }}</span>
                </div>
                <div class="col-xs-7 valign-middle">
                    <h5 class="semibold nm">Rigs online
                        @if ($cluster->rigs()->offline()->count() > 0)
                            <span title="Rigs offline" class="label label-danger">{{ $cluster->rigs()->offline()->count() }}</span>
                        @endif
                    </h5>
                    <small class="semibold">{{ $cluster->getTotalPower() }} Watts</small>
                </div>
            </div>

            <div class="table-layout">
                <div class="col-xs-5 valign-middle">
                    <span title="Total GPU working" class="sidebar-sparklines" sparkType="bar" sparkBarColor="#91C854">2,5,3,6,4,2,4,7,8,9,7,6</span>
                </div>
                <div class="col-xs-7 valign-middle">
                    <h5 class="semibold nm">GPUs
                        @if ($cluster->getMissingGpuCount() > 0)
                            <span title="Missing GPU working" class="label label-danger">-{{ $cluster->getMissingGpuCount() }}</span>
                        @endif
                    </h5>
                    <small class="semibold">83.1%</small>
                </div>
            </div>

            @foreach ($cluster->getHashrates() as $code => $hashrate)
                <? $currency = \App\Models\Enums\Cryptocurrency::findByCode($code);?>
                <div class="table-layout">
                    <div class="col-xs-5 valign-middle align-right" style="text-align: right;padding-right:20px;font-size:24px;">
                        {{ cc($currency) }}
                    </div>
                    <div class="col-xs-7 valign-middle">
                        <h5 class="semibold nm">{{ $hashrate }} Mh/s</h5>
                        <small class="semibold">{{ $currency->getName() }}</small>
                    </div>
                </div>
            @endforeach
        </div>
        <!--/ Summary -->
        <!--/ END Sidebar summary -->
    </section>
    <!--/ END Sidebar Container -->
@endsection

@section('footer')
    <!-- START container-fluid -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <!-- copyright -->
                <p class="nm text-muted">&copy; Copyright 2015. All Rights Reserved.</p>
                <!--/ copyright -->
            </div>
            <div class="col-sm-6 text-right hidden-xs">
                <a target="_blank" href="javascript:void(0);" class="semibold">Privacy Policy</a>
                <span class="ml5 mr5">&#8226;</span>
                <a target="_blank" href="javascript:void(0);" class="semibold">Terms of Service</a>
                <span class="ml5 mr5">&#8226;</span>
                <a href="javascript:void(0);" class="semibold">Support</a>
                <span class="ml5 mr5">&#8226;</span>
                <a href="javascript:void(0);" class="semibold">FAQ</a>
            </div>
        </div>
    </div>
    <!--/ END container-fluid -->
@endsection

@section('main')
    <!-- START Template Container -->
    <div class="@yield('container', 'container-fluid')">
        @hasSection('h1')
            <!-- Page Header -->
            <div class="page-header page-header-block">
                <div class="page-header-section">
                    <h4 class="title semibold">@yield('h1', 'Header')</h4>
                </div>
                <div class="page-header-section">
                    <!-- Toolbar -->
                    <div class="toolbar">
                        {{ Breadcrumbs::render(isset($breadcrumbs) ? $breadcrumbs : 'personal') }}
                    </div>
                    <!--/ Toolbar -->
                </div>
            </div>
            <!-- Page Header -->
        @endif

        @yield('content')
    </div>
    <!--/ END Template Container -->

    <!-- START To Top Scroller -->
    <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%"><i class="ico-angle-up"></i></a>
    <!--/ END To Top Scroller -->
@endsection
