@if (count($breadcrumbs) > 1)

    <ol class="breadcrumb breadcrumb-transparent nm">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && (Request::url() != $breadcrumb->url))
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="active">{{ $breadcrumb->title }}</li>
            @endif

        @endforeach
    </ol>

@endif