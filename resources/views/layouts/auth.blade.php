@extends('layouts.base')

@section('main')
    <!-- START Template Container -->
    <section class="container">
        <!-- START row -->
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <!-- Brand -->
                <a href="/" class="text-center" style="margin-bottom:20px;display: block">
                    <span class="logo-figure inverse"></span>
                    <span class="logo-text inverse"></span>
                    <h5 class="semibold text-muted mt-5">@yield('h1', 'Auth')</h5>
                </a>
                <!--/ Brand -->

                @yield('content')
            </div>
        </div>
        <!--/ END row -->
    </section>
    <!--/ END Template Container -->

    <script>
        'use strict';

        (function (factory) {
            if (typeof define === 'function' && define.amd) {
                define([
                    'parsley'
                ], factory);
            } else {
                factory();
            }
        }(function () {

            $(function () {
                // Register form function
                // ================================
                var $form    = $('form[name=form-register]');

                // On button submit click
                $form.on('click', 'button[type=submit]', function (e) {
                    var $this = $(this);

                    // Run parsley validation
                    if ($form.parsley().validate()) {
                        // Disable submit button
                        $this.prop('disabled', true);

                        // start nprogress bar
                        NProgress.start();

                        // you can do the ajax request here
                        // this is for demo purpose only
                        setTimeout(function () {
                            // done nprogress bar
                            NProgress.done();

                            // redirect user
                            location.href = 'page-login.html';
                        }, 500);
                    } else {
                        // toggle animation
                        $form
                            .removeClass('animation animating shake')
                            .addClass('animation animating shake')
                            .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                                $(this).removeClass('animation animating shake');
                            });
                    }
                    // prevent default
                    e.preventDefault();
                });
            });

        }));
    </script>
@endsection
