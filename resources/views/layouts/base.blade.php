<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="backend">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>@yield('title', config('app.name'))</title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- START STYLESHEETS -->
    <!-- Plugins stylesheet : optional -->
    @stack('css')
    <!--/ Plugins stylesheet : optional -->

    <!-- Application stylesheet : mandatory -->
    <link rel="stylesheet" href="/theme/lander/stylesheet/bootstrap.css">
    <link rel="stylesheet" href="/theme/lander/stylesheet/layout.css">
    <link rel="stylesheet" href="/theme/lander/stylesheet/uielement.css">
    <!--/ Application stylesheet -->

    <!-- Theme stylesheet -->
    <link rel="stylesheet" href="/theme/lander/stylesheet/themes/theme.css">
    <!--/ Theme stylesheet -->

    <link href="{{ asset('/vendor/fontawesome/css/fontawesome-all.min.css') }}" rel="stylesheet"/>

    <link rel="stylesheet" href="/vendor/cryptocoins/cryptocoins.css">
    <link rel="stylesheet" href="/vendor/cryptocoins/cryptocoins-colors.css">

    <link rel="stylesheet" href="/css/ui.css">

    <!-- modernizr script -->
    <script type="text/javascript" src="/theme/lander/plugins/modernizr/js/modernizr.js"></script>
    <!--/ modernizr script -->

    <script src="/js/app.js"></script>

    <script>
        @if (! empty($cluster))
            var rigsData = {};
            @foreach ($cluster->rigs as $rig)
                rigsData['{{ $rig->id }}'] = {
                'hashrate': '{{ $rig->getHashrate() }}',
                'second_hashrate': '{{ $rig->getSecondHashrate() }}',
                'temperature': '{{ $rig->getHashrate() }}',
            };
            @endforeach

            function updateRig(rig)
            {
                rigsData[rig.id] = rig;

                $('.hashrate-' + rig.id).text(rig.hashrate);
                $('.hashrate2-' + rig.id).text(rig.second_hashrate);
                $('.temperature-' + rig.id ).text(rig.temperature);
                $('.online_at-' + rig.id ).text(rig.online_at);


                if (rig.facts.gpus) {
                    for (i in rig.facts.gpus) {
                        $('.gpu-temp-' + rig.id + '-' + i).text(rig.facts.gpus[i].temp);
                        $('.gpu-fan' + rig.id + '-' + i).text(rig.facts.gpus[i].fan);
                    }
                }

                updateTotals();
            }

            function updateTotals()
            {

            }

            Echo.private('cluster.{{ $cluster->id }}')
                .listen('RigFactsUpdated', function (e) {
                    console.log(e);
                    updateRig(e.rig);
            });
        @endif
        // define a handler
        function doc_keyUp(e) {
            // this would test for whichever key is 40 and the ctrl key at the same time
            if (e.ctrlKey && e.keyCode == 77) {
                // call your function to do the thing
                $('body').prepend('<div style="width:100%;height:100%;background-size:cover;position:absolute;top:0px;left:0px;background-color:white;background: url(/i/bg.jpg);background-size: 100%;z-index:6666;"></div>')
            }
        }
        // register the handler
        document.addEventListener('keyup', doc_keyUp, false);
    </script>

@if (config('captcha.enabled'))
    {!! NoCaptcha::renderJs('en') !!}
@endif

<!-- Latest compiled and minified JavaScript -->
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--}}
    {{--<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>--}}
    {{--<script>--}}
    {{--$.fn.editable.defaults.mode = 'inline';--}}
    {{--</script>--}}

    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>--}}
</head>
<!-- START Body -->
<body>

@hasSection('header')
    <header id="header" class="navbar">
        @yield('header')
    </header>
@endif

@hasSection('sidebar')
    <aside class="sidebar sidebar-left sidebar-menu">
        @yield('sidebar')
    </aside>
@endif

@hasSection('footer')
    <footer id="footer">
        @yield('footer')
    </footer>
@endif

@hasSection('main')
    <section id="main" role="main">
        @yield('main')
    </section>
@endif

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- Application and vendor script : mandatory -->
<script type="text/javascript" src="/theme/lander/javascript/vendor.js"></script>
<script type="text/javascript" src="/theme/lander/javascript/core.js"></script>
<script type="text/javascript" src="/theme/lander/javascript/backend/app.js"></script>
<script type="text/javascript" src="/vendor/favico.min.js"></script>
<script>
    var favicon=new Favico({
        bgColor : '#5CB85C',
        textColor : '#ff0',
        // fontFamily : 'FontAwesome',
        // elementId : 'badgeFont'
    });
    favicon.badge('Ok');
</script>

<!--/ Application and vendor script : mandatory -->

<!-- Plugins and page level script : optional -->
<script type="text/javascript" src="/theme/lander/javascript/pace.min.js"></script>

@stack('scripts')
<!--/ Plugins and page level script : optional -->

<!--/ END JAVASCRIPT SECTION -->
</body>
<!--/ END Body -->
</html>