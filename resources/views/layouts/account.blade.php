@extends('layouts.app')

@section('content')
    <div class="row">
        <!-- Left / Top Side -->
        <div class="col-lg-3">
            <!-- tab menu -->
            <ul class="list-group list-group-tabs">
                @foreach ($tabs as $url => list($title, $icon))
                    <li class="list-group-item{!! $_SERVER['REQUEST_URI'] == $url ? ' active' : '' !!}">
                         <a href="{{ $url }}"><i class="{{ $icon }} mr5"></i> {{ $title }}</a>
                    </li>
                @endforeach
            </ul>
            <!-- tab menu -->

            <hr><!-- horizontal line -->

            @php
            $daysLeft = \App\Models\Pricing::getDaysLeft($user);
            $pricePerMonth = \App\Models\Pricing::calculateMonthlyPaymentForUser($user);
            $percent = max(100, ($daysLeft / 30 * 100));
            @endphp
            <!-- figure with progress -->
            <ul class="list-table">
                <li style="width:70px;">
                    <img class="img-circle img-bordered" src="../image/avatar/avatar7.jpg" alt="" width="65px">
                </li>
                <li class="text-left">
                    <h5 class="semibold ellipsis mt0">$ {{ number_format($user->balance, 2) }}</h5>
                    <div style="max-width:200px;">
                        <div class="progress progress-xs mb5">
                            <div class="progress-bar progress-bar-{{ $percent > 60 ? 'success' : ($percent > 20 ?  'warning' : 'danger' ) }}" style="width:{{ max(100, ($daysLeft / 30 * 100)) }}%"></div>
                        </div>
                        <p class="text-muted clearfix nm">
                            @if ($pricePerMonth == 0)
                                <span class="pull-left">Free</span>
                                <span class="pull-right">Inf. days left</span>
                            @else
                                <span class="pull-left">You are doing fine!</span>
                                <span class="pull-right">{{ $daysLeft }} {{ str_plural('day', $daysLeft) }}</span>
                            @endif
                        </p>
                    </div>
                </li>
            </ul>
            <!--/ figure with progress -->

            <hr><!-- horizontal line -->

            <!-- follower stats -->
            <ul class="nav nav-section nav-justified mt15">
                <li>
                    <div class="section">
                        <h4 class="nm semibold">{{ $user->getOwnCluster()->rigs()->count() }}</h4>
                        <p class="nm text-muted">Active {{ str_plural('rig', $user->getOwnCluster()->rigs()->count()) }}</p>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <h4 class="nm semibold">$3</h4>
                        <p class="nm text-muted">Price per rig</p>
                    </div>
                </li>
                <li>
                    <div class="section">
                        <h4 class="nm semibold">${{ $pricePerMonth }}</h4>
                        <p class="nm text-muted">Per month</p>
                    </div>
                </li>
            </ul>
            <!--/ follower stats -->
        </div>
        <!--/ Left / Top Side -->

        <!-- Left / Bottom Side -->
        <div class="col-lg-9">
            <!-- START Tab-content -->
            <div class="tab-content">
                <!-- tab-pane: profile -->
                <div class="tab-pane active" id="profile">
                    @yield('content2')
                </div>
                <!--/ tab-pane: profile -->
            </div>
            <!--/ END Tab-content -->
        </div>
        <!--/ Left / Bottom Side -->
    </div>
@endsection