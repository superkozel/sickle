@extends('layouts.app')

@section('h1')
    Rigs
@endsection

@push('scripts')
    <script type="text/javascript" src="/theme/lander/plugins/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/theme/lander/plugins/datatables/tabletools/js/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/theme/lander/plugins/datatables/js/datatables-bs3.js"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="/theme/lander/plugins/datatables/css/datatables.css">
    <link rel="stylesheet" href="/theme/lander/plugins/datatables/css/tabletools.css">
@endpush

@section('content')
    <script>
        $().ready(function() {
            $('#rig-table').DataTable({
                'paging' : false,
                'info' : false
            });
        });
    </script>
    <style>
        .cluster-name i{display: none; font-size:12px;}
        .cluster-name:hover i{display: inline-block}

        #rig-table_filter{position: absolute;right:15px;margin-top:-25px;}
    </style>
    <div class="row">
        <div class="col-md-12">
            <table id="rig-table" class="table table-bordered" id="table-rigs">
                <thead>
                <tr>
                    <th width="10" nowrap=""></th>
                    <th class="sorting sorting-asc">Name</th>
                    <th width="55" nowrap="">Type</th>
                    <th>Hashrate</th>
                    <th width="55" nowrap="">GPUs</th>
                    <th style="min-width: 100px" nowrap="">Description</th>
                    <th>Configuration</th>
                    <th>Wallet</th>
                    <th>Miner</th>
                    <th width="14"></th>
                    <th width="14"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($user->clusters as $i => $cluster)
                    @if ($i > 0)
                        <tr class="group suc">
                            <th colspan="9" style="text-align: center">{{ $cluster->getOwner()->email }}</th>
                        </tr>
                    @endif
                    @foreach ($cluster->rigs as $rig)
                        <tr id="{{ $rig->id }}" class="table-danger">
                            <td><i class="fa fa-circle" style="color:{{ ($rig->isOnline()) ? '#3bb33b' : 'red' }};"></i></td>
                            <td>
                                <a href="{{ action('Personal\RigController@show', ['rig' => $rig]) }}">
                                    <span class="rig-name">{{ $rig->getDisplayName() }}</span>
                                </a>
                                <div class="text-nowrap">
                                    <a class="small text-muted">{{ short_hash($rig->ldap_id) }}</a>
                                </div>
                            </td>
                            <td style="width:30px;">
                                @include('personal._parts.columns.rig_type', ['rig' => $rig])
                            </td>
                            <td>
                                @include('personal._parts.columns.rig_hashrate', ['rig' => $rig])
                            </td>
                            <td>
                                <span class="label label-primary">{{ $rig->gpu_count }}</span>&nbsp

                                @if ($rig->missing_gpu_count)
                                    <span class="label label-danger">-<?=$rig->missing_gpu_count?></span>
                                @endif
                            </td>
                            <td data-order="">
                                <span class="description">{{ $rig->description ? short_text($rig->description, 50) : '---' }}</span>
                            </td>
                            <td>
                                <span class="text-success-darker">
                                    @if ($rig->configuration)
                                        <a style="display: block;" href="{{ $rig->configuration->getUrl() }}">{{ $rig->configuration->name }}</a>
                                        @if ($rig->isUpToDate())
                                            <span class="text-muted">Up&nbsp;to&nbsp;date</span>
                                        @else
                                            {{ BootForm::open(['action' => ['Personal\RigController@burn', $rig], 'method' => 'POST']) }}
                                            <button class="btn btn-xs btn-danger" style="width:100%;">Burn new config</button>
                                            {{ BootForm::close() }}
                                        @endif
                                    @else
                                        ---
                                    @endif
                                </span>
                            </td>
                            <td>
                                @if ($rig->configuration && ($wallet = $rig->configuration->wallet))
                                    <i title="{{ $wallet->getCurrency()->getName() }}" class="cc {{ mb_strtoupper($wallet->getCurrency()->getCode()) }}"></i>&nbsp;{{ short_hash($wallet->address) }}</a>

                                    @if ($secondWallet = $rig->configuration->secondWallet)
                                        <br/><i title="{{ $secondWallet->getCurrency()->getName() }}" class="cc {{ mb_strtoupper($secondWallet->getCurrency()->getCode()) }}"></i>&nbsp;{{ short_hash($secondWallet->address) }}</a>
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if ($miner = $rig->getMiner())
                                    <span class="text-info">{{ $miner->getName() }}</span>
                                @endif
                            </td>
                            <td style="text-align: center;width:14px;">
                                <a href="{{ $rig->getEditUrl() }}"><i class="fa fa-pencil-alt" aria-hidden="true" title="Edit"></i></a>
                            </td>
                            <td style="text-align: center;width:14px;">
                                <form style="display: inline;" method="post" action="<?=$rig->getDeleteUrl()?>">
                                    {{ csrf_field() }}
                                    <a title="Delete rig" style="margin-left:0px;" onclick="if (confirm('Delete rig')) $(event.currentTarget).closest('form').submit()"><i class="fa fa-times"></i></a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-default" href="{{ action('Personal\RigController@add', ['cluster' => $cluster]) }}">Add rig</a>
        </div>
    </div>
@endsection
