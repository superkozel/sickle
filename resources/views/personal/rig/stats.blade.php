@extends('layouts.app')

@section('h1')
    Rig stats
@endsection

@push('scripts')
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js') }}"></script>
@endpush

@section('content')
    <div class="panel panel-default">
        <!-- panel heading/header -->
        <div class="panel-heading">
            <div class="panel-title">Line chart</div>
            <!-- panel toolbar -->
            <div class="panel-toolbar text-right">
                <!-- option -->
                <div class="option">
                    <button class="btn" data-toggle="panelrefresh"><i class="reload"></i></button>
                    <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                    <button class="btn" data-toggle="panelremove" data-parent=".col-md-4"><i class="remove"></i></button>
                </div>
                <!--/ option -->
            </div>
            <!--/ panel toolbar -->
        </div>
        <!--/ panel heading/header -->
        <!-- panel body with collapse capabale -->
        <div class="panel-collapse pull out">
            <div class="panel-body">
                <!-- Loading indicator -->
                <div class="indicator show"><span class="spinner"></span></div>
                <!--/ Loading indicator -->
                <div class="chart mt10" id="chart-line" style="height:250px;"></div>
            </div>
        </div>
        <!--/ panel body with collapse capabale -->
    </div>
    <script>
        var chart  = new flotDemo('#chart-line-spline', 'api/flot.php?type=linespline'),
            option = {
                series: {
                    lines: { show: false },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 2,
                        fill: 0
                    },
                    points: {
                        show: true,
                        radius: 4
                    }
                },
                grid: {
                    borderColor: '#eee',
                    borderWidth: 1,
                    hoverable: true,
                    backgroundColor: '#fcfcfc'
                },
                tooltip: true,
                tooltipOpts: {
                    content: '%x : %y'
                },
                xaxis: {
                    tickColor: '#eee',
                    mode: 'categories'
                },
                yaxis: {
                    tickColor: '#eee'
                },
                shadowSize: 0
            };

        // Load chart data
        chart.remoteData(option);

        // Reload chart data on panel refresh
        $('html').on('fa.panelrefresh.refresh', function (event, options) {
            if(options.element.find('#chart-line-spline').length !== 0) {
                chart.remoteData(option);
            }
        });
    </script>

    <div class="dropdown show">
        <h1 class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
            <?=$rig->name?>
        </h1>
        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 30px, 0px); top: 0px; left: 0px; will-change: transform;">
            <a class="dropdown-item " href="/a/stat/">Account Stats</a>
            <div class="dropdown-divider"></div>
            <? foreach ($cluster->rigs as $otherRig):?>
                <a class="dropdown-item active" href="{{ action('Personal\RigController@stats', ['rig' => $otherRig]) }}">{{ $otherRig->name }}</a>
            <? endforeach;?>
        </div>
    </div>
    <h3 class="text-muted"><?=$rig->id?></h3>

    <h3>Temperature</h3>

    <canvas id="myChart" width="400" height="400"></canvas>
    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endsection