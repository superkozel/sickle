<?php
/** @var \App\Models\Rig $rig */
/** @var \App\Models\Gpu $gpus */
$gpus = $rig->getGpus();
/** @var \App\Models\Configuration $configuration */
$configuration = $rig->configuration;
?>
@extends('layouts.app')

@section('h1')
    <div class="dropdown" style="display: inline-block;">
        <div class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
            {{ $rig->getDisplayName() }}
        </div>
        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 30px, 0px); top: 0px; left: 0px; will-change: transform;">
            <a class="dropdown-item " href="">Rig stats</a>
            <div class="dropdown-divider"></div>
            <? foreach ($rig->cluster->rigs as $otherRig):?>
            <a class="dropdown-item" href="{{ action('Personal\RigController@stats', ['rig' => $otherRig]) }}">{{ $otherRig->name }}</a>
            <? endforeach;?>
        </div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div style="float:left;margin-bottom:6px;">
            @if ($configuration && $configuration->getCurrency())
                <span class="label label-info" style="display: inline-block;font-size:17px;height:25px;"><span class="hashrate-{{ $rig->id }}">{{ $rig->calculateHashrate($configuration->getCurrency()) }}</span> mh/s</span>
            @endif
            <span style="display: inline-block;font-size:17px;margin-top:-8px;height:25px;" class="label label-success"><span class="temperature-{{ $rig->id }}">{{ $rig->getTemperature() }}</span>&deg;C</span>
            <span class="label label-info" style="display: inline-block;font-size:17px;height:25px;">{{ $rig->getPower() }} W</span>
            <a class="label" style="display: inline-block;font-size:17px" href="{{ action('Personal\RigController@stats', ['rig' => $rig]) }}"><i class="fa fa-chart-bar"></i></a>
        </div>

        {{--@foreach($gpus as $gpu)--}}
            {{--<div class="col-xs-2" style="padding:20px;">--}}
                {{--{{ $gpu->getTemperature() }}&deg;C {{ $gpu->getFanSpeed() }}%<br/>--}}
                {{--{{ $gpu->getHashrate() }}--}}
            {{--</div>--}}
        {{--@endforeach--}}
    </div>
    <div class="col-xs-6">
        <table class="table table-condensed">
            @foreach($rig->getGpus() as $i => $gpu)
                <tr>
                    <td style="width:10px;">
                        @if ($gpu->isFine())
                            <i class="fa fa-circle" style="color:#3bb33b"></i>
                        @else
                            <i class="fa fa-warning" style="color:red"></i>
                        @endif
                    </td>
                    <td style="width:20px;">
                        <div class="text-muted">GPU{{ $i }} </div>
                        {{ $gpu->getBusId() }}
                    </td>
                    <td>
                        <b>{{ $gpu->getName() }}</b>
                        <div>{{ $gpu->getPower() }} W</div>
                    </td>
                    <td style="width:20px;">
                        <div class="label label-success"><span class="gpu-temperature-{{ $rig->id }}-{{ $i }}">{{ $gpu->getTemperature() }}</span> &deg; / {{ $rig->getMaxTemperature() }} &deg;C</div>
                        <span class="label label-primary gpu-fan-{{ $rig->id }}-{{ $i }}">{{ $gpu->getFanSpeed() }}%</span>
                    </td>
                    <td style="width:20px;">
                        @if ($configuration)
                            <span class="label label-info"><i class="cc {{ mb_strtoupper($configuration->getCurrency()->getCode()) }}"></i>&nbsp;<span class="gpu-hashrate-{{ $rig->id }}-{{ $i }}">{{ $gpu->getHashrate() }}</span>  mh/s</span>
                            @if ($configuration->isDual())
                                <span class="label label-info"><i class="cc {{ mb_strtoupper($configuration->getSecondCurrency()->getCode()) }}"></i>&nbsp;<span class="gpu-hashrate2-{{ $rig->id }}-{{ $i }}">{{ $gpu->getSecondHashrate($configuration->getSecondCurrency()) }}</span>  mh/s</span>
                            @endif
                        @endif
                    </td>
                    <td style="width:10px;">
                        <a href="{{ $gpu->getUrl() }} "><i class="fa fa-chart-bar"></i></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="col-xs-5">
        <div class="tab-rig-description">
            <span class="label {{  ! $rig->isOnline() ? 'label-danger' : 'label-success' }}">{{  ! $rig->isOnline() ? 'offline' : 'online' }}</span>
            <h4>Description</h4>
            {{ $rig->description ? $rig->description : '---' }}<br/>
            <a href="{{ $rig->getEditUrl() }}" class="btn btn-sm btn-default">Edit description</a>

            @if ($configuration)
                <h4>Configuration "<b>{{ $configuration->name }}"</b></h4>

                Miner: {{ $configuration->getMiner() ? $configuration->getMiner()->getName() : 'None'}} <br/>
                Currency: {{ $configuration->getCurrency() ? $configuration->getCurrency()->getName() : 'None' }} <br/>
                @if ($configuration->isDual())
                    Second currency: {{ $configuration->getSecondCurrency()->getName() }} <br/>
                @endif

                <a href="{{ $configuration->getEditUrl() }}" class="btn btn-sm btn-default">Edit configuration</a>
                <a href="{{ $rig->getEditUrl() }}" class="btn btn-sm btn-default">Select another configuration</a>
            @else
                <a href="{{ $rig->getEditUrl() }}" class="btn btn-sm btn-default">Select configuration</a>
            @endif

            <h4>System info</h4>
            IP address: {{ $rig->getIpAddress() }} <br/>
            Boot time: {{ $rig->getLastBootTime() }} <br/>
            <br/>
            OS: {{ $rig->getFact('operatingsystem') }}  {{ $rig->getFact('operatingsystemrelease') }} <br/>
            Free space: {{ $rig->getFact('disks.sda.size') }} <br/>

            {{ BootForm::open(['method' => 'POST']) }}
            <button formaction="{{ action('Personal\RigController@reboot', $rig) }}"  href="" class="btn btn-danger btn-sm">
                <i class="fa fa-sync"></i> Reboot
            </button>

            <button formaction="{{ action('Personal\RigController@shutdown', $rig) }}" href="" class="btn btn-danger btn-sm">
                <i class="fa fa-power-off"></i> Shutdown
            </button>
            {{ BootForm::close() }}
        </div>
        <div class="tab-overclock">

        </div>

    </div>
</div>
@endsection