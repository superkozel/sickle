@extends('layouts.app')

@section('h1')
    Add Rig
@endsection

@section('content')
    @if ($registration)
        @include('personal.rig.form', array('hash' => $registration->hash, 'rig' => $rig, 'cluster' => $cluster))
    @else
        <h2>Select rig</h2>

        @if (($registrations = \App\Models\RigRegistration::where('email', $user->email)->new()->get())->count() > 0)
            <h4>Unregistered rigs</h4>

            <table class="table" style="width:auto;">
                <thead>
                <th>Hash</th>
                <th>Date</th>
                <th></th>
                </thead>
                <tbody>
                @foreach ($registrations as $registration)
                    <tr>
                        <td>{{ $registration->hash }}</td>
                        <td>{{ $registration->created_at }}</td>
                        <td>
                            <a href="{{ action('Personal\RigController@add', ['cluster' => $cluster, 'certname' => $registration->hash]) }}" class="btn btn-default btn-sm">Add</a>
                            <a class="btn btn-default btn-sm">Remove</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            You don't have any registered rigs in our system, please follow OS installation steps
        @endif
    @endif
@endsection
