<?
/** @var \App\Models\Gpu $gpu */
?>
@extends('layouts.app')

@section
    {{ $gpu->getName() }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {{ $gpu->getTemperature() }} &deg; C
            {{ $gpu->getHashRate() }} mh/s
        </div>
    </div>
@endsection