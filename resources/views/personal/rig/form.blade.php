<?php
/** @var \App\Models\Rig $rig */

if ($rig->id)
    $subForm = view('personal.configuration.form', ['configuration' => new \App\Models\Configuration(), 'cluster' => $cluster, 'subform' => 'Configuration'])->render();
?>
{{ BootForm::open(['model' => $rig, 'method' => 'POST']) }}
    {!! BootForm::hidden('cluster_id') !!}
    @if (! $rig->id)
        {!! BootForm::text('certname', 'Hash ID *', Request::get('certname'), ['maxlength' => \App\Models\Rig::HASH_LENGTH, 'required' => true, 'placeholder' => 'This ID is provided to you after successful installation of OS on your rig']) !!}
    @endif

    {!! BootForm::select('type', 'Rig type', ($rig->type ? [] : ['' => '---']) + $rig::getOptionsTypes()) !!}

    {!! BootForm::text('hostname', 'Hostname *', null, ['placeholder' => 'Name of your rig']) !!}
{{--    {!! BootForm::text('name', 'Name *', null, ['placeholder' => 'Name of your rig']) !!}--}}
    {!! BootForm::textarea('description', 'Description', null, ['placeholder' => 'Any description comfortable to you'])!!}

    @if ($rig->id)
       <h3>Config</h3>

       {!! BootForm::select('configuration_id', 'Select configuration', ['' => '--'] + $cluster->configurations()->pluck('name', 'id')->all() + [0 => '+ New configuration']) !!}
       <div class="configurations-group" @if($rig->configuration_id !== 0) style="display: none" @endif>
           {!! $subForm !!}
       </div>

        @if ($rig->isGpuRig())
            <?php
                $options = '';
                $disabled = \App\Flow\Puppet\Module\FanControl::disabled();
                foreach (\App\Flow\Puppet\Module\FanControl::getOptions() as $k => $v) {
                    $options .= '<option value="' . $k . '" ' . (in_array($k, $disabled) ? 'disabled="disabled"' : '') . ' ' . ($k == $rig->fan_mode ? 'selected="selected"' : '') . '>' . $v . '</option>';
                }
                $select = '<select class="form-control" id="fan_mode" name="fan_mode">' . $options . '</select>';
            ?>
            {!! BootForm::getFormGroup('fan_mode', 'Fan mode', $select)  !!}

            <div class="b-fan-speed" @if($rig->fan_mode != \App\Flow\Puppet\Module\FanControl::MODE_MANUAL) style="display: none" @endif>
                {!! BootForm::text('fan_speed', 'Fan speed') !!}
            </div>

            <h3>Overclock</h3>


            <div class="b-overclock-nvidia" @if(! $rig->isNvidia()) style="display: none" @endif>
                {!! BootForm::text('nv_graphicsclockoffset', 'Graphics clock offset (Mhz)') !!}

                <div class="alert alert-info" style="">
                    Usually this value is double from what you see in AfterBurner.
                    E.g. if in Windows it's +800Mhz, here you should write 1600.
                </div>
                {!! BootForm::text('nv_memorytransferrateoffset', 'Memory Transfer rate offset (Mhz)') !!}
                {!! BootForm::text('nv_powerlimit', 'Power limit (W)') !!}
            </div>

            <div class="b-overclock-amd" @if(! $rig->isAmd()) style="display: none" @endif>

                <div class="alert alert-info" style="">
                    Set core clock here, a good value would be around <code>1100</code>.
                    If "Core State" is unset, then the default <code>5</code> state will be used to set the clock.
                </div>
                {!! BootForm::text('amd_core_clock', 'Core clock (mhZ)') !!}

                <div class="alert alert-info" style="">
                    <p>
                        This is required if you try to undervolt you card.
                        You should use this parameter with "Core Clock" setting and "Core Voltage".
                    </p>
                    <p class="mb-0">
                        <b>DPM (Dynamic Power Management)</b> or "Power Level" of GPU core.
                        For RX cards it's a value from <code>1</code> to <code>7</code>. Default is <code>5</code>.
                        Lower the value to downvolt.
                    </p>
                </div>
                {!! BootForm::text('amd_core_state', 'Core state (Index)') !!}

                <div class="alert alert-info" style="">
                    <p>
                        This is required if you try to undervolt you card. And you MUST set "Core State" or default <code>5</code> state will be used to set the voltage.
                    </p>
                    <p class="mb-0">
                        You can set values like <code>900</code> meaning mV or values like <code>65284</code> from VBIOS table.
                    </p>
                </div>
                {!! BootForm::text('amd_core_voltage', 'Core voltage (mV)') !!}

                {!! BootForm::text('amd_voltage_state', 'Voltage state (Index)') !!}

                {!! BootForm::text('amd_memory_clock', 'Memory clock (Mhz)') !!}

                <div class="alert alert-info" style="">
                    <p>
                        This is a very advanced parameter.
                        Try this if there are problems with undervolting. If it works - don't touch this.
                    </p>
                    <p>
                        RX cards known to have 1 or 2 memory states (actually 3 including 0 idle state).
                        By default the highest state will be selected.
                        But some rare cards are known to fail to undervolt on the highest state and required to select a lower one.
                        For example the highest is 2 and for undervolting you would want to set state 1.
                    </p>
                    <p class="mb-0">
                        You should use this parameter with "Mem Clock" setting.
                    </p>
                </div>
                {!! BootForm::text('amd_mem_state', 'Mem state (Index)') !!}
            </div>
       @endif
    @else
       @if (config('captcha.enabled'))
           <div class="form-group">
               <div class="col-md-12">
                   {!! NoCaptcha::display() !!}

                    @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        @endif
    @endif

    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="history.back()">Cancel</button>
        <button type="submit" class="btn btn-success">@if ($rig->id) Save rig @else Add rig @endif</button>
    </div>
{{ BootForm::close() }}
<script>
    $('[name=fan_mode]').change(function(){
        if ($(this).val() == '{{ \App\Flow\Puppet\Module\FanControl::MODE_MANUAL }}') {
            $('.b-fan-speed').show();
        }
        else {
            $('.b-fan-speed').hide();
        }
    });
    $('[name=type]').change(function(){
        if ($(this).val() == '{{ \App\Models\Rig::TYPE_AMD }}') {
            $('.b-overclock-amd').show();
            $('.b-overclock-nvidia').hide();
        }
        if ($(this).val() == '{{ \App\Models\Rig::TYPE_NVIDIA }}') {
            $('.b-overclock-amd').hide();
            $('.b-overclock-nvidia').show();
        }
    });
    $('[name=id]').change(function(){
        var value = $(this).val();
        if (value.length == '{{ \App\Models\Rig::HASH_LENGTH }}') {
            $.ajax({
                url: '{{ action('Personal\RigController@suggestName') }}',
                dataType: 'json',
                method: 'GET',
                data: {
                    'hash': value
                }
            }).success(function(response){
                if (response.success && emptyName) {
                    $(['name']).val(response.name);
                }
            });
        }
    });
    $('[name=configuration_id]').change(function(){
        val = $(this).val();
        if (val === '0') {
            $('.configurations-group').show();
        }
        else {
            $('.configurations-group').hide();
        }
    });
</script>