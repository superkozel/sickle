@extends('layouts.app')

@section('h1')
    Edit Rig
@endsection

@section('content')
    @include('personal.rig.form', array('rig' => $rig, 'cluster' => $cluster))
@endsection