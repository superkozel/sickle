@extends('layouts.app')

@section('h1')
    Delete wallet #{{ $wallet->id }}
@endsection

@section('content')
    <div class="alert alert-warning">
        This wallet [{{ $wallet->address }}] being used in rig configs, please choose new address.
    </div>

    {!! BootForm::open(['method' => 'POST', 'model' => $wallet, 'action' => ['Personal\WalletController@delete', $wallet]]) !!}
        {{ BootForm::hidden('id') }}

        @if ($requiredNewWallet)
            {!! BootForm::text('new_wallet_address', 'New wallet address', null) !!}
            <script>
                $('[name=new_wallet_id]').typeahead({
                    source: {{ json_encode($otherWallets->pluck('name', 'id')) }}
                });
            </script>
        @endif

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="history.back()">Cancel</button>
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    {!! BootForm::close() !!}
@endsection