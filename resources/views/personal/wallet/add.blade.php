@extends('layouts.app')

@section('h1')
    Add wallet
@endsection

@section('content')
    @include('personal.wallet.form', array('wallet' => $wallet, 'cluster' => $cluster))
@endsection