{!! BootForm::open(['model' => $wallet, 'action' => $wallet->id ? ['Personal\WalletController@update', $wallet] : 'Personal\WalletController@store']) !!}
    @if ($cluster)
        {!! BootForm::hidden('cluster_id', $cluster->id) !!}
    @endif
    @if (! $wallet->id)
        {!! BootForm::select('currency_id', 'Currency', \App\Models\Enums\Cryptocurrency::getNames()) !!}
    @else
        <div class="form-group "><label for="currency_id" class="control-label">Currency</label><div>
            <h4 style="margin-top:0px;">{{ cc($wallet->getCurrency()) }} {{ $wallet->getCurrency()->getName() }}</h4>
        </div></div>
    @endif
    {!! BootForm::text('address', 'Address') !!}
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="history.back()">Cancel</button>
        <button type="submit" class="btn btn-success">Ok</button>
    </div>
{!! BootForm::close() !!}