@extends('layouts.app')

@section('h1')
    Edit wallet #{{ $wallet->id }}
@endsection

@section('content')
<div class="container">
    @include('personal.wallet.form', array('wallet' => $wallet, 'cluster' => $cluster))
</div>
@endsection