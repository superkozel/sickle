@extends('layouts.app')

@section('h1')
    Wallets
@endsection

@section('content')
{{--<script>--}}
    {{--$(document).ready(function() {--}}
        {{--$('.wallet-address').editable();--}}
    {{--});--}}
{{--</script>--}}
<style>

    .wallet-block .wallet-address{
        max-width:100%;
        padding-right:45px;
        position: relative;
        line-height:1;
    }

    .currency-block .wallet-hover{
        visibility: hidden;
    }
    .currency-block:hover .wallet-hover{
        visibility: visible;
    }

    .wallet-address a{
        max-width:100%;
        overflow: hidden;
        display: inline-block;
    }
    .wallet-address a:hover{overflow: visible;background-color: white;display: inline;}
</style>
<table class="table table-condensed table-bordered">
    <thead>
        <tr>
            <th>Currency</th>
            <th>Address</th>
            <th>Balance</th>
            <th>Worth</th>
            <th>Updated at</th>
            <th>Rigs</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($wallets as $wallet)
            <tr>
                <td><i class="cc <?=mb_strtoupper($wallet->getCurrency()->getCode())?>"></i> {{ $wallet->getCurrency()->getName() }}</td>
                <td><a href="<?=$wallet->getEditUrl()?>" data-type="text" data-name="address" data-pk="1" data-url="<?=$wallet->getEditUrl()?>" data-title="Edit address">{{ $wallet->address }}</a></td>
                <td>
                    @php
                        $balance = $wallet->getBalance();
                        $rate = $wallet->getCurrency()->getRate();
                        $balanceLink = $wallet->getBalanceLink();
                    @endphp

                    @if ($balance !== false)
                        @if ($balanceLink)
                            <a target="_blank" href="{{ $wallet->getBalanceLink() }}">
                                {{ $balance }}
                            </a>
                        @else
                            {{ $balance }}
                        @endif
                    @endif

                </td>
                <td>
                    @if ($rate !== false)
                        ${{ round($rate * $balance, 2) }}
                    @endif
                </td>
                <td>
                    @if ($wallet->updated_at != $wallet->updated_at)
                        <span class="text-muted text-danger" href="">
                            Changed at {{ $wallet->updated_at }}
                        </span>
                    @endif
                </td>
                <td>
                    <div class="dropdown">
                        <a class="btn btn-sm btn-default dropdown-toggle" id="wallet{{ $wallet->id }}_rigs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%;">
                            @php ($rigCount = $cluster->rigs()->whereIn('configuration_id', $wallet->getConfigurations()->pluck('id'))->count())
                            {{ $rigCount }} {{ str_plural('rig', $rigCount) }}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="wallet{{ $wallet->id }}_rigs">
                            @foreach ($cluster->rigs()->whereIn('configuration_id', $wallet->getConfigurations()->pluck('id'))->get() as $rig)
                                <li><a href="{{ $rig->getUrl() }}">{{ $rig->getDisplayName() }}
                                        <div style="float:right">@include('personal._parts.columns.rig_status', ['rig' => $rig])</div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </td>
                <td style="text-align: center;width:14px;">
                    <a href="{{ $wallet->getEditUrl() }}"><i class="fa fa-pencil-alt"></i></a>
                </td>
                <td style="text-align: center;width:14px;">
                    <form style="display: inline;" method="post" action="<?=$wallet->getDeleteUrl()?>">
                        {{ csrf_field() }}
                        <a title="Delete wallet" style="margin-left:0px;" onclick="$(event.currentTarget).closest('form').submit()"><i class="fa fa-times"></i></a>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<a href="{{ action('Personal\WalletController@add') }}" class="btn btn-default">Add wallet</a>
@endsection