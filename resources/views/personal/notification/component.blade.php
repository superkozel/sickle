<a {{ ! empty($url) ? ('href='. $url) : '' }} class="media {{ $read ? 'read' : '' }} border-dotted">
    <span class="media-object pull-left">
        {{ $icon }}
    </span>
    <span class="media-body">
        @isset($heading)
            <span class="media-heading">{{ $heading }}</span>
        @endisset
        <span class="media-text">
            {{ $slot }}
        </span>
        <!-- meta icon -->
        <span class="media-meta pull-right">
            {{ $date }}
        </span>
        <!--/ meta icon -->
    </span>
</a>
