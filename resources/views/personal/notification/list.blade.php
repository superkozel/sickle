@extends('layouts.app')

@section('h1')
    Notifications
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-4">
            @include('personal.notification.listing', ['notifications' => $notifications])
        </div>
    </div>
@endsection
