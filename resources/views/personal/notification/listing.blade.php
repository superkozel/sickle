<!-- Message list -->
<div class="media-list">
    @if ($notifications->count() > 0)
        @foreach ($notifications->get() as $notification)
            @component('personal.notification.component')
                @switch($notification->type)
                    @case (\App\Notifications\Auth\LoginFromUnknownLocation::class)
                        @slot('url')
                            {{ action('Personal\AccountController@security') }}
                        @endslot

                        @slot('icon')
                            <i class="ico-warning-sign bgcolor-danger"></i>
                        @endslot

                        <span class="text-success">Login to your account from new device {{ $notification->data['user_agent'] }} [{{ $notification->data['ip'] }}]</span>
                    @break

                    @case (\App\Notifications\Rig\NotMining::class)
                        @php($rig = \App\Models\Rig::find($notification->data['id']))
                        @slot('url')
                            {{ $rig->getUrl() }}
                        @endslot

                        @slot('icon')
                            <i class="ico-heart-broken bgcolor-warning"></i>
                        @endslot

                        <span class="text-danger">Rig "{{ $rig->getDisplayName() }}" is not mining</span>
                    @break

                    @case (\App\Notifications\Rig\Offline::class)
                        @php($rig = \App\Models\Rig::find($notification->data['id']))
                        @slot('url')
                            {{ $rig->getUrl() }}
                        @endslot

                        @slot('icon')
                            <i class="ico-heart-broken bgcolor-danger"></i>
                        @endslot

                        <span class="text-danger">Rig "{{ $rig->getDisplayName() }}" is offline</span>
                    @break

                    @case (\App\Notifications\Rig\Online::class)
                        @php($rig = \App\Models\Rig::find($notification->data['id']))
                        @slot('url')
                            {{ $rig->getUrl() }}
                        @endslot

                        @slot('icon')
                            <i class="ico-ok bgcolor-success"></i>
                        @endslot

                        <span class="text-success">Rig "{{ $rig->getDisplayName() }}" is online</span>
                    @break

                    @case (\App\Notifications\Rig\MinerMessage::class)
                        @php($message = \App\Models\RigMessage::find($notification->data['id']))
                        @slot('url')
                            {{ $message->rig->getUrl() }}
                        @endslot

                        @slot('icon')
                            <i class="ico-error bgcolor-success"></i>
                        @endslot

                        @slot('heading')
                            Error on rig {{ $message->rig->name }}
                        @endslot

                        <span class="text-success">{{ $message->content }}</span>
                    @break

                    @default
                    @slot('url')
                        #
                    @endslot

                    @slot('icon')
                        <i class="ico-error bgcolor-danger"></i>
                    @endslot

                    @slot('heading')
                        Unknown notification {{ $notification->type }}
                    @endslot

                    <span class="text-warning">please add me</span>
                    @break;
                @endswitch

                @slot('date')
                    {{ $notification->created_at->diffForHumans() }}
                @endslot

                @slot('read')
                    {{ ! empty($notification->read_at) }}
                @endslot
            @endcomponent
        @endforeach

        @if (isset($limit) AND $notifications->count() > $limit)
            <a href="javascript:void(0);" class="media read border-dotted">
                <span class="media-body">
                    More notifications({{ $notifications->count() }})
                </span>
            </a>
        @endif
    @else
        <a class="media read border-dotted">
            <span class="media-body">
                <span class="media-text">No notifications yet</span>
            </span>
        </a>
    @endif
</div>
<!--/ Message list -->