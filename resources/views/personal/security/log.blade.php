@extends('layouts.app')

@section('h1')
Currency rates
@endsection

@section('content')

<table class="table table-condensed table-bordered">
    <thead>
    <tr>
        <th>Date</th>
        <th>Action</th>
        <th>IP</th>
        <th>Session</th>
        @foreach (\App\Models\Enums\Currency::getNames() as $code => $name)
        <th>{{ $code }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($logs as $event)
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @endforeach
    </tbody>
</table>
@endsection