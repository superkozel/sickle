@extends('layouts.app')

@section('container')
    nothing
@endsection

@section('content')
    <table class="table table-bordered table-condensed">
        <tr>
            <th></th>
            <th></th>
            <th>Type</th>
            <th>Hostname</th>
            <th>Hashrate</th>
            <th>Miner</th>
            @for($i=0;$i<$maxGpus;$i++)
                <th>GPU{{ $i }}</th>
            @endfor
        </tr>
        @foreach ($user->clusters as $cluster)
            @if (! $cluster->isOwner($user))
                <div class="h2" style="text-align: center;margin-top:0px;">
                    <?=$cluster->name?>
                </div>
            @endif
            @foreach ($cluster->rigs as $rig)
                <tr>
                    <td>
                        @include('personal._parts.columns.rig_status', ['rig' => $rig])
                    </td>
                    <td class="online_at-{{ $rig->id }}">
                        {{ $rig->online_at }}
                    </td>
                    <td style="width:30px;">
                        @include('personal._parts.columns.rig_type', ['rig' => $rig])
                    </td>
                    <td>
                        <a href="{{ action('Personal\RigController@show', ['rig' => $rig]) }}">
                            <span class="rig-name">{{ $rig->getDisplayName() }}</span>
                        </a>
                    </td>
                    <td>
                        @include('personal._parts.columns.rig_hashrate', ['rig' => $rig])
                    </td>
                    <td>{{ $rig->getMiner() ? $rig->getMiner()->getName() : ''}}</td>

                    @foreach($rig->getGpus() as $i => $gpu)
                        <td>
                            <span class="label label-{{ $gpu->getTemperature() > $rig->getRedTemperature() ? 'danger' : 'success' }}"><span class="gpu-temp-{{ $rig->id }}-{{ $i }}">{{ $gpu->getTemperature() }}</span>&deg;</span>
                            <span class="label label-info"><span class="gpu-fan-{{ $rig->id }}-{{ $i }}">{{ $gpu->getFanSpeed() }}</span>%</span>
                        </td>
                    @endforeach
                    @for($i=count($rig->getGpus());$i<$maxGpus;$i++)
                        <td></td>
                    @endfor
                </tr>
            @endforeach
        @endforeach
    </table>
@endsection
