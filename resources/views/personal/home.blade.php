@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <? foreach ($user->clusters as $cluster):?>
                        <h2>{{ $cluster->name }}</h2>
                        <a class="btn btn-default" href="<?=action('Personal\RigController@add')?>">Add rig</a>
                        <a class="btn btn-default" href="<?=action('Personal\WalletController@add')?>">Add wallet</a>
                    <? endforeach;?>

                    <h2>Getting started</h2>

                    <ol>
                        <li>Install OS on your Rig</li>
                        <li>Add rig using with hash, given to you after OS installation</li>
                        <li>Configure rig, click update rig button to upload configuration</li>
                        <li>Now you are getting profit, check hashrates and temps on your monitor</li>
                        <li>Learn advanced functions: overclocking, sharing rig access, security and alert notifications</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
