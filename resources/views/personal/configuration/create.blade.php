@extends('layouts.app')

@section('h1')
    Create configuration
@endsection

@section('content')
    @include('personal.configuration.form', array('configuration' => $configuration))
@endsection