@extends('layouts.app')

@section('h1')
Configurations
@endsection

@section('content')
<?php
/** @var \App\Models\Configuration $configuration */
?>

<table class="table table-bordered table-condensed table-responsive">
    <thead>
        <tr>
            <th>Name</th>
            <th>Currency</th>
            <th>Second currency</th>
            <th>Miner</th>
            <th>Rigs</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    @foreach($configurations as $configuration)
        <tr>
            <td>{{ $configuration->name }}</td>
            <td>
                <i class="cc <?=mb_strtoupper($configuration->getCurrency()->getCode())?>"></i> {{ $configuration->getCurrency()->getCode() }}
                <a {{ $configuration->wallet->address }} href="<?=$configuration->wallet->getEditUrl()?>">{{ short_hash($configuration->wallet->address) }}</a>
            </td>
            <td>
                @if ($configuration->isDual())
                    <i class="cc <?=mb_strtoupper($configuration->getSecondCurrency()->getCode())?>"></i> {{ $configuration->getSecondCurrency()->getCode() }}
                    <a title="{{ $configuration->secondWallet->address }}" href="<?=$configuration->secondWallet->getEditUrl()?>">{{ short_hash($configuration->secondWallet->address) }}</a>
                @endif
            </td>
            <td>
                {{ $configuration->getMiner() ? $configuration->getMiner()->getName() : '' }}
            </td>
            <td>
                <div class="dropdown">
                    <a class="btn btn-xs btn-default dropdown-toggle" id="configuration{{ $configuration->id }}_rigs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%;">
                        @php ($rigCount = $configuration->rigs->count())
                        {{ $rigCount }} {{ str_plural('rig', $rigCount) }}

                        @if ($configuration->rigs->count())
                            <span class="caret"></span>
                        @endif
                    </a>
                    @if ($configuration->rigs->count())
                    <ul class="dropdown-menu" aria-labelledby="configuration{{ $configuration->id }}_rigs">
                        @foreach ($configuration->rigs as $rig)
                            <li><a href="{{ $rig->getUrl() }}">{{ $rig->getDisplayName() }}
                                    <div style="float:right">@include('personal._parts.columns.rig_status', ['rig' => $rig])</div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </td>
            <td>
                @if ($configuration->rigs()->notUpToDate()->count() == 0)
                    <span class="text-muted">Rigs up to date</span>
                @else
                    {{ BootForm::open(['action' => ['Personal\ConfigurationController@apply', $configuration], 'method' => 'POST']) }}
                        <button class="btn btn-xs btn-danger" style="width:100%;">Burn to rigs</button>
                    {{ BootForm::close() }}
                @endif
            </td>
            <td style="text-align: center;width:14px;">
                <a href="<?=$configuration->getUrl()?>" title="Edit configuration"><i class="fa fa-pencil-alt"></i></a>
            </td>
            <td style="text-align: center;width:14px;">
                <form style="display: inline;" method="post" action="<?=$configuration->getDeleteUrl()?>">
                    {{ csrf_field() }}
                    <a title="Delete configuration" style="margin-left:0px;" onclick="if (confirm('Delete configuration?')) $(event.currentTarget).closest('form').submit()"><i class="fa fa-times"></i></a>
                </form>
            </td>
        </tr>
    @endforeach
</table>
<a class="btn btn-default" href="<?=action('Personal\ConfigurationController@create')?>" class="btn">Create configuration</a>
@endsection
