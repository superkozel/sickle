<?
/** @var \App\Models\Configuration $configuration */
/** @var \App\Models\Cryptocurrency\BaseCryptocurrency|null $currency */
/** @var \App\Models\Cryptocurrency\BaseCryptocurrency|null $secondCurrency */
/** @var \App\Models\Miner\BaseMiner|null $miner */
/** @var array $availableCurrencies */
if (! $configuration->currency_id)
    $configuration->currency_id = \App\Models\Enums\Cryptocurrency::ID_ETHEREUM;

$availableCurrencies = [];
$availableSecondCurrencies = [];
$availableMiners = [];
$currency = $configuration->getCurrency();
$secondCurrency = $configuration->getSecondCurrency();
$wallet = $configuration->wallet;
$secondWallet = $configuration->secondWallet;

foreach (\App\Models\Enums\Miner::all() as $miner) {
    $availableCurrencies += $miner->getPrimaryCurrencies();

    if ($currency) {
        $availableSecondCurrencies += $miner->getCurrencies();
    }

    if (($currency && $miner->hasCurrency($currency)) AND (! $secondCurrency OR $miner->hasCurrency($secondCurrency))) {
        $availableMiners[] = $miner;
    }
}

$availableCurrencies = collect($availableCurrencies)->unique(function($v){
    return $v->getId();
});

$availableSecondCurrencies = collect($availableSecondCurrencies)->unique(function($v){
    return $v->getId();
})->filter(function($v) use ($currency){
    return $currency->getId() != $v->getId();
});

$macroses = $configuration->getMacroses();

if (empty($subform))
    $subform = null;

$formName = function($name) use ($subform){
    if ($subform)
        return $subform . '[' . $name . ']';
    else
        return $name;
};

?>

@push('scripts')
    <script type="text/javascript" src="/theme/lander/plugins/select2/js/select2.js"></script>
@endpush
@push('css')
    <link rel="stylesheet" href="/theme/lander/plugins/select2/css/select2.css"/>
@endpush

<style>
    .select2-container .select2-choice abbr {
        right: 36px;
        top: 2px;
        font-size: 25px;
    }
</style>

<div class="configuration-form-container">
<? $echo = BootForm::open(['model' => $configuration, 'method' => 'POST']); if (! $subform) echo $echo;?>
    <?=BootForm::hidden($formName('cluster_id'), $cluster->id)?>
    <?=BootForm::text($formName('name'), 'Configuration name *')?>
    <?=BootForm::select($formName('currency_id'), 'Currency *', ['' => ''] + \App\Models\Enums\Cryptocurrency::getNames($availableCurrencies))?>

    <? if ($currency):?>
        <?=BootForm::select($formName('wallet_address'), $currency->getName() . ' wallet *', ['' => ''] + $cluster->wallets()->where('currency_id', $currency->getId())->pluck('address', 'address')->all(), Request::get($formName('wallet_address'), $configuration->wallet ? $configuration->wallet->address : ''))?>
    <? endif;?>

    <? if ($availableSecondCurrencies):?>
        <?=BootForm::select($formName('second_currency_id'), 'Second currency', ['' => ''] + \App\Models\Enums\Cryptocurrency::getNames($availableSecondCurrencies))?>
    <? endif;?>

    @if (count($availableMiners) > 0)

        <? if ($secondCurrency):?>
            <?=BootForm::select($formName('second_wallet_address'), $secondCurrency->getName() . ' wallet', ['' => ''] + $cluster->wallets()->where('currency_id', $secondCurrency->getId())->pluck('address', 'address')->all(), Request::get($formName('second_wallet_address'), $configuration->secondWallet ? $configuration->secondWallet->address : ''))?>
        <? endif;?>

        <h3>Config variables, click to use</h3>

        @foreach ($macroses as $macros => $macrosValue)
            <a style="cursor:pointer" onmousedown="event.stopPropagation();return false;" onclick="insertAtCaret('{{ $macros }}');">{{ $macros }}</a>
        @endforeach

        @if (count($availableMiners) > 1)
            <?=BootForm::select($formName('miner_id'), 'Miner', array_pluck($availableMiners, 'name', 'id'))?>
        @else
            <?=BootForm::hidden($formName('miner_id'), $availableMiners[0]->getId())?>
        @endif

        @if ($miner)
            @foreach ($fields = array_keys($miner->getValidationRules()) as $field)
                @if ($field == 'epools')
                    <?=BootForm::text($formName('epools'), 'epools')?>
                @elseif ($field == 'dpools' && $secondCurrency)
                    <?=BootForm::text($formName('dpools'), 'dpools')?>
                @elseif ($field == 'second_coin_intensity' && $secondCurrency)
                    <?=BootForm::number($formName('second_coin_intensity'), 'Second coin intensity(0-100)', null, ['max' => 100, 'min' => 1])?>
                @elseif ($field == 'config_override')
                    <?=BootForm::text($formName('config_override'), 'Config override')?>
                @endif
            @endforeach
        @endif

        @if (! $subform)
            <button type="submit" class="btn btn-success">Save configuration</button>
        @endif
    @else
        Данная конфигурация не поддерживается
    @endif
<? $echo = BootForm::close(); if (! $subform) echo $echo; ?>
</div>
<script>
    var formUpdateHandler = function(){
        var $form = $(this).closest('form');

        $.ajax({
            url: '<?=$subform ? action('Personal\ConfigurationController@create'): $_SERVER["REQUEST_URI"]?>',
            method: 'get',
            data: $form.serialize(),
            success: function(data){
                $('.configuration-form-container').replaceWith(data);
            }
        });
    };
    $('[name="{{ $formName('currency_id') }}"],[name="{{ $formName('second_currency_id') }}"],[name="{{ $formName('wallet_id') }}"],[name="{{ $formName('second_wallet_id') }}"]').change(formUpdateHandler);


    $().ready(function(){
        $("[name=wallet_address],[name=second_wallet_address]").removeClass('form-control').select2({
            placeholder: 'Select wallet'
        }).change(formUpdateHandler);
        $("[name=currency_id]").removeClass('form-control').select2({
            placeholder: 'Select currency'
        }).change(formUpdateHandler);
        $("[name=second_currency_id]").removeClass('form-control').select2({
            placeholder: 'Select currency',
            allowClear: true
        }).change(formUpdateHandler);
    })

    function insertAtCaret(text) {
        var $el = $(':focus');

        if ($el.length == 0)
            return;

        var cursorPos = $el.prop('selectionStart');
        var cursorEnd = $el.prop('selectionEnd');

        var v = $el.val();
        var textBefore = v.substring(0, cursorPos);
        var textAfter  = v.substring(cursorEnd, v.length);

        $el.val(textBefore + text + textAfter);

        $el.prop('selectionStart', cursorPos + text.length);
        $el.prop('selectionEnd', cursorPos + text.length);
    }
</script>
