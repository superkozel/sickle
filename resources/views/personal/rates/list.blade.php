@extends('layouts.app')

@section('h1')
    Currency rates
@endsection

@section('content')

    <table class="table table-condensed table-bordered">
        <thead>
        <tr>
            <th>Currency</th>
            @foreach (\App\Models\Enums\Currency::getNames() as $code => $name)
                <th>{{ $code }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
            @foreach(\App\Models\Enums\Cryptocurrency::all() as $cryptoCurrency)
                <tr>
                    <th>{{ cc($cryptoCurrency) }} {{ $cryptoCurrency->getCode() }}</th>
                    @foreach (\App\Models\Enums\Currency::getNames() as $code => $name)
                        @if ($rate =  $rates[$cryptoCurrency->getCode()][$code])
                            <th>{{ $rate }}</th>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection