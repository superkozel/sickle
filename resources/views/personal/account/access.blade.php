@extends('layouts.account')

@section('h1')
    Grant access
@endsection

@section('content2')
    <div class="col-md-6">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        {!! BootForm::inline() !!}
        {!! BootForm::email('email', 'email') !!}
        {!! BootForm::select('role_id', 'Role', \App\Models\Cluster::getGrantableUserRoleOptions()) !!}
        {!! BootForm::submit('Grant', ['style' => 'margin-top:23px']) !!}
        {!! BootForm::close() !!}

        <h3>Granted to:</h3>
        @if ($granted->count() > 0)
            <ul>
                @foreach ($granted as $access)
                    <li>
                        {{ BootForm::open() }}
                        <a onclick="var $el = $(event.currentTarget); confirm('Delete access to this cluster?', function(){$(event.currentTarget).closest('form').submit();});">
                            {{ $access->name }}<i class="fa fa-times"></i>
                        </a>
                        {!! BootForm::close() !!}
                    </li>
                @endforeach
            </ul>
        @else
            None
        @endif
    </div>
    <div class="col-md-6">
        <h2>Received accesses</h2>

        @if ($received->count())
            <ul>
                @foreach ($received as $cluster)
                    <li>
                        <span class="label label-info">
                            {{ \App\Models\Cluster::getUserRoleOptions()[$cluster->pivot->role_id] }}
                        </span>
                        {{ $cluster->name }} [{{ $cluster->getOwner()->name }}]

                        {{ BootForm::open() }}
                            <a onclick="var $el = $(event.currentTarget); confirm('Delete access to this cluster?', function(){$(event.currentTarget).closest('form').submit();});">
                                <i class="fa fa-times"></i>
                            </a>
                        {!! BootForm::close() !!}
                    </li>
                @endforeach
            </ul>
        @else
            No received accesses
        @endif
    </div>
@endsection