@extends('layouts.account')

@section('h1')
    Edit account
@endsection

@section('content2')
    {{ BootForm::horizontal(['model' => $user, 'method' => 'POST', 'id' => 'account-edit-form', 'class' => 'panel form-horizontal form-bordered']) }}

    <div class="panel-body pt0 pb0">
        <div class="form-group header bgcolor-default">
            <div class="col-md-12">
                <h4 class="semibold text-primary mt0 mb5">Change password</h4>
            </div>
        </div>

        {!! BootForm::password('old_password', 'Current password', ['autocomplete' => 'off', 'right_column_class' => 'col-sm-5', 'left_column_class' => 'col-sm-3']) !!}

        {!! BootForm::password('new_password', 'New password', ['autocomplete' => 'off', 'right_column_class' => 'col-sm-5', 'left_column_class' => 'col-sm-3']) !!}

        {!! BootForm::password('new_password_confirmation', 'Confirm password', ['autocomplete' => 'off', 'right_column_class' => 'col-sm-5', 'left_column_class' => 'col-sm-3']) !!}

    </div>
    <div class="panel-footer">
        <button type="reset" class="btn btn-default">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>

    {{ BootForm::close() }}
@endsection