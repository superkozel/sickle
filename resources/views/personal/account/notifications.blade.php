@extends('layouts.account')

@section('h1')
    Notifications
@endsection

@section('content2')
    <div class="row">
        <div class="col-md-4">
            {!! BootForm::open(['action' => 'Personal\AccountController@updateNotificationSettings']) !!}

            <label class="control-label">Notification settings</label>
            <span class="checkbox custom-checkbox" style="margin-top:8px;">
                <input onchange="javascript:$el = $(event.currentTarget);if ($el.prop('checked')) {$('.b-notification-settings').hide()} else {$('.b-notification-settings').show()};" type="checkbox" name="disable_all" id="disable-all" value="1" {{ ($settings->getDisableAll()) ? 'checked="checked"' : '' }}>
                <label for="disable-all">&nbsp;&nbsp;Disable all notifications</label>
            </span>

            <div class="b-notification-settings" {!! $settings->getDisableAll() ? 'style="display:none;"' : ''  !!}>
                <label class="control-label mt20">Receive notifications via</label>
                @foreach(\App\Models\User\NotificationSettings::getChannelOptions() as $type => $label)
                    <span class="checkbox custom-checkbox" style="margin-top:8px;">
                    <input type="checkbox" name="channels[]" id="channels-{{ $type }}" value="{{ $type }}" {{ ($settings->channelEnabled($type)) ? 'checked="checked"' : '' }}>
                    <label for="channels-{{ $type }}">&nbsp;&nbsp;{{ $label }}</label>
                </span>
                @endforeach

                <label class="control-label mt20">Enabled notifications</label>
                @foreach(\App\Models\User\NotificationSettings::getNotificationOptions() as $type => $label)
                    <span class="checkbox custom-checkbox" style="margin-top:8px;">
                    <input type="checkbox" name="notifications[]" id="notifications-{{ $type }}" value="{{ $type }}" {{ ($settings->notificationEnabled($type)) ? 'checked="checked"' : '' }}>
                    <label for="notifications-{{ $type }}">&nbsp;&nbsp;{{ $label }}</label>
                </span>
                @endforeach
            </div>

            <input class="btn btn-primary" style="margin-top:23px" type="submit" value="Save">

            {!! BootForm::close() !!}
        </div>
        <div class="col-md-8">
            <form method="POST">
                <div class="input-group">
                    <input type="text" class="form-control" name="auth_chat_id" placeholder="Set your chat code given by bot" value="">
                    <div class="input-group-btn">
                        <input type="hidden" name="action" value="telebot_auth_chat_id">
                        <button type="submit" class="btn btn-primary">Set Code</button>
                    </div>
                </div>
            </form>
            <br>

            <h5 class="mt-2 mb-3"><i class="fa fa-telegram mr-2" aria-hidden="true"></i>Telegram Bot Chat Setup</h5>
            <p>
                <!--<a href="tg://resolve?domain=hiveosbot">dsds</a>-->
                Start a new chat with this bot <a href="https://t.me/{{ env('TELEGRAM_BOT_NAME') }}">t.me/{{ env('TELEGRAM_BOT_NAME') }}</a>.
                The bot will reply to you and you will see your CODE which you should in the field above. Something like "12345".
            </p>
            <p>
                Your login is "superkozel".
                Send a command to bot <code>/user superkozel</code> and it will subscribe your account to receive notifications.
            </p>
            <p>
                You can add this bot to a chat group so that several admins can monitor rigs.
                Create your group and add "hiveosbot" as a member there.
                The chat code is negative for groups, something like "-12345".
            </p>
        </div>
    </div>
@endsection