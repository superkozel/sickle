@extends('layouts.account')

@section('h1')
    Finance
@endsection

@section('content2')

    <form class="panel form-horizontal form-bordered" name="form-security">
        <div class="panel-body pt0 pb0">
            <div class="form-group header bgcolor-default">
                <div class="col-md-12">
                    <h4 class="semibold text-primary mt0 mb5">Finance information</h4>
                    <p class="text-default nm">Payments and current balance.</p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <div class="mb-2">
                        <span class="label label-info">{{ Auth::user()->getOwnCluster()->rigs->count() }}</span> active rigs in profile
                        <br>
                        <span class="label label-success">${{ \App\Models\Pricing::getMonthlyRigFee(Auth::user()) }}</span> is your price per rig
                    </div>
                    <div class="small">
                        <i>* You pay for the maximum number of active rigs during the day and you have used 1 rigs today</i>
                    </div>
                </div>
                <div class="col-sm-4 text-center mt-4 mt-md-0 mb-4 mb-md-0">
                    Your current balance
                    <br>
                    <div class="big-balance">
                        <span class="text-muted small">$</span><span class="text-success">{{ number_format($user->balance, 2) }}</span>
                    </div>
                    Infinite days left
                </div>

                <div class="col-sm-4 text-right">
                    Recommended payment is
                    <br>
                    ${{ \App\Models\Pricing::calculateMonthlyPaymentForUser($user) }} per month
                    <br>

                    <form action="https://www.coinpayments.net/index.php" method="POST" class="form-inline mb-2 mt-2 pull-right">
                        <!--<div class="form-group">-->
                        <!--	<label for="amountf">Amount</label>-->
                        <!--	<input type="number" class="form-control" id="amountf" name="amountf" value="--><!--" required>-->
                        <!--</div>-->

                        <label for="amountf">Top&nbsp;up your balance&nbsp;$</label>
                        <input type="number" class="form-control form-control-sm" style="width: 68px; text-align: center; padding-left: 18px;" id="amountf" name="amountf" value="0" required="" pattern="[0-9]{1,}" min="1" step="1">

                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="cmd" value="_pay">
                        <input type="hidden" name="reset" value="1">
                        <input type="hidden" name="merchant" value="2f8bced1a48891d5fb118ae8ca50183b">
                        <input type="hidden" name="item_name" value="Hive Top Up for user superkozel (id 19148)">
                        <input type="hidden" name="item_number" value="19148">
                        <input type="hidden" name="currency" value="USD">
                        <input type="hidden" name="allow_extra" value="0">
                        <input type="hidden" name="want_shipping" value="0">
                        <input type="hidden" name="success_url" value="http://hiveos.farm/a/account/#pane-deposits">
                        <input type="hidden" name="cancel_url" value="http://hiveos.farm/a/account/#pane-deposits">

                        <button type="submit" class="btn btn-warning btn-sm ml-2">Pay</button>
                        <!--<input type="image" src="https://www.coinpayments.net/images/pub/buynow-wide-yellow.png" alt="Pay with CoinPayments.net">-->
                    </form>

                    <div class="clearfix"></div>
                    <div class="small">
                        <div class="text-warning"><i>* Add TRANSACTION FEE to the specified amount</i></div>
                        <a href="http://forum.hiveos.farm/discussion/189/payments-faq">Payments FAQ</a>
                    </div>
                </div>
            </div>
            <div class="form-group header bgcolor-default">
                <div class="col-md-12">
                    <h4 class="semibold text-primary mt0 mb5">Payment history</h4>
                </div>
            </div>
            <div class="form-group">
                <!-- panel body with collapse capabale -->
                <div class="table-responsive panel-collapse pull out">
                    <table class="table table-bordered table-hover" id="table1">
                        <thead>
                        <tr>
                            <th>Time</th>
                            <th>Currency</th>
                            <th>Coins</th>
                            <th>Worth in $</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($user->paymentTransactions as $transaction)
                            <tr>
                                <td>{{ $transaction->created_at }}</td>
                                <td>{{ $transaction->currency_id }}</td>
                                <td>{{ $transaction->amount }}</td>
                                <td>{{ $transaction->inDollars() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!--/ panel body with collapse capabale -->
            </div>
        </div>
    </form>


    <div class="row">
        @if ($address)
            <div class="col-md-12">
                <h1>Finance</h1>

                <h2>Your balance: ${{ $user->balance }}</h2>
                <h2>Your ethereum address: {{$user->payment_ethereum_address}}</h2>

                <h3>Add funds</h3>

                1$ = {{ $ethRate }};
                {{ BootForm::number('payment_sum', 'Payment sum', 0) }}
                {{ BootForm::number('ethereum_sum', 'Ethereum eq', 0, ['readonly' => 1]) }}

                <script>
                    var rate = {{ $ethRate }}
                    $('[name=payment_sum]').change(function(){
                        $('[name=ethereum_sum]').val($(this).val() * rate);
                    });
                </script>

                <a class="btn btn-sm btn-primary">Check new payments</a>
            </div>
        @endif
    </div>
@endsection
