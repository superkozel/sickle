@extends('layouts.account')

@section('h1')
    Security
@endsection

@php ($gi = geoip_open(storage_path('geoip/GeoLiteCity.dat'), GEOIP_STANDARD))

@section('content2')
    <div class="row">
        <div class="col-md-6">
            <h4>History</h4>
            <table class="table table-bordered" style="width:auto;">
                @foreach (\App\Models\User\SecurityLog::where('user_id', $user->id)->orderBy('created_at', 'DESC')->limit(20)->get() as $log)
                    <tr>
                        <td>{{ $log->created_at}}</td>
                        <td style="color:{{ in_array($log->event_type_id, [\App\Models\User\SecurityLog::EVENT_LOCKOUT, \App\Models\User\SecurityLog::EVENT_FAILED]) ? 'red' : 'green' }}">
                            {{ $log->getOptionsEventType()[$log->event_type_id]}}
                        </td>

                        <td>
                            @php ($record = GeoIP_record_by_addr($gi, $log->ip))
                            @if ($record)
                                ({{ $record->country_name }}{{ $record->city }},
                            @endif{{ $log->ip }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-6">
            <h4>Login locations</h4>
            <table class="table table-bordered" style="width:auto;">
                @foreach ($user->locations()->orderBy('updated_at', 'DESC')->get() as $location)
                    <tr>
                        <td>{{ $location->created_at}}</td>
                        <td>{{ $location->user_agent}}</td>
                        <td>{{ $location->ip }}</td>
                        <td style="color:{{ $location->confirmed ? 'green' : 'red' }}">{{ $location->confirmed ? 'Confirmed' : 'Non-confirmed' }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection