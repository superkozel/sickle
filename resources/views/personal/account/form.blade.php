{{ BootForm::horizontal(['model' => $user, 'method' => 'POST', 'id' => 'account-edit-form', 'class' => 'panel form-horizontal form-bordered']) }}

<div class="panel-body pt0 pb0">
    <div class="form-group header bgcolor-default">
        <div class="col-md-12">
            <h4 class="semibold text-primary mt0 mb5">{{ 'Edit profile' }}</h4>
        </div>
    </div>

    {!! BootForm::email('email', 'Email *', null, ['placeholder' => 'Email', 'right_column_class' => 'col-sm-5', 'left_column_class' => 'col-sm-3']) !!}

    {!! BootForm::text('telegram', 'Telegram', null, ['placeholder' => 'Telegram', 'right_column_class' => 'col-sm-5', 'left_column_class' => 'col-sm-3']) !!}

    {!! BootForm::password('password', 'Enter password', ['placeholder' => 'Enter password to authorize action', 'autocomplete' => 'new-password', 'right_column_class' => 'col-sm-5', 'left_column_class' => 'col-sm-3']) !!}


    {{--{!! BootForm::text('phone', 'Phone', '', ['placeholder' => 'Phone']) !!}--}}
</div>
<div class="panel-footer">
    <button type="reset" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-primary">OK</button>
</div>

{{ BootForm::close() }}
