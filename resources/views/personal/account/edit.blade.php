@extends('layouts.account')

@section('h1')
    Edit account
@endsection

@section('content2')
    @include('personal.account.form', array('user' => $user))
@endsection