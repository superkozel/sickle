@if ($rig->configuration)
    {{ cc($rig->configuration->getCurrency()) }} <span class="hashrate-{{ $rig->id }}">{{ $rig->getHashrate() }}</span> Mh/s
    @if ($second = $rig->configuration->getSecondCurrency())
        / {{ cc($second) }} <span class="hashrate2-{{ $rig->id }}">{{ $rig->getSecondHashrate() }}</span> Mh/s
    @endif
@else
    ---
@endif