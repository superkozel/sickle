<?
    $data = [
        ['#3bb33b', 'online'],
        ['orange', 'overheated'],
        ['grey', 'not mining'],
        ['red', 'offline'],
    ];

    if ($rig->isOnline()) {
        if ($rig->isMining()) {
            $state = 0;
        }
        elseif ($rig->isOverheated()) {
            $state = 1;
        }
        else {
            $state = 2;
        }
    }
    else {
        $state = 3;
    }
?>
<i title="{{ $data[$state][1] }}" class="fa fa-circle" style="width:7px;color:{{ $data[$state][0] }};"></i>