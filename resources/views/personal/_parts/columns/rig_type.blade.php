@if (isset($rig::getOptionsTypes()[$rig->type]))
    @if (is_subclass_of($rig, \App\Models\Rig\GpuRig::class))
        @if ($rig->type == \App\Models\Rig::TYPE_AMD)
        <span style="background-color: #c71f2f;padding:2px 5px;font-size:0.8em;color:white;font-weight:bold;">AMD</span>
        @else
        <span style="background-color: green;padding:2px 5px;font-size:0.6em;color:white;font-weight:bold;">Nvidia</span>
        @endif
    @else
    {{ $rig::getOptionsTypes()[$rig->type] }}
    @endif
@endif