{
    "authHost": "http://harvester1",
    "authEndpoint": "/broadcasting/auth",
    "clients": [
    {
        "appId": "93eee85d5dc6a207",
        "key": "aaddb09913c807e26f29b3e769efd8f8"
    }
],
    "database": "redis",
    "databaseConfig": {
    "redis": {},
    "sqlite": {
        "databasePath": "/database/laravel-echo-server.sqlite"
    }
},
    "devMode": true,
    "host": null,
    "port": "6002",
    "protocol": "http",
    "socketio": {},
    "sslCertPath": "",
    "sslKeyPath": "",
    "sslCertChainPath": "",
    "sslPassphrase": "",
    "apiOriginAllow": {
    "allowCors": false,
        "allowOrigin": "",
        "allowMethods": "",
        "allowHeaders": ""
}
}